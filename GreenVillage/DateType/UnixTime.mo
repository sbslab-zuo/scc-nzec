within GreenVillage.DateType;
model UnixTime "Read unix time"

  parameter Modelica.SIunits.Time samplePeriod= 600
    "Sample period of component";
  parameter Modelica.SIunits.Time startTime=0
    "First sample time instant";
  final parameter String moduleName="pythonUnixTime"
    "Name of the python module that contains the function";
  final parameter String functionName="unixTime" "Name of the python function";

  Buildings.Utilities.IO.Python27.Real_Real pyt(
    final nDblRea=1,
    samplePeriod=samplePeriod,
    startTime=startTime,
    moduleName=moduleName,
    functionName=functionName,
    final nDblWri=5)           "Python module calculating holidays"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Interfaces.IntegerInput year "Number of year"
    annotation (Placement(transformation(extent={{-140,60},{-100,100}})));
  Modelica.Blocks.Interfaces.IntegerInput month
    "Number of month in a year,[1,12]"
    annotation (Placement(transformation(extent={{-140,20},{-100,60}})));
  Modelica.Blocks.Interfaces.IntegerInput day "Number of day in a month [1,31]"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.IntegerInput hour "Number of hours"
    annotation (Placement(transformation(extent={{-140,-60},{-100,-20}})));
  Modelica.Blocks.Interfaces.RealInput    minute "Number of minutes"
    annotation (Placement(transformation(extent={{-140,-100},{-100,-60}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaYea "Real to integer"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaMon "Real to integer"
    annotation (Placement(transformation(extent={{-80,30},{-60,50}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaDay "Real to integer"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaHou "Real to integer"
    annotation (Placement(transformation(extent={{-80,-50},{-60,-30}})));

  Modelica.Blocks.Interfaces.RealOutput y "Real outputs received from Python"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
equation
  connect(year, reaYea.u)
    annotation (Line(points={{-120,80},{-82,80}}, color={255,127,0}));
  connect(month, reaMon.u) annotation (Line(points={{-120,40},{-102,40},{-102,40},
          {-82,40}}, color={255,127,0}));
  connect(day, reaDay.u)
    annotation (Line(points={{-120,0},{-82,0}}, color={255,127,0}));
  connect(hour, reaHou.u)
    annotation (Line(points={{-120,-40},{-82,-40}}, color={255,127,0}));
  connect(reaYea.y, pyt.uR[1]) annotation (Line(points={{-59,80},{-32,80},{-32,-1.6},
          {-12,-1.6}}, color={0,0,127}));
  connect(reaMon.y, pyt.uR[2]) annotation (Line(points={{-59,40},{-36,40},{-36,-0.8},
          {-12,-0.8}}, color={0,0,127}));
  connect(reaDay.y, pyt.uR[3])
    annotation (Line(points={{-59,0},{-12,0}}, color={0,0,127}));
  connect(reaHou.y, pyt.uR[4]) annotation (Line(points={{-59,-40},{-36,-40},{-36,
          0.8},{-12,0.8}}, color={0,0,127}));
  connect(y, pyt.yR[1])
    annotation (Line(points={{110,0},{11,0}}, color={0,0,127}));
  connect(minute, pyt.uR[5]) annotation (Line(points={{-120,-80},{-32,-80},{-32,
          1.6},{-12,1.6}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,127},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-152,112},{148,152}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end UnixTime;
