within GreenVillage.DateType;
model HolidayUS "Holidays in United States"
  Buildings.Utilities.IO.Python27.Real_Real pyt(
    final nDblWri=3,
    final nDblRea=1,
    samplePeriod=samplePeriod,
    startTime=startTime,
    moduleName=moduleName,
    functionName=functionName) "Python module calculating holidays"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Interfaces.IntegerInput year "Number of year"
    annotation (Placement(transformation(extent={{-140,30},{-100,70}})));
  Modelica.Blocks.Interfaces.IntegerInput day "Number of day in a month [1,31]"
    annotation (Placement(transformation(extent={{-140,-70},{-100,-30}})));
  Modelica.Blocks.Interfaces.IntegerInput month
    "Number of month in a year,[1,12]"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaYea "Real to integer"
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaMon "Real to integer"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaDay "Real to integer"
    annotation (Placement(transformation(extent={{-80,-60},{-60,-40}})));
  Modelica.Blocks.Interfaces.IntegerOutput
                                        holiday
    "Real outputs received from Python"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  parameter Modelica.SIunits.Time samplePeriod "Sample period of component";
  parameter Modelica.SIunits.Time startTime=0 "First sample time instant";
  final parameter String moduleName = "pythonHoliday"
    "Name of the python module that contains the function";
  final parameter String functionName="holidayUS" "Name of the python function";
  Buildings.Controls.OBC.CDL.Conversions.RealToInteger reaToInt
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));
equation
  connect(year, reaYea.u)
    annotation (Line(points={{-120,50},{-82,50}}, color={255,127,0}));
  connect(reaYea.y, pyt.uR[1]) annotation (Line(points={{-59,50},{-30,50},{-30,-1.33333},
          {-12,-1.33333}}, color={0,0,127}));
  connect(month, reaMon.u)
    annotation (Line(points={{-120,0},{-82,0}}, color={255,127,0}));
  connect(day, reaDay.u)
    annotation (Line(points={{-120,-50},{-82,-50}}, color={255,127,0}));
  connect(reaMon.y, pyt.uR[2])
    annotation (Line(points={{-59,0},{-12,0}}, color={0,0,127}));
  connect(reaDay.y, pyt.uR[3]) annotation (Line(points={{-59,-50},{-30,-50},{-30,
          1.33333},{-12,1.33333}}, color={0,0,127}));
  connect(reaToInt.u, pyt.yR[1])
    annotation (Line(points={{38,0},{11,0}}, color={0,0,127}));
  connect(reaToInt.y, holiday)
    annotation (Line(points={{61,0},{110,0}}, color={255,127,0}));
  annotation (defaultComponentName="holDay",
  Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,127},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-28,12},{28,-16}},
          lineColor={28,108,200},
          textString="Holiday"),
        Text(
          extent={{-152,112},{148,152}},
          lineColor={0,0,255},
          textString="%name")}),
  Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end HolidayUS;
