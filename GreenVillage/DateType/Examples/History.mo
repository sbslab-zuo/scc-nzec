within GreenVillage.DateType.Examples;
model History "Delayed data"
  extends Modelica.Icons.Example;

  GreenVillage.DateType.History his "Historical data"
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));
  Modelica.Blocks.Sources.Step step(startTime=3600)
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
equation
  connect(step.y, his.u)
    annotation (Line(points={{-19,0},{18,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=86400),
    __Dymola_Commands(file="modelica://GreenVillage/Resources/Scripts/Dymola/DateType/Examples/History.mos"
        "Simulate and Plot"));
end History;
