within GreenVillage.DateType.Examples;
model UnixTime
  extends Modelica.Icons.Example;

  GreenVillage.DateType.UnixTime unixTime(samplePeriod=60)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Buildings.Utilities.Time.CalendarTime calTim(zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017, offset(
        displayUnit="h"))
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
equation
  connect(calTim.year, unixTime.year) annotation (Line(points={{-59,47.8},{-32,47.8},
          {-32,8},{-12,8}},         color={255,127,0}));
  connect(calTim.month, unixTime.month) annotation (Line(points={{-59,50.6},{-30,
          50.6},{-30,4},{-12,4}},     color={255,127,0}));
  connect(calTim.day, unixTime.day) annotation (Line(points={{-59,53.4},{-28,53.4},
          {-28,0},{-12,0}},       color={255,127,0}));
  connect(calTim.hour, unixTime.hour) annotation (Line(points={{-59,56.2},{-26,56.2},
          {-26,-4},{-12,-4}},       color={255,127,0}));
  connect(calTim.minute, unixTime.minute) annotation (Line(points={{-59,59},{-24,
          59},{-24,-8},{-12,-8}},     color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end UnixTime;
