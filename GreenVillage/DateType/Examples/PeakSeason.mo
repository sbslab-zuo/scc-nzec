within GreenVillage.DateType.Examples;
model PeakSeason "Peak season "
  import GreenVillage;
  extends Modelica.Icons.Example;

  GreenVillage.DateType.PeakSeason peaSea1(
    nPeriods=1,
    startPeriod=[2017,1,1,0,0],
    endPeriod=[2017,4,1,0,0])
    "peak season with 1 period"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Buildings.Utilities.Time.CalendarTime calTim(zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017)
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));

  GreenVillage.DateType.PeakSeason peaSea2(
    nPeriods=2,
    startPeriod=[2017,1,1,0,0; 2017,3,1,0,0],
    endPeriod=[2017,2,1,0,0; 2017,4,1,0,0])
    "peak season with 1 period"
    annotation (Placement(transformation(extent={{-10,-56},{10,-36}})));
equation
  connect(calTim.day, peaSea1.day) annotation (Line(points={{-59,33.4},{-26,33.4},
          {-26,0},{-12,0}},   color={255,127,0}));
  connect(calTim.month, peaSea1.month) annotation (Line(points={{-59,30.6},{-28,
          30.6},{-28,4},{-12,4}}, color={255,127,0}));
  connect(calTim.year, peaSea1.year) annotation (Line(points={{-59,27.8},{-30,27.8},
          {-30,8},{-12,8}}, color={255,127,0}));
  connect(calTim.minute, peaSea1.minute) annotation (Line(points={{-59,39},{-22,
          39},{-22,-8},{-12,-8}}, color={0,0,127}));
  connect(calTim.hour, peaSea1.hour) annotation (Line(points={{-59,36.2},{-24,36.2},
          {-24,-4},{-12,-4}}, color={255,127,0}));
  connect(calTim.minute, peaSea2.minute) annotation (Line(points={{-59,39},{-22,
          39},{-22,-54},{-12,-54}}, color={0,0,127}));
  connect(calTim.hour, peaSea2.hour) annotation (Line(points={{-59,36.2},{-24,
          36.2},{-24,-50},{-12,-50}}, color={255,127,0}));
  connect(calTim.day, peaSea2.day) annotation (Line(points={{-59,33.4},{-26,
          33.4},{-26,-46},{-12,-46}}, color={255,127,0}));
  connect(calTim.month, peaSea2.month) annotation (Line(points={{-59,30.6},{-28,
          30.6},{-28,-42},{-12,-42}}, color={255,127,0}));
  connect(calTim.year, peaSea2.year) annotation (Line(points={{-59,27.8},{-30,
          27.8},{-30,-38},{-12,-38}}, color={255,127,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PeakSeason;
