within GreenVillage.DateType.Examples;
model HolidayUS "Holiday in US"
  import GreenVillage;
  extends Modelica.Icons.Example;
  GreenVillage.DateType.HolidayUS holDay(samplePeriod(displayUnit="h") = 3600)
    annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Buildings.Utilities.Time.CalendarTime calTim(zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017)
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
equation
  connect(calTim.day, holDay.day) annotation (Line(points={{-59,53.4},{-20,53.4},
          {-20,-5},{-10,-5}}, color={255,127,0}));
  connect(calTim.month, holDay.month) annotation (Line(points={{-59,50.6},{-22,
          50.6},{-22,0},{-10,0}}, color={255,127,0}));
  connect(calTim.year, holDay.year) annotation (Line(points={{-59,47.8},{-24,
          47.8},{-24,5},{-10,5}}, color={255,127,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=5184000));
end HolidayUS;
