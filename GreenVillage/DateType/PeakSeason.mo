within GreenVillage.DateType;
model PeakSeason "Block that determines the period of peak season"

  parameter Integer nPeriods= 2 "Number of peak season in a year";
  parameter Integer startPeriod[nPeriods,5] "Start date of the peak period: [year,month,day]";
  parameter Integer endPeriod[nPeriods,5] "Start date of the peak period: [year,month,day]";
  parameter Modelica.SIunits.Time samplePeriod=60 "Sample period of component";

  final parameter String moduleName="pythonUnixTime"
    "Name of the python module that contains the function";
  final parameter String functionName="unixTime" "Name of the python function";

  Modelica.Blocks.Interfaces.IntegerInput year "Number of year"
    annotation (Placement(transformation(extent={{-140,60},{-100,100}})));

  Modelica.Blocks.Interfaces.IntegerInput month
    "Number of month in a year,[1,12]"
    annotation (Placement(transformation(extent={{-140,20},{-100,60}})));
  Modelica.Blocks.Interfaces.IntegerInput day "Number of day in a month [1,31]"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.IntegerOutput y "Peak season index, 0-false, 1-true"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));

  Integer inter[nPeriods] "Intermediate variable";
  Integer peaInt "Peak season internal index";

  final parameter Real startUniTim[nPeriods](fixed=false);
  final parameter Real endUniTim[nPeriods](fixed=false);
  final parameter Real startUniTim_internal[1](fixed=false);
  final parameter Real endUniTim_internal[1](fixed=false);

  GreenVillage.DateType.UnixTime uniTim(samplePeriod=samplePeriod)
                                        "Unix time"
    annotation (Placement(transformation(extent={{-20,40},{0,60}})));
  Modelica.Blocks.Interfaces.IntegerInput hour "Number of hours"
    annotation (Placement(transformation(extent={{-140,-60},{-100,-20}})));
  Modelica.Blocks.Interfaces.RealInput    minute "Number of minutes"
    annotation (Placement(transformation(extent={{-140,-100},{-100,-60}})));


initial algorithm

   for i in 1:nPeriods loop
     startUniTim_internal := Buildings.Utilities.IO.Python27.Functions.exchange(
        moduleName=moduleName,
        functionName=functionName,
        dblWri={0.0},
        intWri=startPeriod[i],
        nDblWri=0,
        nDblRea=1,
        nIntWri=5,
        nIntRea=0,
        nStrWri=0,
        strWri={""});
      endUniTim_internal := Buildings.Utilities.IO.Python27.Functions.exchange(
        moduleName=moduleName,
        functionName=functionName,
        dblWri={0.0},
        intWri=endPeriod[i],
        nDblWri=0,
        nDblRea=1,
        nIntWri=5,
        nIntRea=0,
        nStrWri=0,
        strWri={""});
     startUniTim[i] := startUniTim_internal[1];
     endUniTim[i] := endUniTim_internal[1];
   end for;

equation

algorithm
  for j in 1:nPeriods loop
    // compare the timestamp
    if uniTim.y > startUniTim[j] and uniTim.y < endUniTim[j] then
       inter[j] := 1;
    else
       inter[j] := 0;
    end if;

  end for;

equation
 peaInt =integer(sum(inter));
 y = peaInt;

  connect(year, uniTim.year) annotation (Line(points={{-120,80},{-40,80},{-40,58},
          {-22,58}}, color={255,127,0}));
  connect(month, uniTim.month) annotation (Line(points={{-120,40},{-42,40},{-42,
          54},{-22,54}}, color={255,127,0}));
  connect(day, uniTim.day) annotation (Line(points={{-120,0},{-40,0},{-40,50},{-22,
          50}}, color={255,127,0}));
  connect(hour, uniTim.hour) annotation (Line(points={{-120,-40},{-38,-40},{-38,
          46},{-22,46}}, color={255,127,0}));
  connect(minute, uniTim.minute) annotation (Line(points={{-120,-80},{-36,-80},{
          -36,42},{-22,42}}, color={0,0,127}));
  annotation (defaultComponentName="peaSea",
  Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,127},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-152,112},{148,152}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PeakSeason;
