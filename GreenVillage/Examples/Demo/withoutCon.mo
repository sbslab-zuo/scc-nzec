within GreenVillage.Examples.Demo;
model withoutCon "\"test without controller\""
  extends base(sou(T=286.15));
  Modelica.Blocks.Sources.Constant const(k=1) annotation (Placement(transformation(extent={{-80,0},{-60,20}})));
equation
  connect(const.y, val.y) annotation (Line(points={{-59,10},{-46,10},{-36,10},{-36,-18}}, color={0,0,127}));
  annotation (experiment(StopTime=86400));
end withoutCon;
