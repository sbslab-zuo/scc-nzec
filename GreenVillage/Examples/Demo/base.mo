within GreenVillage.Examples.Demo;
partial model base
  extends Modelica.Icons.Example;
  package MediumAir = Buildings.Media.Air "medium of air";
  parameter Modelica.SIunits.Length rooLen = 30 "Length of the room";
  parameter Modelica.SIunits.Length rooWid = 30 "Width of the room";
  parameter Modelica.SIunits.Height rooHei = 3 "Height of the room";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 1 "Nominal mass flow rate";

  GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels.SimplifiedRoom
    simplifiedRoom(redeclare package Medium = MediumAir,
    rooLen=rooLen,
    rooWid=rooWid,
    rooHei=rooHei,
    m_flow_nominal=m_flow_nominal,
    nPorts=2)
    annotation (Placement(transformation(extent={{-8,-8},{12,12}})));
  Buildings.Fluid.Sources.Boundary_pT sin(
    redeclare package Medium = MediumAir,
    nPorts=1,
    p(displayUnit="Pa") = 100000 + 500)
                               annotation (Placement(transformation(extent={{70,-40},{50,-20}})));
  Modelica.Blocks.Sources.Sine loa(freqHz=1/86400/2, amplitude=1*1000*7)
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Buildings.Fluid.Sources.Boundary_pT sou(
    redeclare package Medium = MediumAir,
    nPorts=1,
    p(displayUnit="Pa") = 100000 + 1000)
                                annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));
  Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage val(
    redeclare package Medium = MediumAir,
    m_flow_nominal=m_flow_nominal,
    dpValve_nominal=250) annotation (Placement(transformation(extent={{-46,-40},{-26,-20}})));
  Buildings.Fluid.FixedResistances.PressureDrop res(
    redeclare package Medium = MediumAir,
    m_flow_nominal=m_flow_nominal,
    dp_nominal=250) annotation (Placement(transformation(extent={{16,-40},{36,-20}})));
equation
  connect(loa.y, simplifiedRoom.theLoa) annotation (Line(points={{-39,50},{-30,
          50},{-20,50},{-20,6},{-10,6}}, color={0,0,127}));
  connect(sou.ports[1], val.port_a) annotation (Line(points={{-60,-30},{-54,-30},{-46,-30}}, color={0,127,255}));
  connect(sin.ports[1], res.port_b) annotation (Line(points={{50,-30},{36,-30}}, color={0,127,255}));
  connect(val.port_b, simplifiedRoom.airPorts[2]) annotation (Line(points={{-26,-30},{0.425,-30},{0.425,-6.7}}, color={0,127,255}));
  connect(res.port_a, simplifiedRoom.airPorts[1])
    annotation (Line(points={{16,-30},{4.475,-30},{4.475,-6.7}},   color={0,127,255}));
end base;
