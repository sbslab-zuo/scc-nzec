within GreenVillage.Examples.Demo;
model withCon "\"test without controller\""
  extends base(sou(T=286.15));
  Buildings.Controls.Continuous.LimPID conPID(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    k=0.05,
    Ti=240) annotation (Placement(transformation(extent={{-66,4},{-46,24}})));
  Modelica.Blocks.Discrete.Sampler sampler(samplePeriod=60)
    annotation (Placement(transformation(extent={{38,-8},{58,12}})));
  Modelica.Blocks.Sources.Constant TSet(k=273.15 + 21)
    annotation (Placement(transformation(extent={{-100,4},{-80,24}})));
equation
  connect(TSet.y, conPID.u_s) annotation (Line(points={{-79,14},{-73.5,14},{
          -68,14}}, color={0,0,127}));
  connect(simplifiedRoom.TRooAir, sampler.u)
    annotation (Line(points={{13,2},{24.5,2},{36,2}}, color={0,0,127}));
  connect(sampler.y, conPID.u_m) annotation (Line(points={{59,2},{66,2},{66,
          -12},{-56,-12},{-56,2}}, color={0,0,127}));
  connect(conPID.y, val.y)
    annotation (Line(points={{-45,14},{-36,14},{-36,-18}}, color={0,0,127}));
  annotation (experiment(StopTime=86400));
end withCon;
