within GreenVillage.Examples.HeatPump;
model System3 "Part3 - Heat pump system model with control."
  extends Modelica.Icons.Example;
  final package Medium1 = Buildings.Media.Air
    "Medium model";
  replaceable package Medium2 = Buildings.Media.Water constrainedby
    Buildings.Media.Water
    "Medium model at the condenser side";
  // Room related parameters
  parameter Modelica.SIunits.Length rooLen=8 "Length of the room";
  parameter Modelica.SIunits.Length rooWid=8 "Width of the room";
  parameter Modelica.SIunits.Height rooHei=10 "Height of the room";
  final parameter Modelica.SIunits.Volume VAir = rooLen*rooWid*rooHei "Air volume of the zone";
  parameter Integer nOrientations = 2 "Number of orientations";
  parameter Modelica.SIunits.Area AWin[nOrientations]={4,4}
    "Vector of areas of windows by orientations";
  parameter Modelica.SIunits.Area ATransparent[nOrientations]={4,4}
    "Vector of areas of transparent (solar radiation transmittend) elements by
    orientations";
  final parameter Modelica.SIunits.Area AExt[nOrientations]={rooLen*rooHei,rooWid*
      rooHei} "Vector of areas of exterior walls by orientations";
  parameter Integer nExt=1 "Number of RC-elements of exterior walls";
  parameter Modelica.SIunits.ThermalResistance RExt[nExt]=fill(0.33142, nExt)
      "Vector of resistances of exterior walls, from inside to outside";
  parameter Modelica.SIunits.HeatCapacity CExt[nExt]=fill(5259993.23, nExt)
    "Vector of heat capacities of exterior walls, from inside to outside";
  final parameter Modelica.SIunits.Area AInt=2*rooHei*rooLen "Area of interior walls";
  parameter Integer nInt=1 "Number of RC-elements of interior walls";
  parameter Modelica.SIunits.ThermalResistance RInt[nInt]=fill(0.0168895, nInt)
    "Vector of resistances of interior walls, from port to center";
  parameter Modelica.SIunits.HeatCapacity CInt[nInt]=fill(123911363.86, nInt)
    "Vector of heat capacities of interior walls, from port to center";
  // Heat pump related parameters
  parameter Boolean computeReevaporation=true
    "Set to true to compute reevaporation of water that accumulated on coil";
  parameter Real minSpeRat=0.05            "Minimum speed ratio";
  parameter Modelica.SIunits.PressureDifference dpEva_nominal=1000
    "Pressure difference over evaporator at nominal flow rate";
  parameter Modelica.SIunits.PressureDifference dpCon_nominal=40000
    "Pressure difference over condenser at nominal flow rate";
  // fan related parameters
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP1 perFan "Performance data for fan";
  parameter Boolean addPowerToMedium=false
    "Set to false to avoid any power (=heat and flow work) being added to medium (may give simpler equations)";
  // sensor related parameters
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = datHP.sta[datHP.nSta].nomVal.mCon_flow_nominal
    "Nominal mass flow rate";

  Buildings.ThermalZones.ReducedOrder.RC.TwoElements theZon(
   redeclare package Medium = Medium1,
    VAir=VAir,
    alphaRad=2,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWin=2.7,
    RWin=0.01642857143,
    gWin=0.6,
    ratioWinConRad=0.09,
    AExt=AExt,
    alphaExt=2.7,
    nExt=nExt,
    RExt=RExt,
    RExtRem=0.126522,
    CExt=CExt,
    AInt=AInt,
    alphaInt=2.12,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    nPorts=2)
   "Room model"
    annotation (Placement(transformation(extent={{32,-36},{80,0}})));
  Modelica.Thermal.HeatTransfer.Components.Convection theConWin
    "Outdoor convective heat transfer of windows"
    annotation (Placement(transformation(extent={{20,0},{0,20}})));
  Modelica.Thermal.HeatTransfer.Components.Convection theConWal
    "Outdoor convective heat transfer of walls"
    annotation (Placement(transformation(extent={{20,-20},{0,-40}})));
  Modelica.Blocks.Sources.Constant alpWinOut(k=150)
    "Outdoor coefficient of heat transfer for windows"
    annotation (Placement(transformation(extent={{-20,20},{0,40}})));
  Modelica.Blocks.Sources.Constant alpWalOut(k=150)
    "Outdoor coefficient of heat transfer for walls"
    annotation (Placement(transformation(extent={{-20,-60},{0,-40}})));
  Buildings.HeatTransfer.Sources.PrescribedTemperature preTemWin
    "Prescribed temperature for windows outdoor surface temperature"
    annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
  Buildings.HeatTransfer.Sources.PrescribedTemperature preTemWal
    "Prescribed temperature for wall outdoor surface temperature"
    annotation (Placement(transformation(extent={{-60,-40},{-40,-20}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-108,-20},{-68,20}}), iconTransformation(extent=
           {{-182,4},{-162,24}})));
  Buildings.ThermalZones.ReducedOrder.SolarGain.CorrectionGDoublePane corG(n=
        nOrientations, UWin=2.1)
    annotation (Placement(transformation(extent={{40,40},{60,60}})));
  Buildings.BoundaryConditions.SolarIrradiation.DirectTiltedSurface HDirTil[nOrientations](
    each til=1.5707963267949,
    each lat=0.87266462599716,
    azi={3.1415926535898,4.7123889803847})
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
  Buildings.BoundaryConditions.SolarIrradiation.DiffusePerez HDifTil[nOrientations](
    each til=1.5707963267949,
    each lat=0.87266462599716,
    azi={3.1415926535898,4.7123889803847},
    each outSkyCon=true,
    each outGroCon=true)
    annotation (Placement(transformation(extent={{-60,34},{-40,54}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam=
        ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    annotation (Placement(transformation(extent={{-120,-10},{-100,10}})));
  Buildings.HeatTransfer.Sources.PrescribedHeatFlow perRadGai
    "Radiative heat flow of persons"
    annotation (Placement(transformation(extent={{40,-80},{60,-60}})));
  Buildings.HeatTransfer.Sources.PrescribedHeatFlow perConGai
    "Convective heat flow of persons"
    annotation (Placement(transformation(extent={{40,-100},{60,-80}})));
  Modelica.Blocks.Sources.CombiTimeTable intGai(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Examples/HeatPump/InternalGain.txt"),
    tableName="table",
    columns=2:3) "Internal heat gain."
    annotation (Placement(transformation(extent={{0,-90},{20,-70}})));
  Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.VariableSpeed varSpeHeaPum(
    redeclare final package MediumEva = Medium1,
    redeclare package MediumCon = Medium2,
    dpEva_nominal=dpEva_nominal,
    dpCon_nominal=dpCon_nominal,
    computeReevaporation=computeReevaporation,
    minSpeRat=minSpeRat,
    datCoi=datHP)
    "Variable speed heat pump"
    annotation (Placement(transformation(extent={{0,-140},{-20,-120}})));
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump1 datHP
    "Performance data for heat pump"
    annotation (Placement(transformation(extent={{84,82},{98,96}})));
  Buildings.Fluid.Movers.SpeedControlled_y fan(
  redeclare package Medium = Medium1, addPowerToMedium=addPowerToMedium,
    per=perFan.per)
    annotation (Placement(transformation(extent={{-60,-100},{-40,-80}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senSupAir(redeclare final package
      Medium = Medium1, final m_flow_nominal=m_flow_nominal)
    "Supply air temperature sensor"
    annotation (Placement(transformation(extent={{-40,-140},{-60,-120}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senConWatIn(redeclare final
      package Medium = Medium2, final m_flow_nominal=m_flow_nominal)
    "Inlet condenser water temperature"
    annotation (Placement(transformation(extent={{-60,-170},{-40,-150}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senConWatOut(redeclare final
      package Medium = Medium2, final m_flow_nominal=m_flow_nominal)
    "Inlet condenser water temperature"
    annotation (Placement(transformation(extent={{20,-170},{40,-150}})));
  Buildings.Fluid.Sources.MassFlowSource_T souWat(
    redeclare package Medium = Medium2,
    use_T_in=false,
    use_m_flow_in=false,
    m_flow=m_flow_nominal,
    T=298.15,
    nPorts=1) "Source on water side"
    annotation (Placement(transformation(extent={{-100,-178},{-80,-158}})));
  Buildings.Fluid.Sources.Boundary_pT sinWat(
    redeclare package Medium = Medium2,
    nPorts=1,
    p(displayUnit="Pa")) "Sink on water side"
    annotation (Placement(transformation(extent={{80,-178},{60,-158}})));
  Subsystems.HeatPump.WSHPPhysics.BaseModels.FanSpeedControl heaPumSpeCon(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    Ti=60,
    reverseAction=true) "Control block for heat pump speed."
    annotation (Placement(transformation(extent={{120,-120},{100,-100}})));
  Buildings.Controls.SetPoints.OccupancySchedule occSch(firstEntryOccupied=true)
    "Occupancy schedule for a residential house."
    annotation (Placement(transformation(extent={{-140,-100},{-120,-80}})));
  Buildings.Controls.OBC.CDL.Conversions.BooleanToReal booToRea
    annotation (Placement(transformation(extent={{-108,-100},{-88,-80}})));
  Buildings.Controls.OBC.CDL.Continuous.Sources.Constant rooAirSet(k=273.15 +
        20) "Room air setpoint"
    annotation (Placement(transformation(extent={{100,-160},{120,-140}})));
equation
  connect(theConWin.solid, theZon.window)
    annotation (Line(points={{20,10},{26,10},{26,-14},{32,-14}},
                                                             color={191,0,0}));
  connect(theConWal.solid, theZon.extWall) annotation (Line(points={{20,-30},{26,
          -30},{26,-22},{32,-22}},
                                 color={191,0,0}));
  connect(alpWinOut.y, theConWin.Gc)
    annotation (Line(points={{1,30},{10,30},{10,20}}, color={0,0,127}));
  connect(alpWalOut.y, theConWal.Gc)
    annotation (Line(points={{1,-50},{10,-50},{10,-40}}, color={0,0,127}));
  connect(preTemWin.port, theConWin.fluid)
    annotation (Line(points={{-40,10},{0,10}}, color={191,0,0}));
  connect(preTemWal.port, theConWal.fluid)
    annotation (Line(points={{-40,-30},{0,-30}}, color={191,0,0}));
  connect(weaBus.TDryBul, preTemWin.T) annotation (Line(
      points={{-88,0},{-72,0},{-72,10},{-62,10}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus.TDryBul, preTemWal.T) annotation (Line(
      points={{-88,0},{-72,0},{-72,-30},{-62,-30}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(corG.solarRadWinTrans, theZon.solRad) annotation (Line(points={{61,50},
          {70,50},{70,22},{28,22},{28,-3},{31,-3}}, color={0,0,127}));
  connect(HDirTil.H, corG.HDirTil) annotation (Line(points={{-39,70},{20,70},{20,
          56},{38,56}}, color={0,0,127}));
  connect(HDirTil.inc, corG.inc) annotation (Line(points={{-39,66},{22,66},{22,44},
          {38,44}}, color={0,0,127}));
  connect(HDifTil.HSkyDifTil, corG.HSkyDifTil) annotation (Line(points={{-39,50},
          {-32,50},{-32,52},{38,52}}, color={0,0,127}));
  connect(HDifTil.HGroDifTil, corG.HGroDifTil) annotation (Line(points={{-39,38},
          {-32,38},{-32,48},{38,48}}, color={0,0,127}));
  connect(weaBus, HDirTil[1].weaBus) annotation (Line(
      points={{-88,0},{-88,70},{-60,70}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus, HDirTil[2].weaBus) annotation (Line(
      points={{-88,0},{-88,70},{-60,70}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus, HDifTil[1].weaBus) annotation (Line(
      points={{-88,0},{-88,44},{-60,44}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus, HDifTil[2].weaBus) annotation (Line(
      points={{-88,0},{-88,44},{-60,44}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{-100,0},{-88,0}},
      color={255,204,51},
      thickness=0.5));
  connect(perRadGai.port, theZon.intGainsRad) annotation (Line(points={{60,-70},
          {92,-70},{92,-10},{80,-10}}, color={191,0,0}));
  connect(perConGai.port, theZon.intGainsConv) annotation (Line(points={{60,-90},
          {86,-90},{86,-14},{80,-14}}, color={191,0,0}));
  connect(intGai.y[1], perRadGai.Q_flow) annotation (Line(points={{21,-80},{30,-80},
          {30,-70},{40,-70}}, color={0,0,127}));
  connect(intGai.y[2], perConGai.Q_flow) annotation (Line(points={{21,-80},{30,-80},
          {30,-90},{40,-90}}, color={0,0,127}));
  connect(varSpeHeaPum.port_b, senSupAir.port_a)
    annotation (Line(points={{-20,-130},{-40,-130}}, color={0,127,255}));
  connect(senSupAir.port_b, fan.port_a) annotation (Line(points={{-60,-130},{-68,
          -130},{-68,-90},{-60,-90}}, color={0,127,255}));
  connect(fan.port_b, theZon.ports[1]) annotation (Line(points={{-40,-90},{-30,-90},
          {-30,-100},{69.475,-100},{69.475,-35.95}}, color={0,127,255}));
  connect(theZon.ports[2], varSpeHeaPum.port_a) annotation (Line(points={{72.525,
          -35.95},{72.525,-130},{0,-130}}, color={0,127,255}));
  connect(souWat.ports[1], senConWatIn.port_a) annotation (Line(points={{-80,-168},
          {-70,-168},{-70,-160},{-60,-160}}, color={0,127,255}));
  connect(senConWatIn.port_b, varSpeHeaPum.portCon_a) annotation (Line(points={{
          -40,-160},{-16,-160},{-16,-140}}, color={0,127,255}));
  connect(varSpeHeaPum.portCon_b, senConWatOut.port_a) annotation (Line(points={
          {-4,-140},{-4,-160},{20,-160}}, color={0,127,255}));
  connect(senConWatOut.port_b, sinWat.ports[1]) annotation (Line(points={{40,-160},
          {50,-160},{50,-168},{60,-168}}, color={0,127,255}));
  connect(heaPumSpeCon.spe, varSpeHeaPum.speRat) annotation (Line(
      points={{99,-110},{8,-110},{8,-122},{1.2,-122}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(theZon.TAir, heaPumSpeCon.u_m) annotation (Line(
      points={{81,-2},{130,-2},{130,-117},{122,-117}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(occSch.occupied, booToRea.u) annotation (Line(
      points={{-119,-96},{-116,-96},{-116,-90},{-110,-90}},
      color={255,0,255},
      pattern=LinePattern.Dash));
  connect(booToRea.y, fan.y) annotation (Line(
      points={{-87,-90},{-80,-90},{-80,-70},{-50,-70},{-50,-78}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(occSch.occupied, heaPumSpeCon.on) annotation (Line(
      points={{-119,-96},{-116,-96},{-116,-106},{96,-106},{96,-94},{128,-94},{
          128,-103},{122,-103}},
      color={255,0,255},
      pattern=LinePattern.Dash));
  connect(rooAirSet.y, heaPumSpeCon.u_s) annotation (Line(
      points={{121,-150},{128,-150},{128,-110},{122,-110}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}})),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-160,-180},{140,
            100}})),
    experiment(StartTime=17280000, StopTime=17366400),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Examples/HeatPump/System3.mos"
        "Simulate and Plot"));
end System3;
