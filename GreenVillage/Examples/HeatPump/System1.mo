within GreenVillage.Examples.HeatPump;
model System1 "Part1 - RC room model with heat transfer from the envelope."
  extends Modelica.Icons.Example;

  // Room related parameters
  parameter Modelica.SIunits.Length rooLen=8 "Length of the room";
  parameter Modelica.SIunits.Length rooWid=8 "Width of the room";
  parameter Modelica.SIunits.Height rooHei=10 "Height of the room";
  parameter Integer nOrientations = 2 "Number of orientations";

  Buildings.ThermalZones.ReducedOrder.RC.TwoElements theZon(
   redeclare package Medium = Buildings.Media.Air,
    VAir=rooLen*rooWid*rooHei,
    alphaRad=2,
    nOrientations=nOrientations,
    AWin={4,4},
    ATransparent={4,4},
    alphaWin=2.7,
    RWin=0.01642857143,
    gWin=0.6,
    ratioWinConRad=0.09,
    AExt={rooLen*rooHei,rooWid*rooHei},
    alphaExt=2.7,
    nExt=1,
    RExt={0.33142},
    RExtRem=0.126522,
    CExt={5259932.23},
    AInt=2*rooHei*rooLen,
    alphaInt=2.12,
    nInt=1,
    RInt={0.0168895},
    CInt={123911363.86})
   "Room model"
    annotation (Placement(transformation(extent={{32,-36},{80,0}})));
  Modelica.Thermal.HeatTransfer.Components.Convection theConWin
    "Outdoor convective heat transfer of windows"
    annotation (Placement(transformation(extent={{20,0},{0,20}})));
  Modelica.Thermal.HeatTransfer.Components.Convection theConWal
    "Outdoor convective heat transfer of walls"
    annotation (Placement(transformation(extent={{20,-20},{0,-40}})));
  Modelica.Blocks.Sources.Constant alpWinOut(k=150)
    "Outdoor coefficient of heat transfer for windows"
    annotation (Placement(transformation(extent={{-20,20},{0,40}})));
  Modelica.Blocks.Sources.Constant alpWalOut(k=150)
    "Outdoor coefficient of heat transfer for walls"
    annotation (Placement(transformation(extent={{-20,-60},{0,-40}})));
  Buildings.HeatTransfer.Sources.PrescribedTemperature preTemWin
    "Prescribed temperature for windows outdoor surface temperature"
    annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
  Buildings.HeatTransfer.Sources.PrescribedTemperature preTemWal
    "Prescribed temperature for wall outdoor surface temperature"
    annotation (Placement(transformation(extent={{-60,-40},{-40,-20}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-108,-20},{-68,20}}), iconTransformation(extent=
           {{-182,4},{-162,24}})));
  Buildings.ThermalZones.ReducedOrder.SolarGain.CorrectionGDoublePane corG(n=
        nOrientations, UWin=2.1)
    annotation (Placement(transformation(extent={{40,40},{60,60}})));
  Buildings.BoundaryConditions.SolarIrradiation.DirectTiltedSurface HDirTil[nOrientations](
    each til=1.5707963267949,
    each lat=0.87266462599716,
    azi={3.1415926535898,4.7123889803847})
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
  Buildings.BoundaryConditions.SolarIrradiation.DiffusePerez HDifTil[nOrientations](
    each til=1.5707963267949,
    each lat=0.87266462599716,
    azi={3.1415926535898,4.7123889803847},
    each outSkyCon=true,
    each outGroCon=true)
    annotation (Placement(transformation(extent={{-60,34},{-40,54}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam=
        ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    annotation (Placement(transformation(extent={{-120,-10},{-100,10}})));
  Buildings.HeatTransfer.Sources.PrescribedHeatFlow perRadGai
    "Radiative heat flow of persons"
    annotation (Placement(transformation(extent={{40,-80},{60,-60}})));
  Buildings.HeatTransfer.Sources.PrescribedHeatFlow perConGai
    "Convective heat flow of persons"
    annotation (Placement(transformation(extent={{40,-100},{60,-80}})));
  Modelica.Blocks.Sources.CombiTimeTable intGai(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Examples/HeatPump/InternalGain.txt"),
    tableName="table",
    columns=2:3) "Internal heat gain."
    annotation (Placement(transformation(extent={{0,-90},{20,-70}})));

equation
  connect(theConWin.solid, theZon.window)
    annotation (Line(points={{20,10},{26,10},{26,-14},{32,-14}},
                                                             color={191,0,0}));
  connect(theConWal.solid, theZon.extWall) annotation (Line(points={{20,-30},{26,
          -30},{26,-22},{32,-22}},
                                 color={191,0,0}));
  connect(alpWinOut.y, theConWin.Gc)
    annotation (Line(points={{1,30},{10,30},{10,20}}, color={0,0,127}));
  connect(alpWalOut.y, theConWal.Gc)
    annotation (Line(points={{1,-50},{10,-50},{10,-40}}, color={0,0,127}));
  connect(preTemWin.port, theConWin.fluid)
    annotation (Line(points={{-40,10},{0,10}}, color={191,0,0}));
  connect(preTemWal.port, theConWal.fluid)
    annotation (Line(points={{-40,-30},{0,-30}}, color={191,0,0}));
  connect(weaBus.TDryBul, preTemWin.T) annotation (Line(
      points={{-88,0},{-72,0},{-72,10},{-62,10}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus.TDryBul, preTemWal.T) annotation (Line(
      points={{-88,0},{-72,0},{-72,-30},{-62,-30}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(corG.solarRadWinTrans, theZon.solRad) annotation (Line(points={{61,50},
          {70,50},{70,22},{28,22},{28,-3},{31,-3}}, color={0,0,127}));
  connect(HDirTil.H, corG.HDirTil) annotation (Line(points={{-39,70},{20,70},{20,
          56},{38,56}}, color={0,0,127}));
  connect(HDirTil.inc, corG.inc) annotation (Line(points={{-39,66},{22,66},{22,44},
          {38,44}}, color={0,0,127}));
  connect(HDifTil.HSkyDifTil, corG.HSkyDifTil) annotation (Line(points={{-39,50},
          {-32,50},{-32,52},{38,52}}, color={0,0,127}));
  connect(HDifTil.HGroDifTil, corG.HGroDifTil) annotation (Line(points={{-39,38},
          {-32,38},{-32,48},{38,48}}, color={0,0,127}));
  connect(weaBus, HDirTil[1].weaBus) annotation (Line(
      points={{-88,0},{-88,70},{-60,70}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus, HDirTil[2].weaBus) annotation (Line(
      points={{-88,0},{-88,70},{-60,70}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus, HDifTil[1].weaBus) annotation (Line(
      points={{-88,0},{-88,44},{-60,44}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaBus, HDifTil[2].weaBus) annotation (Line(
      points={{-88,0},{-88,44},{-60,44}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{-100,0},{-88,0}},
      color={255,204,51},
      thickness=0.5));
  connect(perRadGai.port, theZon.intGainsRad) annotation (Line(points={{60,-70},
          {92,-70},{92,-10},{80,-10}}, color={191,0,0}));
  connect(perConGai.port, theZon.intGainsConv) annotation (Line(points={{60,-90},
          {86,-90},{86,-14},{80,-14}}, color={191,0,0}));
  connect(intGai.y[1], perRadGai.Q_flow) annotation (Line(points={{21,-80},{30,
          -80},{30,-70},{40,-70}}, color={0,0,127}));
  connect(intGai.y[2], perConGai.Q_flow) annotation (Line(points={{21,-80},{30,
          -80},{30,-90},{40,-90}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}})),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{100,100}})),
    experiment(StopTime=86400),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Examples/HeatPump/System1.mos"
        "Simulate and Plot"));
end System1;
