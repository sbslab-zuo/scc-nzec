within GreenVillage.Subsystems;
package RenewableEnergy "Renewable energy subsystem"
  annotation (Documentation(info="<html>
<p>This package is built for the renewable energy subsystem of HGV. 
At present, only <a href=\"modelica://GreenVillage.Subsystems.RenewableEnergy.PV\">PV subsystem</a> is included.</p>
</html>",
        revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"));
end RenewableEnergy;
