within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel;
model HGVPVs "HGV's PV subsystem "
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroA(
    num=2,
    A={15.36,15.36},
    pf={0.8,0.8},
    eta_DCAC={0.8,0.8},
    fAct={1,1},
    eta={0.149,0.149},
    til={0.5235987755983,0.26179938779915},
    lat={0.48051008702506,0.48051008702506},
    azi={-0.78539816339745,0.78539816339745},
    V_nominal=V_nominal)
    "PV panel group A (Thelma Building)"
    annotation (Placement(transformation(extent={{0,90},{20,110}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroC(
    num=3,
    A={46.08,38.40,25.60},
    pf={0.8,0.8,0.8},
    eta_DCAC={0.8,0.8,0.8},
    fAct={1,1,1},
    eta={0.149,0.149,0.149},
    til={0.087266462599716,0.087266462599716,0.5235987755983},
    lat={0.48051008702506,0.48051008702506,0.48051008702506},
    azi={-0.78539816339745,-0.78539816339745,0.78539816339745},
    V_nominal=V_nominal)
    "PV panel group C (Rosedale Building)"
    annotation (Placement(transformation(extent={{0,60},{20,80}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroD(
    num=1,
    A={51.20},
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    eta={0.149},
    til={0.34906585039887},
    lat={0.48051008702506},
    azi={-0.78539816339745},
    V_nominal=V_nominal)     "PV panel group D (Sears Building)"
    annotation (Placement(transformation(extent={{0,30},{20,50}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroF(
    num=1,
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    A={29.34},
    eta={0.16},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={-0.78539816339745},
    V_nominal=V_nominal)     "PV panel group F (Pickle_fish Building)"
    annotation (Placement(transformation(extent={{0,0},{20,20}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroG(
    num=4,
    A={17.93,19.56,13.04,9.78},
    pf={0.8,0.8,0.8,0.8},
    eta_DCAC={0.8,0.8,0.8,0.8},
    fAct={1,1,1,1},
    eta={0.16,0.16,0.16,0.16},
    til={0.17453292519943,0.087266462599716,0.087266462599716,0.17453292519943},
    lat={0.48051008702506,0.48051008702506,0.48051008702506,0.48051008702506},
    azi={0.78539816339745,0.78539816339745,-0.78539816339745,0.78539816339745},
    V_nominal=V_nominal)
    "PV panel group G (Pillsbury Building)"
    annotation (Placement(transformation(extent={{0,-30},{20,-10}})));

  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroNC(
    num=1,
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    A={84.48},
    eta={0.149},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={-0.78539816339745},
    V_nominal=V_nominal)     "PV panel group NC (North Carport)"
    annotation (Placement(transformation(extent={{0,-60},{20,-40}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroSC(
    num=1,
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    eta={0.149},
    A={96},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={-0.78539816339745},
    V_nominal=V_nominal)     "PV panel group SC (South Carport)"
    annotation (Placement(transformation(extent={{0,-90},{20,-70}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroW(
    num=1,
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    A={149.96},
    eta={0.16},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={0.78539816339745},
    V_nominal=V_nominal)    "PV panel group W (Warehouse)"
    annotation (Placement(transformation(extent={{0,-120},{20,-100}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus "Weather data"
    annotation (
     Placement(transformation(extent={{-112,70},{-92,90}}),iconTransformation(
          extent={{-112,70},{-92,90}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin1(
    P_nominal=5000,
    l=50,
    V_nominal=240) "Cable"
    annotation (Placement(transformation(extent={{-40,90},{-60,110}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin2(
    P_nominal=5000,
    l=50,
    V_nominal=240) "Cable"
    annotation (Placement(transformation(extent={{-40,60},{-60,80}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin3(
    P_nominal=5000,
    l=50,
    V_nominal=240) "Cable"
    annotation (Placement(transformation(extent={{-40,30},{-60,50}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin4(
    P_nominal=5000,
    l=50,
    V_nominal=240) "Cable"
    annotation (Placement(transformation(extent={{-40,0},{-60,20}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin5(
    P_nominal=5000,
    l=50,
    V_nominal=240) "Cable" annotation (Placement(transformation(extent=
            {{-40,-30},{-60,-10}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin6(
    P_nominal=5000,
    l=50,
    V_nominal=240) "Cable" annotation (Placement(transformation(extent=
            {{-40,-60},{-60,-40}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin7(
    P_nominal=5000,
    l=50,
    V_nominal=240) "Cable" annotation (Placement(transformation(extent=
            {{-40,-90},{-60,-70}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin8(
    P_nominal=5000,
    l=50,
    V_nominal=240) "Cable" annotation (Placement(transformation(extent=
            {{-40,-120},{-60,-100}})));
  Modelica.Blocks.Math.MultiSum totPVPowDC(nu=14, y(unit="W"))
    "Total power generated from PV panels"
    annotation (Placement(transformation(extent={{60,40},{80,60}})));
  replaceable Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_p terminal
    annotation (Placement(transformation(extent={{-108,-8},{-92,8}}),
        iconTransformation(extent={{-108,-8},{-92,8}})));
  Modelica.Blocks.Interfaces.RealOutput totPowDC "Total DC power"
    annotation (Placement(transformation(extent={{100,40},{120,60}})));
  Modelica.Blocks.Math.MultiSum totPVPowAC(nu=14, y(unit="W"))
    "Total PV power could be consumed by users"
    annotation (Placement(transformation(extent={{60,-40},{80,-20}})));
  Modelica.Blocks.Interfaces.RealOutput totPowAC "Total AC power"
    annotation (Placement(transformation(extent={{100,-40},{120,-20}})));
  parameter Modelica.SIunits.Voltage V_nominal=120
    "Nominal voltage (V_nominal >= 0)";
  System.Baseclasss.ControlBus infBus "Information bus" annotation (Placement(
        transformation(extent={{80,-100},{100,-80}}), iconTransformation(extent=
           {{-8,-106},{12,-86}})));
equation
  connect(weaBus, PVGroA.weaBus) annotation (Line(
      points={{-102,80},{-20,80},{-20,118},{10,118},{10,109}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, PVGroC.weaBus) annotation (Line(
      points={{-102,80},{10,80},{10,79}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, PVGroD.weaBus) annotation (Line(
      points={{-102,80},{-20,80},{-20,60},{10,60},{10,49}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, PVGroF.weaBus) annotation (Line(
      points={{-102,80},{-20,80},{-20,30},{10,30},{10,19}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, PVGroG.weaBus) annotation (Line(
      points={{-102,80},{-20,80},{-20,0},{10,0},{10,-11}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, PVGroNC.weaBus) annotation (Line(
      points={{-102,80},{-20,80},{-20,-30},{10,-30},{10,-41}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, PVGroSC.weaBus) annotation (Line(
      points={{-102,80},{-20,80},{-20,-60},{10,-60},{10,-71}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, PVGroW.weaBus) annotation (Line(
      points={{-102,80},{-20,80},{-20,-92},{10,-92},{10,-101}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(lin1.terminal_n, PVGroA.terminal)
    annotation (Line(points={{-40,100},{0.2,100}}, color={0,120,120}));
  connect(lin2.terminal_n, PVGroC.terminal)
    annotation (Line(points={{-40,70},{0.2,70}}, color={0,120,120}));
  connect(lin3.terminal_n, PVGroD.terminal)
    annotation (Line(points={{-40,40},{0.2,40}}, color={0,120,120}));
  connect(lin4.terminal_n, PVGroF.terminal)
    annotation (Line(points={{-40,10},{0.2,10}}, color={0,120,120}));
  connect(lin5.terminal_n, PVGroG.terminal)
    annotation (Line(points={{-40,-20},{0.2,-20}}, color={0,120,120}));
  connect(lin6.terminal_n, PVGroNC.terminal)
    annotation (Line(points={{-40,-50},{0.2,-50}}, color={0,120,120}));
  connect(lin7.terminal_n, PVGroSC.terminal)
    annotation (Line(points={{-40,-80},{0.2,-80}}, color={0,120,120}));
  connect(lin8.terminal_n, PVGroW.terminal)
   annotation (Line(points={{-40,
          -110},{0.2,-110}}, color={0,120,120}));
  connect(lin1.terminal_p, lin2.terminal_p)
    annotation (Line(points={{-60,100},{-60,70}}, color={0,120,120}));
  connect(lin2.terminal_p, lin3.terminal_p)
    annotation (Line(points={{-60,70},{-60,40}}, color={0,120,120}));
  connect(lin3.terminal_p, lin4.terminal_p)
    annotation (Line(points={{-60,40},{-60,10}}, color={0,120,120}));
  connect(lin4.terminal_p, lin5.terminal_p)
    annotation (Line(points={{-60,10},{-60,-20}}, color={0,120,120}));
  connect(lin5.terminal_p, lin6.terminal_p)
    annotation (Line(points={{-60,-20},{-60,-50}}, color={0,120,120}));
  connect(lin6.terminal_p, lin7.terminal_p)
    annotation (Line(points={{-60,-50},{-60,-80}}, color={0,120,120}));
  connect(lin7.terminal_p, lin8.terminal_p) annotation (Line(points={{-60,
          -80},{-60,-110}}, color={0,120,120}));
  connect(PVGroA.PDC, totPVPowDC.u[1:2]) annotation (Line(points={{21,
          104},{40,104},{40,0},{60,0},{60,55.5}}, color={0,0,127}));
  connect(PVGroC.PDC, totPVPowDC.u[3:5]) annotation (Line(points={{21,
          74},{40,74},{40,0},{60,0},{60,52.5}}, color={0,0,127}));
  connect(PVGroD.PDC, totPVPowDC.u[6:6]) annotation (Line(points={{21,
          44},{40,44},{40,0},{60,0},{60,51.5}}, color={0,0,127}));
  connect(PVGroF.PDC, totPVPowDC.u[7:7]) annotation (Line(points={{21,
          14},{40,14},{40,0},{60,0},{60,50.5}}, color={0,0,127}));
  connect(PVGroG.PDC, totPVPowDC.u[8:11]) annotation (Line(points={{21,
          -16},{40,-16},{40,0},{60,0},{60,46.5}}, color={0,0,127}));
  connect(PVGroNC.PDC, totPVPowDC.u[12:12]) annotation (Line(points={{
          21,-46},{40,-46},{40,0},{60,0},{60,45.5}}, color={0,0,127}));
  connect(PVGroSC.PDC, totPVPowDC.u[13:13]) annotation (Line(points={{
          21,-76},{40,-76},{40,0},{60,0},{60,44.5}}, color={0,0,127}));
  connect(PVGroW.PDC, totPVPowDC.u[14:14]) annotation (Line(points={{21,
          -106},{40,-106},{40,0},{60,0},{60,43.5}}, color={0,0,127}));
  connect(lin8.terminal_p, terminal) annotation (Line(points={{-60,-110},
          {-60,0},{-100,0}}, color={0,120,120}));
  connect(totPVPowDC.y, totPowDC) annotation (Line(points={{81.7,50},{110,50}}, color={0,0,127}));
  connect(terminal, terminal) annotation (Line(points={{-100,0},{-100,0}}, color={0,120,120}));
  connect(totPVPowAC.y, totPowAC) annotation (Line(points={{81.7,-30},{110,-30}}, color={0,0,127}));
  connect(PVGroA.PAC, totPVPowAC.u[1:2]) annotation (Line(points={{21,
          96},{50,96},{50,-30},{60,-30},{60,-24.5}}, color={0,0,127}));
  connect(PVGroC.PAC, totPVPowAC.u[3:5]) annotation (Line(points={{21,
          66},{50,66},{50,-29.5},{60,-29.5},{60,-27.5}}, color={0,0,127}));
  connect(PVGroD.PAC, totPVPowAC.u[6:6]) annotation (Line(points={{21,
          36},{50,36},{50,-30},{60,-30},{60,-28.5}}, color={0,0,127}));
  connect(PVGroF.PAC, totPVPowAC.u[7:7]) annotation (Line(points={{21,6},
          {50,6},{50,-30},{60,-30},{60,-29.5}}, color={0,0,127}));
  connect(PVGroG.PAC, totPVPowAC.u[8:11]) annotation (Line(points={{21,
          -24},{50,-24},{50,-30},{60,-30},{60,-33.5}}, color={0,0,127}));
  connect(PVGroNC.PAC, totPVPowAC.u[12:12]) annotation (Line(points={{
          21,-54},{50,-54},{50,-29.5},{60,-29.5},{60,-34.5}}, color={0,
          0,127}));
  connect(PVGroSC.PAC, totPVPowAC.u[13:13]) annotation (Line(points={{
          21,-84},{50,-84},{50,-29.5},{60,-29.5},{60,-35.5}}, color={0,
          0,127}));
  connect(PVGroW.PAC, totPVPowAC.u[14:14]) annotation (Line(points={{21,
          -114},{50,-114},{50,-29.5},{60,-29.5},{60,-36.5}}, color={0,0,
          127}));
  connect(totPVPowAC.y, infBus.renEneGen) annotation (Line(points={{81.7,-30},{
          90,-30},{90,-90}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false,
        initialScale=0.1), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Bitmap(extent={{-88,-100},{96,84}},
        fileName="modelica://GreenVillage/Resources/Images/Subsystems/RenewableEnergy/PV/PhysicalModel/HGVPVs.png"),
        Text(
          extent={{-110,140},{130,110}},
          lineColor={0,0,255},
          textString="%name")}),
          Diagram(
        coordinateSystem(preserveAspectRatio=false,
        initialScale=0.1,
        extent={{-100,-120},{100,120}})),
    Documentation(info="<html>
<p>This model is describes the real topology of PVs in the HGV. All the parameters are set based on the real data, 
except the parameters of cable models, whose information is unavailable.
</p>
</html>",
  revisions="<html>
<ul>
<li>
January 8, 2018 by Yangyang Fu:<br/>
Revised the documentation
</li>
</ul>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"));
end HGVPVs;
