within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel;
package Utilities "Utility package "

annotation (Documentation(info="<html>
<p>This package includes the revised weather data reader model. After the revising, the solar data (GHI, DHI and DNI) could be read from input table.</p>
</html>"));
end Utilities;
