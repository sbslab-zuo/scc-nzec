within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel;
package Examples "Package with example models"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
  <p>This package contains examples for the use of the model <a href=\"modelica://
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.HGVPVs\">GreenVillage.
  Subsystems.RenewableEnergy.PV.PhysicalModel.HGVPVs</a>. </p>
</html>"));
end Examples;
