within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Examples;
model HGVPVs "Package with example models"
  extends Modelica.Icons.Example;
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.HGVPVs HGVPVs
    "HGV's whole PV groups"
    annotation (Placement(transformation(extent={{20,40},{40,60}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.
      Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
      filNam=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data"
    annotation (Placement(transformation(extent={{-20,60},{0,80}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrrSBS(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/Examples/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation (SBS Dec. 2017)"
    annotation (Placement(transformation(extent={{-80,60},{-60,80}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrrSBS(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/Examples/gloHorIrrSBS.txt"))
    "Global horizontal irradiation (SBS Dec. 2017)"
    annotation (Placement(transformation(extent={{-80,30},{-60,50}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrrSBS(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/Examples/dirNorIrrSBS.txt"))
    "Direct normal irradiation (SBS Dec. 2017)"
    annotation (Placement(transformation(extent={{-80,0},{-60,20}})));
  GreenVillage.Subsystems.Grid.Grid gri "Electrical grid"
    annotation (Placement(transformation(extent={{60,40},{80,60}})));
  Modelica.Blocks.Sources.CombiTimeTable powGenPV(
    tableOnFile=true,
    tableName="table",
    columns=2:10,
    fileName=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/Examples/powGenPV.txt"))
    "Power generation of HGV's PV groups (SBS Dec. 2017)"
    annotation (Placement(transformation(extent={{-80,-30},{-60,-10}})));
equation
  connect(weaDat.weaBus,HGVPVs. weaBus) annotation (Line(
      points={{0,70},{12,70},{12,58},{19.8,58}},
      color={255,204,51},
      thickness=0.5));
  connect(difHorIrrSBS.y[1], weaDat.HDifHor_in) annotation (Line(
        points={{-59,70},{-40,70},{-40,60.4},{-21,60.4}}, color={0,0,
          127}));
  connect(gloHorIrrSBS.y[1], weaDat.HGloHor_in) annotation (Line(
        points={{-59,40},{-40,40},{-40,58},{-21,58}}, color={0,0,127}));
  connect(dirNorIrrSBS.y[1], weaDat.HDirNor_in) annotation (Line(
        points={{-59,10},{-40,10},{-40,56.6},{-21,56.6}}, color={0,0,
          127}));
  connect(HGVPVs.terminal, gri.terminal) annotation (Line(points={{20,
          50},{12,50},{12,32},{70,32},{70,40.2}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
experiment(
      StartTime=0,
      StopTime=86400,
      Tolerance=1e-06),
    __Dymola_Commands(file=
        "modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/RenewableEnergy/PV/PhysicalModel/Examples/HGVPVs.mos"
        "Simulate and Plot"),
    Documentation(revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>", info="<html>
<p>This model is used to compare the real PV generation and model predicted PV generation. Solar data and PV generation data were measured during December 2017.</p>
</html>"));
end HGVPVs;
