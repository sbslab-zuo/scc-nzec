within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.Examples;
model PVGroupW
  "Compare real PV generation and model predicting PV generation with different solar irradiance input data"
  extends Modelica.Icons.Example;
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroWNREL(
    num=1,
    A={149.96},
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    eta={0.16},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={0.78539816339745}) "PV panel group W (Warehouse)"
    annotation (Placement(transformation(extent={{38,40},{58,60}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDatNREL(
    HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.
    RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
    filNam=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-20,60},{0,80}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrrNREL(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVGroupW/difHorIrrNREL.txt"))
    "Diffuse horizontal irradiation (NREL Jun. 2016)"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrrNREL(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVGroupW/gloHorIrrNREL.txt"))
    "Global horizontal irradiation (NREL Jun. 2016)"
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrrNREL(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVGroupW/dirNorIrrNREL.txt"))
    "Direct normal irradiation (NREL Jun. 2016)"
    annotation (
    Placement(transformation(extent={{-80,10},{-60,30}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVGroWSBS(
    num=1,
    A={149.96},
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    eta={0.16},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={0.78539816339745}) "PV panel group W (Warehouse)"
    annotation (Placement(transformation(extent={{38,-50},{58,-30}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3 weaDatSBS(
    HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.
    RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
    filNam=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-20,-30},{0,-10}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrrSBS(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVGroupW/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation (SBS Jun. 2017)"
    annotation (Placement(transformation(extent={{-80,-20},{-60,0}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrrSBS(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVGroupW/gloHorIrrSBS.txt"))
    "Global horizontal irradiation (SBS Jun. 2017)"
    annotation (Placement(transformation(extent={{-80,-50},{-60,-30}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrrSBS(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVGroupW/dirNorIrrSBS.txt"))
    "Direct normal irradiation (SBS Jun. 2017)"
    annotation (Placement(transformation(extent={{-80,-80},{-60,-60}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid griNREL(f=60, V=240)
    "Electrical grid model" annotation (Placement(transformation(
          extent={{-20,20},{0,40}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid griSBS(f=60, V=240)
    "Electrical grid model"
    annotation (Placement(transformation(extent={{-20,-70},{0,-50}})));
  Modelica.Blocks.Sources.CombiTimeTable powGenPVW(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVGroupW/powGenPVW.txt"))
    "Power generation of PV group W (SBS Jun. 2017)"
    annotation (Placement(transformation(extent={{40,-100},{60,-80}})));
equation
  connect(weaDatNREL.weaBus, PVGroWNREL.weaBus) annotation (Line(
      points={{0,70},{48,70},{48,59}},
      color={255,204,51},
      thickness=0.5));
  connect(difHorIrrNREL.y[1], weaDatNREL.HDifHor_in) annotation (
      Line(points={{-59,80},{-50,80},{-50,60.4},{-21,60.4}}, color=
          {0,0,127}));
  connect(gloHorIrrNREL.y[1], weaDatNREL.HGloHor_in) annotation (
      Line(points={{-59,50},{-50,50},{-50,58},{-21,58}}, color={0,0,
          127}));
  connect(dirNorIrrNREL.y[1], weaDatNREL.HDirNor_in) annotation (
      Line(points={{-59,20},{-40,20},{-40,56.6},{-21,56.6}}, color=
          {0,0,127}));
  connect(weaDatSBS.weaBus, PVGroWSBS.weaBus) annotation (Line(
      points={{0,-20},{48,-20},{48,-31}},
      color={255,204,51},
      thickness=0.5));
  connect(difHorIrrSBS.y[1], weaDatSBS.HDifHor_in) annotation (Line(
        points={{-59,-10},{-50,-10},{-50,-29.6},{-21,-29.6}}, color=
         {0,0,127}));
  connect(gloHorIrrSBS.y[1], weaDatSBS.HGloHor_in) annotation (Line(
        points={{-59,-40},{-50,-40},{-50,-32},{-21,-32}}, color={0,
          0,127}));
  connect(dirNorIrrSBS.y[1], weaDatSBS.HDirNor_in) annotation (Line(
        points={{-59,-70},{-40,-70},{-40,-33.4},{-21,-33.4}}, color=
         {0,0,127}));
  connect(griNREL.terminal, PVGroWNREL.terminal) annotation (Line(
        points={{-10,20},{-10,10},{20,10},{20,50},{38.2,50}}, color=
         {0,120,120}));
  connect(griSBS.terminal, PVGroWSBS.terminal) annotation (Line(points={{-10,-70},
          {-10,-80},{20,-80},{20,-40},{38.2,-40}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
    coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This example is built to check the diversities among real PV generation, PV generation predicted with the <a href=\"http://rredc.nrel.gov/solar/old_data/nsrdb/\">NREL solar data</a> (Jun. 2016) and HGV local solar data measured by SBS lab (Jun. 2017).</p>
</html>", revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"),
experiment(
      StartTime=0,
      StopTime=86400,
      Tolerance=1e-06),
    __Dymola_Commands(file=
        "modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVGroupW.mos"
        "Simulate and Plot"));
end PVGroupW;
