within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.Examples.Calibration;
model PVGroupG "Model that calibrates the PV group G "
  extends Modelica.Icons.Example;

  parameter String designVariables=Modelica.Utilities.Files.loadResource(
    "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupG/designVariables.txt");
  parameter Real eff(fixed=false) "Efficiency to be calibrated";
  parameter String objective="objective.txt";

  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels pv(
    num=4,
    A={17.93,19.56,13.04,9.78},
    pf={0.8,0.8,0.8,0.8},
    eta_DCAC={0.8,0.8,0.8,0.8},
    fAct={1,1,1,1},
    eta={eff,eff,eff,eff},
    til={0.17453292519943,0.087266462599716,0.087266462599716,0.17453292519943},
    lat={0.48051008702506,0.48051008702506,0.48051008702506,0.48051008702506},
    azi={0.78539816339745,0.78539816339745,-0.78539816339745,0.78539816339745})
    "PV panels"
    annotation (Placement(transformation(extent={{22,30},{42,50}})));

  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
      filNam=ModelicaServices.ExternalReferences.loadResource("Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-20,60},{0,80}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupG/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupG/gloHorIrrSBS.txt"))
    "Global horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupG/dirNorIrrSBS.txt"))
    "Direct normal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,10},{-60,30}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid gri(f=60, V=240)
    "Electrical grid model"
    annotation (Placement(transformation(extent={{-80,-20},{-60,0}})));
  Modelica.Blocks.Sources.CombiTimeTable powMea(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupG/powGenPVG.txt"))
    "Measured power generation of PV group G (measured by SBS in June 2017)"
    annotation (Placement(transformation(extent={{20,-60},{40,-40}})));
  GreenVillage.Calibration.Metric.NMBE nmbe "Normalized mean bias error"
    annotation (Placement(transformation(extent={{60,-20},{80,0}})));
  Modelica.Blocks.Math.MultiSum totPVPowAC(      y(unit="W"), nu=4)
    "Total PV power could be consumed by users"
    annotation (Placement(transformation(extent={{60,34},{80,54}})));
initial algorithm
  if (objective <> "") then
     Modelica.Utilities.Files.removeFile(objective);
  end if;
  eff :=Modelica.Utilities.Examples.readRealParameter(designVariables, "eff");

algorithm
  when terminal() then
    Modelica.Utilities.Streams.print("obj = " + realString(number=nmbe.y, minimumWidth=1, precision=16), objective);
  end when;

equation
  connect(difHorIrr.y[1], weaDat.HDifHor_in) annotation (Line(points={{-59,80},
          {-32,80},{-32,60.4},{-21,60.4}},
                                        color={0,0,127}));
  connect(gloHorIrr.y[1], weaDat.HGloHor_in) annotation (Line(points={{-59,50},
          {-36,50},{-36,58},{-21,58}},color={0,0,127}));
  connect(dirNorIrr.y[1], weaDat.HDirNor_in) annotation (Line(points={{-59,20},
          {-40,20},{-40,56.6},{-21,56.6}},color={0,0,127}));
  connect(weaDat.weaBus, pv.weaBus) annotation (Line(
      points={{0,70},{32,70},{32,49}},
      color={255,204,51},
      thickness=0.5));
  connect(gri.terminal, pv.terminal) annotation (Line(points={{-70,-20},{
          -70,-40},{0,-40},{0,40},{22.2,40}},
                                         color={0,120,120}));
  connect(powMea.y[1], nmbe.u2) annotation (Line(points={{41,-50},{48,-50},
          {48,-16},{58,-16}}, color={0,0,127}));
  connect(totPVPowAC.y, nmbe.u1) annotation (Line(points={{81.7,44},{86,44},{86,
          20},{48,20},{48,-4},{58,-4}}, color={0,0,127}));
  connect(pv.PAC, totPVPowAC.u[1:4]) annotation (Line(points={{43,36},{52,36},{
          52,38.75},{60,38.75}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model is built to calibrate the PV group G. The parameter of module efficiency is optimized by <a href=\"https://simulationresearch.lbl.gov/GO/\">GenOpt</a> to make the model calculated data match measured data to the greastest extent. In this model, measure data in December, 2017 is used.</p>
</html>", revisions="<html>
<ul>
<li>January 17, 2018 by Danlin Hou:<br>First implementation </li>
</ul>
</html>"));
end PVGroupG;
