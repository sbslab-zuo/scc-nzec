within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.Examples;
package Calibration "Package that contains calibrated PV models for the HGV"
  extends Modelica.Icons.ExamplesPackage;

end Calibration;
