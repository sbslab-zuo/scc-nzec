within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC;
package Examples "Package with example models"
  extends Modelica.Icons.ExamplesPackage;



  annotation (Documentation(info="<html>
<p>This package contains examples for the use of the model <a href=\"modelica://GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels\">GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels</a>. </p>
</html>",
revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"));
end Examples;
