within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.Examples.Validation;
model PVGroupC "Model that validates the PV group C"
  extends Modelica.Icons.Example;

  parameter String designVariables=Modelica.Utilities.Files.loadResource(
    "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupC/designVariables.txt");
  parameter Real eff(fixed=false) "Efficiency to be calibrated";
  parameter String objective="objective.txt";

  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels pvOpt(
    num=3,
    A={46.08,38.04,25.60},
    pf={0.8,0.8,0.8},
    eta_DCAC={0.8,0.8,0.8},
    fAct={1,1,1},
    eta={eff,eff,eff},
    til={0.087266462599716,0.087266462599716,0.5235987755983},
    lat={0.48051008702506,0.48051008702506,0.48051008702506},
    azi={-0.78539816339745,-0.78539816339745,0.78539816339745})
    "PV panels with optimized eff"
    annotation (Placement(transformation(extent={{0,-20},{20,0}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVBef(
    num=3,
    A={46.08,38.04,25.60},
    pf={0.8,0.8,0.8},
    eta_DCAC={0.8,0.8,0.8},
    fAct={1,1,1},
    eta={0.149,0.149,0.149},
    til={0.087266462599716,0.087266462599716,0.5235987755983},
    lat={0.48051008702506,0.48051008702506,0.48051008702506},
    azi={-0.78539816339745,-0.78539816339745,0.78539816339745})
    "PV panels with previous eff"
    annotation (Placement(transformation(extent={{0,40},{20,60}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor, filNam=
        ModelicaServices.ExternalReferences.loadResource("Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-40,60},{-20,80}})));
  GreenVillage.Calibration.Metric.NMBE nmbeOpt "Normalized mean bias error"
    annotation (Placement(transformation(extent={{100,-40},{120,-20}})));
  GreenVillage.Calibration.Metric.NMBE nmbeBef "Normalized mean bias error"
    annotation (Placement(transformation(extent={{100,20},{120,40}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Validation/PVGroupC/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Validation/PVGroupC/gloHorIrrSBS.txt"))
    "Global horizontal irradiation"
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Validation/PVGroupC/dirNorIrrSBS.txt"))
    "Direct normal irradiation"
    annotation (Placement(transformation(extent={{-80,10},{-60,30}})));
  Modelica.Blocks.Sources.CombiTimeTable powMea(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Validation/PVGroupC/powGenPVC.txt"))
    "Measured power generation of PV group C (SBS Jul. 2017)"
    annotation (Placement(transformation(extent={{20,-80},{40,-60}})));
  Modelica.Blocks.Math.MultiSum totACOpt(y(unit="W"), nu=3)
    "Total PV power could be consumed by users"
    annotation (Placement(transformation(extent={{40,-20},{60,0}})));
  Modelica.Blocks.Math.MultiSum totACBef(y(unit="W"), nu=3)
    "Total PV power could be consumed by users"
    annotation (Placement(transformation(extent={{40,40},{60,60}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid gri(f=60, V=240)
    "Electrical grid model"
    annotation (Placement(transformation(extent={{-80,-20},{-60,0}})));

initial algorithm
  if (objective <> "") then
     Modelica.Utilities.Files.removeFile(objective);
  end if;
  eff :=Modelica.Utilities.Examples.readRealParameter(designVariables, "eff");

algorithm
  when terminal() then
    Modelica.Utilities.Streams.print("obj = " + realString(
      number=nmbeOpt.y,
      minimumWidth=1,
      precision=16), objective);
  end when;

equation
  connect(difHorIrr.y[1], weaDat.HDifHor_in)
    annotation (Line(points={{-59,80},{-50,80},{-50,60.4},{-41,60.4}},color={0,0,127}));
  connect(gloHorIrr.y[1], weaDat.HGloHor_in)
    annotation (Line(points={{-59,50},{-50,50},{-50,58},{-41,58}},color={0,0,127}));
  connect(dirNorIrr.y[1], weaDat.HDirNor_in)
    annotation (Line(points={{-59,20},{-50,20},{-50,56.6},{-41,56.6}},color={0,0,127}));
  connect(weaDat.weaBus, pvOpt.weaBus)
    annotation (Line(points={{-20,70},{-8,70},{-8,20},{10,20},{10,-1}},color={255,204,51}, thickness=0.5));
  connect(gri.terminal, pvOpt.terminal)
    annotation (Line(points={{-70,-20},{-70,-40},{-20,-40},{-20,-10},{0.2,-10}},color={0,120,120}));
  connect(powMea.y[1], nmbeOpt.u2)
    annotation (Line(points={{41,-70},{80,-70},{80,-36},{98,-36}},color={0,0,127}));
  connect(totACOpt.y, nmbeOpt.u1)
    annotation (Line(points={{61.7,-10},{88,-10},{88,-24},{98,-24}},color={0,0,127}));
  connect(pvOpt.PAC, totACOpt.u[1:3])
    annotation (Line(points={{21,-14},{40,-14},{40,-14.6667}},color={0,0,127}));
  connect(totACBef.y,nmbeBef. u1)
    annotation (Line(points={{61.7,50},{80,50},{80,36},{98,36}},color={0,0,127}));
  connect(nmbeBef.u2, nmbeOpt.u2)
    annotation (Line(points={{98,24},{80,24},{80,-36},{98,-36}},color={0,0,127}));
  connect(PVBef.terminal, pvOpt.terminal)
    annotation (Line(points={{0.2,50},{-20,50},{-20,-10},{0.2,-10}},color={0,120,120}));
  connect(weaDat.weaBus, PVBef.weaBus)
    annotation (Line(points={{-20,70},{-8,70},{-8,59},{10,59}},color={255,204,51},thickness=0.5));
  connect(PVBef.PAC, totACBef.u[1:3])
    annotation (Line(points={{21,46},{30,46},{30,45.3333},{40,45.3333}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{140,100}})),
    Documentation(info="<html>
    <p>This model is built to validate the PV group C. 
    The optimized parameter of module efficiency generated by <a href=\"https://simulationresearch.lbl.gov/GO/\">GenOpt</a> is used.
    In order to compare the improvement, the PV panels with previous value of module efficiency is also set in this model.</p>
    <p>For the solar data and measured PV generation data, it&apos;s from measure data in July, 2017.</p>
    </html>", revisions="<html>
    </ul>
    <ul>
    <li>
    January 14, 2018 by Danlin Hou:<br/>
    First implementation
    </li>
    </ul>
    </html>"));
end PVGroupC;
