within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.Examples.Validation;
model PVGroupSC "Model that validates the PV group SC "
  extends Modelica.Icons.Example;

  parameter String designVariables=Modelica.Utilities.Files.loadResource(
    "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupSC/designVariables.txt");
  parameter Real eff(fixed=false) "Efficiency to be calibrated";
  parameter String objective="objective.txt";

  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVOpt(
    num=1,
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    eta={eff},
    A={96},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={-0.78539816339745}) "PV panels with optimized eff"
    annotation (Placement(transformation(extent={{0,-20},{20,0}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVBef(
    num=1,
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    eta={0.149},
    A={96},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={-0.78539816339745})
    "PV panels with previous eff"
    annotation (Placement(transformation(extent={{0,40},{20,60}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
    filNam=ModelicaServices.ExternalReferences.loadResource("Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-40,60},{-20,80}})));
  GreenVillage.Calibration.Metric.NMBE nmbeOpt "Normalized mean bias error"
    annotation (Placement(transformation(extent={{60,-30},{80,-10}})));
  GreenVillage.Calibration.Metric.NMBE nmbeBef "Normalized mean bias error"
    annotation (Placement(transformation(extent={{60,30},{80,50}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Validation/PVGroupSC/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Validation/PVGroupSC/gloHorIrrSBS.txt"))
    "Global horizontal irradiation"
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Validation/PVGroupSC/dirNorIrrSBS.txt"))
    "Direct normal irradiation"
    annotation (Placement(transformation(extent={{-80,10},{-60,30}})));
  Modelica.Blocks.Sources.CombiTimeTable powMea(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Validation/PVGroupSC/powGenPVSC.txt"))
    "Measured power generation of PV group SC (SBS JuL. 2017)"
    annotation (Placement(transformation(extent={{0,-60},{20,-40}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid gri(f=60, V=240)
    "Electrical grid model"
    annotation (Placement(transformation(extent={{-80,-20},{-60,0}})));

initial algorithm
  if (objective <> "") then
     Modelica.Utilities.Files.removeFile(objective);
  end if;
  eff :=Modelica.Utilities.Examples.readRealParameter(designVariables, "eff");

algorithm
  when terminal() then
    Modelica.Utilities.Streams.print("obj = " + realString(
      number=nmbeOpt.y,
      minimumWidth=1,
      precision=16), objective);
  end when;

equation
  connect(difHorIrr.y[1], weaDat.HDifHor_in)
   annotation (Line(points={{-59,80},{-50,80},{-50,60.4},{-41,60.4}},color={0,0,127}));
  connect(gloHorIrr.y[1], weaDat.HGloHor_in)
   annotation (Line(points={{-59,50},{-50,50},{-50,58},{-41,58}}, color={0,0,127}));
  connect(dirNorIrr.y[1], weaDat.HDirNor_in)
   annotation (Line(points={{-59,20},{-50,20},{-50,56.6},{-41,56.6}}, color={0,0,127}));
  connect(weaDat.weaBus, PVOpt.weaBus) annotation (Line(points={{-20,70},{-8,70},{-8,30},{10,30},{10,-1}},color={255,204,51},thickness=0.5));
  connect(gri.terminal, PVOpt.terminal) annotation (Line(points={{-70,-20},{-70,-40},{-20,-40},{-20,-10},{0.2,-10}}, color={0,120,120}));
  connect(powMea.y[1], nmbeOpt.u2) annotation (Line(points={{21,-50},{40,-50},{
          40,-26},{58,-26}}, color={0,0,127}));
  connect(PVOpt.PAC[1], nmbeOpt.u1)
    annotation (Line(points={{21,-14},{58,-14}}, color={0,0,127}));
  connect(PVBef.PAC[1],nmbeBef. u1)
   annotation (Line(points={{21,46},{58,46}},color={0,0,127}));
  connect(weaDat.weaBus, PVBef.weaBus)
   annotation (Line(points={{-20,70},{-8,70},{-8,59},{10,59}},color={255,204,51},thickness=0.5));
  connect(PVBef.terminal, PVOpt.terminal)
   annotation (Line(points={{0.2,50},{-20,50},{-20,-10},{0.2,-10}},color={0,120,120}));
  connect(nmbeBef.u2, nmbeOpt.u2) annotation (Line(points={{58,34},{40,34},{40,
          -26},{58,-26}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
    <p>This model is built to validate the PV group SC. 
    The optimized parameter of module efficiency generated by <a href=\"https://simulationresearch.lbl.gov/GO/\">GenOpt</a> is used.
    In order to compare the improvement, the PV panels with previous value of module efficiency is also set in this model.</p>
    <p>For the solar data and measured PV generation data, it&apos;s from measure data in July, 2017.</p>
    </html>", revisions="<html>
    </ul>
    <ul>
    <li>
    January 14, 2018 by Danlin Hou:<br/>
    First implementation
    </li>
    </ul>
    </html>"));
end PVGroupSC;
