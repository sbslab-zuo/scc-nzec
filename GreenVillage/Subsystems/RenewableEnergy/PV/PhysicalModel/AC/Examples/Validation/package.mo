within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.Examples;
package Validation "Package that contains validated PV models for the HGV"
  extends Modelica.Icons.ExamplesPackage;

end Validation;
