within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.Examples;
model PVPanels "Example that illustrate the use of PVPanels "
  extends Modelica.Icons.Example;
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels PVPan(til={
    0.26179938779915,0.26179938779915}, V_nominal=240) "PV Panels"
    annotation (Placement(transformation(extent={{42,-10},{62,10}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.
    Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
    filNam=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-20,60},{0,80}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/difHorIrr.txt"))
    "Diffuse horizontal irradiation"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/gloHorIrr.txt"))
    "Global horizontal irradiation"
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/dirNorIrr.txt"))
    "Direct normal irradiation"
    annotation (Placement(transformation(extent={{-80,10},{-60,30}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid gri(
    f=60, V=240)
    "Electrical grid"
    annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-30,0})));

  Buildings.Electrical.AC.OnePhase.Loads.Inductive indLoa(
    P_nominal=-2000,
    V_nominal=120,
    mode=Buildings.Electrical.Types.Load.VariableZ_y_input)
    "Inductive load"
    annotation (Placement(transformation(extent={{-20,-70},{-40,-50}})));
  Modelica.Blocks.Sources.Constant loa(k=0.5)
    "Load consumption"
    annotation (Placement(transformation(extent={{-80,-70},{-60,-50}})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin2(
    P_nominal=5000,
    l=50,
    V_nominal=240)
    "Cable"
     annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={24,-20})));
  Buildings.Electrical.AC.OnePhase.Lines.Line lin1(
    P_nominal=5000,
    l=50,
    V_nominal=240)
    "Cable"
     annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-16,-20})));
equation
  connect(difHorIrr.y[1], weaDat.HDifHor_in) annotation (Line(points={{-59,80},{-32,
          80},{-32,60.4},{-21,60.4}}, color={0,0,127}));
  connect(gloHorIrr.y[1], weaDat.HGloHor_in) annotation (Line(points={{-59,50},{-32,
          50},{-32,58},{-21,58}}, color={0,0,127}));
  connect(dirNorIrr.y[1], weaDat.HDirNor_in) annotation (Line(points={{-59,20},{-28,
          20},{-28,56.6},{-21,56.6}}, color={0,0,127}));
  connect(weaDat.weaBus, PVPan.weaBus) annotation (Line(
      points={{0,70},{52,70},{52,9}},
      color={255,204,51},
      thickness=0.5));
  connect(loa.y, indLoa.y)
  annotation (Line(points={{-59,-60},{-40,-60}}, color={0,0,127}));
  connect(indLoa.terminal, lin2.terminal_p) annotation (Line(points=
         {{-20,-60},{0,-60},{0,-20},{14,-20}}, color={0,120,120}));
  connect(lin2.terminal_n, PVPan.terminal) annotation (Line(points=
          {{34,-20},{40,-20},{40,0},{42.2,0}}, color={0,120,120}));
  connect(gri.terminal, lin1.terminal_p) annotation (Line(points={{
          -30,-10},{-30,-20},{-26,-20}}, color={0,120,120}));
  connect(lin1.terminal_n, indLoa.terminal) annotation (Line(points=
         {{-6,-20},{0,-20},{0,-60},{-20,-60}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
    coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This example tests the vectorized PV panel model 
<a href=\"modelica://GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels\">
GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.PVPanels</a>.
In this example, the grid and the PV panels together provide power for the load.
</p>
<p>
The weather data as the input of the PV panel model is read from external files.
The loads are addressed by the grid when the PV panels have insufficient generation. If the PV panels 
generate suffice power, then the extra power flows to the grid.</p>
</html>", revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"),
experiment(
      StartTime=0,
      StopTime=86400,
      Tolerance=1e-06),
    __Dymola_Commands(
  file="modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels.mos"
        "Simulate and Plot"));
end PVPanels;
