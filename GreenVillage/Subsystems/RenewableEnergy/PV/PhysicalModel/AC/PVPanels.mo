within GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC;
model PVPanels "Vectorized model for multiple PV panels "
  replaceable package PhaseSystem =
    Buildings.Electrical.PhaseSystems.OnePhase
    "Phase system"
    annotation (choicesAllMatching=true);
  parameter Integer num( min=1) =  2 "Number of PV panels";
  parameter Modelica.SIunits.Area A[num] = {2,9} "Area of PV panels";
  parameter Real pf[num]( min=0, max=1) =  {0.9,0.8} "Power factor"
    annotation (Dialog(group="AC-Conversion"));
  parameter Modelica.SIunits.Efficiency eta_DCAC[num] = {0.8,0.8}
    "Efficiency of DC/AC conversion"
    annotation (Dialog(group="AC-Conversion"));
  parameter Real fAct[num](each min=0, each max=1, each unit="1") = {0.9,0.8}
    "Fraction of surface area with active solar cells";
  parameter Modelica.SIunits.Efficiency eta[num] = {0.4,0.5}
    "Module conversion efficiency";
  parameter Modelica.SIunits.Angle til[num]( each displayUnit="deg") = {0.26,0.26}
    "Surface tilt"
    annotation (Dialog(group="Orientation"));
  parameter Modelica.SIunits.Angle lat[num]( each displayUnit="deg") = {0.47,0.47}
    "Latitude"
    annotation (Dialog(group="Orientation"));
  parameter Modelica.SIunits.Angle azi[num]( each displayUnit="deg") = {0.52,0.26}
    "Surface azimuth"
    annotation (Dialog(group="Orientation"));
  parameter Boolean linearized=false "If =true, linearize the load";
  parameter Modelica.SIunits.Voltage V_nominal(start=110) = 240
    "Nominal voltage (V_nominal >= 0)"
    annotation (Dialog(group="Nominal conditions"));
  Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented pv[num](
    A=A,
    pf=pf,
    eta_DCAC=eta_DCAC,
    fAct=fAct,
    eta=eta,
    til=til,
    lat=lat,
    azi=azi,
    each linearized=linearized,
    each V_nominal=V_nominal)
    "PV panels with number n"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus "Weather data"
    annotation (
     Placement(transformation(extent={{-106,50},{-86,70}}),iconTransformation(
          extent={{-10,80},{10,100}})));
  Modelica.Blocks.Interfaces.RealOutput PDC[num](
    quantity="Power",
    unit="W",
    min=0) "Generated power" annotation (Placement(transformation(
          extent={{100,30},{120,50}})));
  Modelica.Blocks.Sources.RealExpression powAC[num](y=PDC .* eta_DCAC)
    "AC power transformed from DC"
    annotation (Placement(transformation(extent={{0,-50},{20,-30}})));
  Modelica.Blocks.Interfaces.RealOutput PAC[num](
    quantity="Power",
    unit="W",
    min=0) "Generated power" annotation (Placement(transformation(
          extent={{100,-50},{120,-30}})));

  Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_p terminal(
    redeclare final package PhaseSystem = PhaseSystem)
    "Generalized terminal"
    annotation (Placement(transformation(extent={{-108,-10},{-88,10}})));
equation
  for i in 1:num loop
    connect(pv[i].terminal, terminal) annotation (Line(points={{-10,0},{-98,0}}, color={0,120,120}));
    connect(pv[i].weaBus, weaBus) annotation (Line(
      points={{0,9},{0,60},{-96,60}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
    connect(pv[i].P, PDC[i]) annotation (Line(points={{11,7},{20,7},{
            20,40},{110,40}}, color={0,0,127}));
  end for;
  connect(powAC.y, PAC)
    annotation (Line(points={{21,-40},{110,-40}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Line(points={{-88,0},{-57,0}}, color={0,0,0}),
        Polygon(
          points={{-78,-52},{-30,63},{80,63},{31,-52},{-78,-52}},
          smooth=Smooth.None,
          fillColor={205,203,203},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None,
          lineColor={0,0,0}),
        Polygon(
          points={{-67,-45},{-55,-19},{-32,-19},{-43,-45},{-67,-45}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-51,-9},{-39,17},{-16,17},{-27,-9},{-51,-9}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-36,27},{-24,53},{-1,53},{-12,27},{-36,27}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-34,-45},{-22,-19},{1,-19},{-10,-45},{-34,-45}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-18,-9},{-6,17},{17,17},{6,-9},{-18,-9}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-3,27},{9,53},{32,53},{21,27},{-3,27}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{-1,-45},{11,-19},{34,-19},{23,-45},{-1,-45}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{15,-9},{27,17},{50,17},{39,-9},{15,-9}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Polygon(
          points={{30,27},{42,53},{65,53},{54,27},{30,27}},
          smooth=Smooth.None,
          fillColor={6,13,150},
          fillPattern=FillPattern.Solid,
          pattern=LinePattern.None),
        Text(
          extent={{-140,-94},{160,-54}},
          lineColor={0,0,0},
          textString="%name"),
        Text(
          extent={{76,45},{98,19}},
          lineColor={0,0,127},
          textString="PDC"),
        Text(
          extent={{74,-13},{96,-39}},
          lineColor={0,0,127},
          textString="PAC")}),
        Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This model implements a vectorized PV panels. The number of PV panel can be specified by the parameter
<code>num</code>. Note that these PV panels are connected in parallel with each other. Detailed mathematical model
can be found in <a href=\"modelica://Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented\">
Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented</a>.
</p>
</html>",
revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"));
end PVPanels;
