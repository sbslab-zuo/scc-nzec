within GreenVillage.Subsystems.RenewableEnergy.PV;
package PhysicalModel "Physical Model of PV generation"

  annotation (Documentation(revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>", info="<html>
<p>This package is about physical models of PV generation. </p>
</html>"));
end PhysicalModel;
