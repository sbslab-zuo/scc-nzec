within GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.Examples;
model HGVPVs
  "Example that shows the data driven model for all PVs in HGV"
  extends Modelica.Icons.Example;

  GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.HGVPVs annPV
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid gri(f=60, V=240)
    "Electrical grid model"
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=90,
        origin={70,0})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/dirNorIrrSBS.txt"))
    "Direct normal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,30},{-60,50}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/gloHorIrrSBS.txt"))
    "Global horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
      filNam=ModelicaServices.ExternalReferences.loadResource("Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-20,40},{0,60}})));
equation
  connect(difHorIrr.y[1],weaDat. HDifHor_in) annotation (Line(points={{-59,80},
          {-40,80},{-40,40.4},{-21,40.4}}, color={0,0,127}));
  connect(dirNorIrr.y[1],weaDat. HDirNor_in) annotation (Line(points={{-59,40},
          {-40,40},{-40,36.6},{-21,36.6}}, color={0,0,127}));
  connect(gloHorIrr.y[1],weaDat. HGloHor_in) annotation (Line(points={{-59,0},{
          -40,0},{-40,38},{-21,38}}, color={0,0,127}));
  connect(weaDat.weaBus, annPV.weaBus) annotation (Line(
      points={{0,50},{21.4,50},{21.4,9}},
      color={255,204,51},
      thickness=0.5));
  connect(gri.terminal, annPV.terminal)
    annotation (Line(points={{60,4.44089e-16},{60,0},{39.2,0}},
                                                        color={0,120,120}));
  annotation (__Dymola_Commands(file="modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/RenewableEnergy/PV/DataDrivenModel/Examples/HGVPVs.mos"
        "Simulate and Plot"));
end HGVPVs;
