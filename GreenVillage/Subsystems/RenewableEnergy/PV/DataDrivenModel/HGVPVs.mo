within GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel;
model HGVPVs "HGV PV with all data driven models"

  extends
    GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.BaseClasses.PartialANNElectricalInterface(
    redeclare package PhaseSystem = Buildings.Electrical.PhaseSystems.OnePhase,
    redeclare Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_p terminal);
  parameter Boolean linearized=false
    "If =true, introduce a linearization in the load";

  parameter Modelica.SIunits.Time startTime=0 "First sample time instant";
  parameter Modelica.SIunits.Time samplePeriod = 60 "Sample period of component";


  GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.ThreeInputs F(
    V_nominal=V_nominal,
    samplePeriod=samplePeriod,
    linearized=linearized,
    startTime=startTime,
    final moduleName="ANNModel_F",
    final functionName="ANN")
                        "Building F"
    annotation (Placement(transformation(extent={{-50,-46},{-30,-26}})));

  GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.ThreeInputs G(
    V_nominal=V_nominal,
    samplePeriod=samplePeriod,
    linearized=linearized,
    startTime=startTime,
    final moduleName="ANNModel_G",
    final functionName="ANN") "Building G"
    annotation (Placement(transformation(extent={{-50,-86},{-30,-66}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{-90,70},{-70,90}}),iconTransformation(extent={{-96,80},
            {-76,100}})));


  ThreeInputs A(
    V_nominal=V_nominal,
    samplePeriod=samplePeriod,
    linearized=linearized,
    startTime=startTime,
    final functionName="ANN",
    final moduleName="ANNModel_A") "Building A"
    annotation (Placement(transformation(extent={{-50,74},{-30,94}})));
  ThreeInputs C(
    V_nominal=V_nominal,
    samplePeriod=samplePeriod,
    linearized=linearized,
    startTime=startTime,
    final functionName="ANN",
    final moduleName="ANNModel_C") "Building C"
    annotation (Placement(transformation(extent={{-50,34},{-30,54}})));
  ThreeInputs D(
    V_nominal=V_nominal,
    samplePeriod=samplePeriod,
    linearized=linearized,
    startTime=startTime,
    final functionName="ANN",
    final moduleName="ANNModel_D") "Building D"
    annotation (Placement(transformation(extent={{-50,-6},{-30,14}})));
  ThreeInputs SC(
    V_nominal=V_nominal,
    samplePeriod=samplePeriod,
    linearized=linearized,
    startTime=startTime,
    final functionName="ANN",
    final moduleName="ANNModel_SC") "Building SC"
    annotation (Placement(transformation(extent={{10,50},{30,70}})));
  ThreeInputs NC(
    V_nominal=V_nominal,
    samplePeriod=samplePeriod,
    linearized=linearized,
    startTime=startTime,
    final functionName="ANN",
    final moduleName="ANNModel_NC") "Building NC"
    annotation (Placement(transformation(extent={{10,14},{30,34}})));
  ThreeInputs W(
    V_nominal=V_nominal,
    samplePeriod=samplePeriod,
    linearized=linearized,
    startTime=startTime,
    final functionName="ANN",
    final moduleName="ANNModel_W") "Building W"
    annotation (Placement(transformation(extent={{10,-66},{30,-46}})));
  Buildings.Electrical.AC.OnePhase.Sensors.GeneralizedSensor sen
    annotation (Placement(transformation(extent={{50,-10},{70,10}})));
equation
  connect(weaBus, F.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,-28},{-48.6,-28},{-48.6,-27}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, G.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,-67},{-48.6,-67}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, A.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,93},{-48.6,93}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, C.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,53},{-48.6,53}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, D.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,13},{-48.6,13}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, SC.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,68},{11.4,68},{11.4,69}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, NC.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,68},{0,68},{0,33},{11.4,33}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, W.weaBus) annotation (Line(
      points={{-80,80},{-60,80},{-60,68},{0,68},{0,-47},{11.4,-47}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(A.terminal, sen.terminal_n) annotation (Line(points={{-30.8,84},{-12,
          84},{-12,0},{50,0}},
                            color={0,120,120}));
  connect(C.terminal, sen.terminal_n) annotation (Line(points={{-30.8,44},{-12,
          44},{-12,0},{50,0}},
                            color={0,120,120}));
  connect(D.terminal, sen.terminal_n) annotation (Line(points={{-30.8,4},{-12,4},
          {-12,0},{50,0}},  color={0,120,120}));
  connect(F.terminal, sen.terminal_n) annotation (Line(points={{-30.8,-36},{-12,
          -36},{-12,0},{50,0}},
                            color={0,120,120}));
  connect(G.terminal, sen.terminal_n) annotation (Line(points={{-30.8,-76},{-12,
          -76},{-12,0},{50,0}},
                            color={0,120,120}));
  connect(SC.terminal, sen.terminal_n) annotation (Line(points={{29.2,60},{40,
          60},{40,0},{50,0}},
                            color={0,120,120}));
  connect(NC.terminal, sen.terminal_n) annotation (Line(points={{29.2,24},{40,
          24},{40,0},{50,0}},
                            color={0,120,120}));
  connect(W.terminal, sen.terminal_n) annotation (Line(points={{29.2,-56},{40,
          -56},{40,0},{50,0}},   color={0,120,120}));
  connect(sen.terminal_p, terminal)
    annotation (Line(points={{70,0},{92,0}},    color={0,120,120}));
  annotation (defaultComponentName="annPV",
    Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Bitmap(extent={{-102,-100},{102,100}},
          fileName="modelica://GreenVillage/Resources/Icons/ann.jpeg"),
        Text(
          extent={{-152,112},{148,152}},
          lineColor={0,0,255},
          textString="%name")}));
end HGVPVs;
