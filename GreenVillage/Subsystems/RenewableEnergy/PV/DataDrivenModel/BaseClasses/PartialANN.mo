within GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.BaseClasses;
partial model PartialANN "Partial ANN model with electricity package "

  extends
    GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.BaseClasses.PartialANNElectricalInterface;

  parameter Boolean linearized=false
    "If =true, introduce a linearization in the load";

   replaceable Buildings.Electrical.AC.OnePhase.Loads.Capacitive load(
    mode=Buildings.Electrical.Types.Load.VariableZ_P_input,
    final pf=1,
    final V_nominal=V_nominal,
    final P_nominal=0,
    final linearized=linearized) "Load model"
    annotation (Placement(transformation(extent={{62,-10},{42,10}})));
equation
  connect(load.terminal, terminal)
    annotation (Line(points={{62,0},{92,0}},    color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PartialANN;
