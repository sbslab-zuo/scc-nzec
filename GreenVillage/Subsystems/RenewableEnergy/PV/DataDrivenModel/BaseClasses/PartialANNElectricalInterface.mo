within GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.BaseClasses;
partial model PartialANNElectricalInterface
  "Partial ANN model with electrical interface"

  replaceable package PhaseSystem =
      Buildings.Electrical.PhaseSystems.PartialPhaseSystem constrainedby
    Buildings.Electrical.PhaseSystems.PartialPhaseSystem "Phase system"
    annotation (choicesAllMatching=true);
  parameter Modelica.SIunits.Voltage V_nominal(min=0, start=110)
    "Nominal voltage (V_nominal >= 0)"  annotation(Evaluate=true, Dialog(group="Nominal conditions"));

  replaceable Buildings.Electrical.Interfaces.Terminal terminal(
    redeclare final package PhaseSystem = PhaseSystem) "Generalized terminal"
    annotation (Placement(transformation(extent={{82,-10},{102,10}})));

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PartialANNElectricalInterface;
