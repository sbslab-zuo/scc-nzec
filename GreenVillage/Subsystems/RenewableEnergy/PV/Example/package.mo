within GreenVillage.Subsystems.RenewableEnergy.PV;
package Example "Compare the physical and ANN model"
 extends Modelica.Icons.ExamplesPackage;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Example;
