within GreenVillage.Subsystems.RenewableEnergy.PV.Example;
model Comparison "Compare the physical and ANN model"
  import GreenVillage;
 extends Modelica.Icons.Example;

   parameter String pathFile1=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVInput201408/DHI.txt");
   parameter String pathFile2=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVInput201408/GHI.txt");
   parameter String pathFile3=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVInput201408/DNI.txt");

  Modelica.Blocks.Sources.CombiTimeTable DHI(
    tableName="table1",
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=pathFile1)
    annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
  Modelica.Blocks.Sources.CombiTimeTable GHI(
    tableName="table1",
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=pathFile2) "Horizontal global radiation"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Sources.CombiTimeTable DNI(
    tableName="table1",
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=pathFile3) "Direct normall radiation"
    annotation (Placement(transformation(extent={{-100,-40},{-80,-20}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(
    computeWetBulbTemperature=false,
    HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
    filNam=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data model"
    annotation (Placement(transformation(extent={{-58,6},{-38,26}})));

  GreenVillage.Subsystems.Grid.Grid Grid "Electrical grid model"
    annotation (Placement(transformation(extent={{20,0},{40,20}})));
equation
  connect(DHI.y[1],weaDat. HDifHor_in) annotation (Line(points={{-79,30},{-68,30},
          {-68,6.4},{-59,6.4}},             color={0,0,127}));
  connect(GHI.y[1],weaDat. HGloHor_in) annotation (Line(points={{-79,0},{-68,0},
          {-68,4},{-59,4}},            color={0,0,127}));
  connect(DNI.y[1],weaDat. HDirNor_in) annotation (Line(points={{-79,-30},{-66,-30},
          {-66,0},{-66,2.6},{-59,2.6}},    color={0,0,127}));
  connect(weaDat.weaBus, physical.WeaBus) annotation (Line(
      points={{-38,16},{-20,16},{-20,-23.5},{0.111111,-23.5}},
      color={255,204,51},
      thickness=0.5));
  connect(Grid.terminal, physical.terminal) annotation (Line(points={{30,0.2},
          {30,-46},{10.1111,-46},{10.1111,-40}},
                                             color={0,120,120}));
  connect(Grid.terminal, ANN.terminal) annotation (Line(points={{30,0.2},{30,0.2},
          {30,-46},{50,-46},{50,-40}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file="Resources/Scripts/PVComparison.mos"
        "simulate and plot"));
end Comparison;
