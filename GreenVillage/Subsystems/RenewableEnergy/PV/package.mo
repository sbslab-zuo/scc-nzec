within GreenVillage.Subsystems.RenewableEnergy;
package PV "PV subsystem"

  annotation (Documentation(revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>", info="<html>
<p>This package is built for PV subsystem. Two kinds of PV models are established. The first kind of model is physical model. The second kind is about ANN model.</p>
</html>"));
end PV;
