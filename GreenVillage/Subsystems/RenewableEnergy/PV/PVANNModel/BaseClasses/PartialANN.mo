within GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel.BaseClasses;
partial model PartialANN "Partial ANN model with electricity package "

  replaceable package PhaseSystem =
      Buildings.Electrical.PhaseSystems.PartialPhaseSystem constrainedby
    Buildings.Electrical.PhaseSystems.PartialPhaseSystem "Phase system"
    annotation (choicesAllMatching=true);
  parameter Modelica.SIunits.Voltage V_nominal(min=0, start=110)
    "Nominal voltage (V_nominal >= 0)"  annotation(Evaluate=true, Dialog(group="Nominal conditions"));
  parameter Boolean linearized=false
    "If =true, introduce a linearization in the load";
  replaceable Buildings.Electrical.Interfaces.Terminal terminal(
    redeclare final package PhaseSystem = PhaseSystem) "Generalized terminal"
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));

  replaceable Buildings.Electrical.AC.OnePhase.Loads.Capacitive load(
    mode=Buildings.Electrical.Types.Load.VariableZ_P_input,
    final pf=1,
    final V_nominal=V_nominal,
    final P_nominal=0,
    final linearized=linearized) "Load model"
    annotation (Placement(transformation(extent={{-62,-10},{-42,10}})));
equation
  connect(load.terminal, terminal)
    annotation (Line(points={{-62,0},{-100,0}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PartialANN;
