within GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel.Examples;
model F
  "Examples that demonstrate how to use ThreeInputs model to predicte PV generation"
  extends Modelica.Icons.Example;
  parameter String moduleName = "ANNModel_F1"
    "Name of the python module that contains the function";
  parameter String functionName = "ANN" "Name of the python function";

  GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel.BaseThreeInputs
    thrInpANN(
    samplePeriod(displayUnit="s") = 60,
    moduleName="ANNModel_F1",
    functionName="ANN")
    "ANN model with three inputs to predict the pv generation"
    annotation (Placement(transformation(extent={{-10,-60},{10,-40}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,30},{-60,50}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/gloHorIrrSBS.txt"))
    "Global horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,-50},{-60,-30}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/dirNorIrrSBS.txt"))
    "Direct normal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));

  PhysicalModel.AC.PVPanels phy(
    num=1,
    pf={0.8},
    eta_DCAC={0.8},
    fAct={1},
    A={29.34},
    eta={0.9},
    til={0.087266462599716},
    lat={0.48051008702506},
    azi={-0.78539816339745}) "PV panels"
    annotation (Placement(transformation(extent={{30,48},{50,68}})));
  PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
      filNam=ModelicaServices.ExternalReferences.loadResource(
        "Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-40,60},{-20,80}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid gri(f=60, V=240)
    "Electrical grid model"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,6})));
equation
  connect(difHorIrr.y[1], thrInpANN.irrDifSol) annotation (Line(points={{-59,40},
          {-20,40},{-20,-43},{-12,-43}},
                                     color={0,0,127}));
  connect(dirNorIrr.y[1], thrInpANN.irrDirSol) annotation (Line(points={{-59,0},
          {-24,0},{-24,-50},{-12,-50}},
                                     color={0,0,127}));
  connect(gloHorIrr.y[1], thrInpANN.irrGloSol) annotation (Line(points={{-59,-40},
          {-28,-40},{-28,-57},{-12,-57}},
                                       color={0,0,127}));
  connect(weaDat.weaBus, phy.weaBus) annotation (Line(
      points={{-20,70},{-12,70},{-12,82},{40,82},{40,67}},
      color={255,204,51},
      thickness=0.5));
  connect(difHorIrr.y[1], weaDat.HDifHor_in) annotation (Line(points={{-59,40},
          {-50,40},{-50,60.4},{-41,60.4}}, color={0,0,127}));
  connect(dirNorIrr.y[1], weaDat.HDirNor_in) annotation (Line(points={{-59,0},{
          -48,0},{-48,56.6},{-41,56.6}}, color={0,0,127}));
  connect(gloHorIrr.y[1], weaDat.HGloHor_in) annotation (Line(points={{-59,-40},
          {-46,-40},{-46,58},{-41,58}}, color={0,0,127}));
  connect(gri.terminal, phy.terminal) annotation (Line(points={{10,6},{20,6},{
          20,58},{30.2,58}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=3600),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Subsystems/RenewableEnergy/PV/ANNModel/Examples/ThreeInputs.mos"
        "Simulate and Plot"));
end F;
