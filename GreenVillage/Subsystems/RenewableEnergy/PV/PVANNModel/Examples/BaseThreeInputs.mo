within GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel.Examples;
model BaseThreeInputs
  "Examples that demonstrate how to use ThreeInputs model to predicte PV generation"
  extends Modelica.Icons.Example;
  parameter String moduleName = "ANNModel_F1"
    "Name of the python module that contains the function";
  parameter String functionName = "ANN" "Name of the python function";

  GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel.BaseThreeInputs
    thrInpANN(
    samplePeriod(displayUnit="s") = 60,
    moduleName="ANNModel_F1",
    functionName="ANN")
    "ANN model with three inputs to predict the pv generation"
    annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,30},{-60,50}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/gloHorIrrSBS.txt"))
    "Global horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,-50},{-60,-30}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource(
      "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/dirNorIrrSBS.txt"))
    "Direct normal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));

equation
  connect(difHorIrr.y[1], thrInpANN.irrDifSol) annotation (Line(points={{-59,40},
          {-20,40},{-20,7},{-10,7}}, color={0,0,127}));
  connect(dirNorIrr.y[1], thrInpANN.irrDirSol) annotation (Line(points={{-59,0},
          {-10,0}},                  color={0,0,127}));
  connect(gloHorIrr.y[1], thrInpANN.irrGloSol) annotation (Line(points={{-59,-40},
          {-20,-40},{-20,-7},{-10,-7}},color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=3600),
    __Dymola_Commands(file=
          "modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/RenewableEnergy/PV/ANNModel/Examples/BaseThreeInputs.mos"
        "Simulate and Plot"));
end BaseThreeInputs;
