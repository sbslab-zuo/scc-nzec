within GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel.Examples;
model ThreeInputsVerification "Example that compare the model output"
  extends Modelica.Icons.Example;
  parameter String moduleName = "ANNModel_F1"
    "Name of the python module that contains the function";
  parameter String functionName = "ANN" "Name of the python function";
  parameter Modelica.SIunits.Time samplePeriod=60 "Sample period of component";

  GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel.BaseThreeInputs
    basThrInp(
    samplePeriod=samplePeriod,
    moduleName=moduleName,
    functionName=functionName) "Base ANN model with three inputs"
    annotation (Placement(transformation(extent={{20,-40},{40,-20}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel.ThreeInputs thrInp(
    V_nominal=240,
    samplePeriod=samplePeriod,
    moduleName=moduleName,
    functionName=functionName) "ANN model with three inputs"
    annotation (Placement(transformation(extent={{20,0},{40,20}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/difHorIrrSBS.txt"))
    "Diffuse horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/dirNorIrrSBS.txt"))
    "Direct normal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,30},{-60,50}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupF/gloHorIrrSBS.txt"))
    "Global horizontal irradiation measured by SBS in June 2017"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(
      filNam=ModelicaServices.ExternalReferences.loadResource("Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"), HSou=
        GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor)
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-20,40},{0,60}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid gri(f=60, V=240)
    "Electrical grid model"
    annotation (Placement(transformation(extent={{-10,10},{10,-10}},
        rotation=-90,
        origin={-10,10})));

equation
  connect(difHorIrr.y[1], weaDat.HDifHor_in) annotation (Line(points={{-59,80},
          {-32,80},{-32,40.4},{-21,40.4}},color={0,0,127}));
  connect(dirNorIrr.y[1], weaDat.HDirNor_in) annotation (Line(points={{-59,40},
          {-36,40},{-36,36.6},{-21,36.6}},color={0,0,127}));
  connect(gloHorIrr.y[1], weaDat.HGloHor_in) annotation (Line(points={{-59,0},{
          -40,0},{-40,38},{-21,38}},
                                 color={0,0,127}));
  connect(weaDat.weaBus, thrInp.weaBus) annotation (Line(
      points={{0,50},{21.4,50},{21.4,19}},
      color={255,204,51},
      thickness=0.5));
  connect(difHorIrr.y[1], basThrInp.irrDifSol) annotation (Line(points={{-59,80},
          {-32,80},{-32,-23},{18,-23}}, color={0,0,127}));
  connect(dirNorIrr.y[1], basThrInp.irrDirSol) annotation (Line(points={{-59,40},
          {-36,40},{-36,-30},{18,-30}}, color={0,0,127}));
  connect(gloHorIrr.y[1], basThrInp.irrGloSol) annotation (Line(points={{-59,0},
          {-40,0},{-40,-37},{18,-37}}, color={0,0,127}));
  connect(gri.terminal, thrInp.terminal)
    annotation (Line(points={{0,10},{20,10}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Subsystems/RenewableEnergy/PV/ANNModel/Examples/ThreeInputsVerification.mos"
        "Simulate and Plot"));
end ThreeInputsVerification;
