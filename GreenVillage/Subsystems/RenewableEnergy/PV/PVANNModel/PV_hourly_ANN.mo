within GreenVillage.Subsystems.RenewableEnergy.PV.PVANNModel;
model PV_hourly_ANN
  replaceable package PhaseSystem =
      Buildings.Electrical.PhaseSystems.OnePhase constrainedby
    Buildings.Electrical.PhaseSystems.PartialPhaseSystem "Phase system";

    parameter String PVW=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVOutput_hourly_20140825-31/PV_predicted20140825-0831/PV Warehouse.txt");
    parameter String PVA=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVOutput_hourly_20140825-31/PV_predicted20140825-0831/Thelmas PV.txt");
    parameter String PVC=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVOutput_hourly_20140825-31/PV_predicted20140825-0831/PV Rosedale.txt");
    parameter String PVD=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVOutput_hourly_20140825-31/PV_predicted20140825-0831/PV Sears.txt");
    parameter String PVF=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVOutput_hourly_20140825-31/PV_predicted20140825-0831/Solar F.txt");
    parameter String PVG=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVOutput_hourly_20140825-31/PV_predicted20140825-0831/PV G.txt");
    parameter String PVNor=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVOutput_hourly_20140825-31/PV_predicted20140825-0831/North Carport.txt");
    parameter String PVSou=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/PVOutput_hourly_20140825-31/PV_predicted20140825-0831/South Carport.txt");

  replaceable Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_p
                                                       terminal(redeclare
      replaceable package PhaseSystem = PhaseSystem) constrainedby
    Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_p(     redeclare
      replaceable package PhaseSystem = PhaseSystem)
    "Generalized electric terminal" annotation (Placement(transformation(extent={{-8,-148},
            {8,-132}}),          iconTransformation(extent={{-8,-148},{8,-132}})));
  System.Baseclasss.ControlBus Bus annotation (Placement(transformation(
          extent={{32,-170},{72,-130}}), iconTransformation(extent={{52,
            -150},{72,-130}})));
  Modelica.Blocks.Sources.CombiTimeTable PV_W_ANN(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=fill(
        0.0,
        0,
        2),
    tableOnFile=true,
    tableName="table1",
    fileName=PVW) "Power consumption profile for load "
                                          annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-90,120})));
  Buildings.Electrical.AC.OnePhase.Sources.Generator PV_W
    annotation (Placement(transformation(extent={{-80,110},{-60,130}})));
  Modelica.Blocks.Sources.CombiTimeTable PV_A_ANN(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=fill(
        0.0,
        0,
        2),
    tableOnFile=true,
    tableName="table1",
    fileName=PVA) "Power consumption profile for load "
                                          annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-90,90})));
  Buildings.Electrical.AC.OnePhase.Sources.Generator PV_A
    annotation (Placement(transformation(extent={{-80,80},{-60,100}})));
  Modelica.Blocks.Sources.CombiTimeTable PV_C_ANN(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=fill(
        0.0,
        0,
        2),
    tableOnFile=true,
    tableName="table1",
    fileName=PVC) "Power consumption profile for load "
                                          annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-90,60})));
  Buildings.Electrical.AC.OnePhase.Sources.Generator PV_C
    annotation (Placement(transformation(extent={{-80,50},{-60,70}})));
  Modelica.Blocks.Sources.CombiTimeTable PV_D_ANN(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=fill(
        0.0,
        0,
        2),
    tableOnFile=true,
    tableName="table1",
    fileName=PVD) "Power consumption profile for load "
                                          annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-90,30})));
  Buildings.Electrical.AC.OnePhase.Sources.Generator PV_D
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));
  Modelica.Blocks.Sources.CombiTimeTable PV_F_ANN(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=fill(
        0.0,
        0,
        2),
    tableOnFile=true,
    tableName="table1",
    fileName=PVF) "Power consumption profile for load "
                                          annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-90,0})));
  Buildings.Electrical.AC.OnePhase.Sources.Generator PV_F
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  Modelica.Blocks.Sources.CombiTimeTable PV_G1_ANN(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=fill(
        0.0,
        0,
        2),
    tableOnFile=true,
    tableName="table1",
    fileName=PVG) "Power consumption profile for load "
                                          annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-90,-30})));
  Buildings.Electrical.AC.OnePhase.Sources.Generator PV_G
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable PV_Nor_ANN(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=fill(
        0.0,
        0,
        2),
    tableOnFile=true,
    tableName="table1",
    fileName=PVNor) "Power consumption profile for load "
                                          annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-90,-90})));
  Buildings.Electrical.AC.OnePhase.Sources.Generator PV_Nor
    annotation (Placement(transformation(extent={{-80,-100},{-60,-80}})));
  Modelica.Blocks.Sources.CombiTimeTable PV_Sou_ANN(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=fill(
        0.0,
        0,
        2),
    tableOnFile=true,
    tableName="table1",
    fileName=PVSou) "Power consumption profile for load "
                                          annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-90,-120})));
  Buildings.Electrical.AC.OnePhase.Sources.Generator PV_Sou
    annotation (Placement(transformation(extent={{-80,-130},{-60,-110}})));

equation
  connect(PV_W_ANN.y[1], PV_W.P)
    annotation (Line(points={{-83.4,120},{-80,120}}, color={0,0,127}));
  connect(PV_A_ANN.y[1], PV_A.P)
    annotation (Line(points={{-83.4,90},{-80,90}}, color={0,0,127}));
  connect(PV_C_ANN.y[1], PV_C.P)
    annotation (Line(points={{-83.4,60},{-80,60}}, color={0,0,127}));
  connect(PV_D_ANN.y[1], PV_D.P)
    annotation (Line(points={{-83.4,30},{-80,30}}, color={0,0,127}));
  connect(PV_F_ANN.y[1], PV_F.P)
    annotation (Line(points={{-83.4,0},{-80,0}}, color={0,0,127}));
  connect(PV_G1_ANN.y[1], PV_G.P)
    annotation (Line(points={{-83.4,-30},{-80,-30}}, color={0,0,127}));
  connect(PV_Nor_ANN.y[1], PV_Nor.P)
    annotation (Line(points={{-83.4,-90},{-80,-90}}, color={0,0,127}));
  connect(PV_Sou_ANN.y[1], PV_Sou.P)
    annotation (Line(points={{-83.4,-120},{-80,-120}}, color={0,0,127}));
  connect(PV_W.terminal, terminal)
    annotation (Line(points={{-60,120},{0,120},{0,-140}},color={0,120,120}));
  connect(PV_A.terminal, terminal)
    annotation (Line(points={{-60,90},{0,90},{0,-140}},color={0,120,120}));
  connect(PV_C.terminal, terminal)
    annotation (Line(points={{-60,60},{0,60},{0,-140}},color={0,120,120}));
  connect(PV_D.terminal, terminal)
    annotation (Line(points={{-60,30},{0,30},{0,-140}},color={0,120,120}));
  connect(PV_F.terminal, terminal)
    annotation (Line(points={{-60,0},{0,0},{0,-140}},color={0,120,120}));
  connect(PV_G.terminal, terminal)
    annotation (Line(points={{-60,-30},{0,-30},{0,-140}}, color={0,120,120}));
  connect(PV_Nor.terminal, terminal)
    annotation (Line(points={{-60,-90},{0,-90},{0,-140}},color={0,120,120}));
  connect(PV_Sou.terminal, terminal)
    annotation (Line(points={{-60,-120},{0,-120},{0,-140}},color={0,120,120}));

  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-160,
            -140},{160,140}}), graphics={
                                        Text(
        extent={{-146,190},{154,150}},
        textString="%name",
        lineColor={0,0,255}),
        Rectangle(
          extent={{-160,140},{160,-140}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-102,44},{120,-36}},
          lineColor={28,108,200},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-160,-140},{160,
            140}})));
end PV_hourly_ANN;
