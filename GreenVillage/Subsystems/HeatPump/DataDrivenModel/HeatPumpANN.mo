within GreenVillage.Subsystems.HeatPump.DataDrivenModel;
model HeatPumpANN "Heat pump ANN model"
  extends
    GreenVillage.Subsystems.RenewableEnergy.PV.DataDrivenModel.BaseClasses.PartialANN(
      redeclare package PhaseSystem =
        Buildings.Electrical.PhaseSystems.OnePhase, redeclare
      Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n terminal);
  parameter Modelica.SIunits.Time samplePeriod = 60 "Sample period of component";
  parameter Modelica.SIunits.Time startTime=0 "First sample time instant";
  parameter String moduleName
    "Name of the python module that contains the function";
  parameter String functionName "Name of the python function";
  final parameter Integer nDblWri = 30 "Number of double values to write to Python";
  final parameter Integer flaDblWri[nDblWri]=zeros(nDblWri)
    "Flag for double values (0: use current value, 1: use average over interval, 2: use integral over interval)";

  GreenVillage.Subsystems.HeatPump.DataDrivenModel.BaseANN baseANN(
    samplePeriod=samplePeriod,
    startTime=startTime,
    moduleName=moduleName,
    functionName=functionName,
    nDblWri=nDblWri,
    flaDblWri=flaDblWri)
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
  Buildings.Controls.OBC.CDL.Continuous.Gain gai(final k=-1)
    annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Modelica.Blocks.Interfaces.RealInput irrGloSol(final quantity="RadiantEnergyFluenceRate",
      final unit="W/m2")
                       "Global solar irradiance"
    annotation (Placement(transformation(extent={{-140,70},{-100,110}})));
  Modelica.Blocks.Interfaces.RealInput TOutDoo(final unit="degC")
    "Outdoor air temperature"
    annotation (Placement(transformation(extent={{-140,40},{-100,80}})));
  Modelica.Blocks.Interfaces.IntegerInput houInd
    "Index of the hour in a day"
    annotation (Placement(transformation(extent={{-140,10},{-100,50}})));
  Modelica.Blocks.Interfaces.IntegerInput weeDay
    "Number of weekday"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.IntegerInput holInd
    "Index of holiday, 0 means False and 1 means True"
    annotation (Placement(transformation(extent={{-140,-50},{-100,-10}})));
  Modelica.Blocks.Interfaces.IntegerInput peaSea
    "Index of peak season, 0 means False and 1 means True"
    annotation (Placement(transformation(extent={{-140,-80},{-100,-40}})));
  Modelica.Blocks.Interfaces.RealInput preHouPow[24](final quantity="Power",
      final unit="W")
    "Power consumption in previous 24 hours"
    annotation (Placement(transformation(extent={{-140,-110},{-100,-70}})));

equation
  connect(baseANN.pow, gai.u)
    annotation (Line(points={{-19,0},{-10,0}}, color={0,0,127}));
  connect(gai.y, load.Pow)
    annotation (Line(points={{13,0},{42,0}}, color={0,0,127}));
  connect(irrGloSol, baseANN.irrGloSol) annotation (Line(points={{-120,90},{-60,
          90},{-60,9},{-42,9}}, color={0,0,127}));
  connect(TOutDoo, baseANN.TOutDoo) annotation (Line(points={{-120,60},{-62,60},
          {-62,6},{-42,6}}, color={0,0,127}));
  connect(houInd, baseANN.houInd) annotation (Line(points={{-120,30},{-64,30},{-64,
          3},{-42,3}}, color={255,127,0}));
  connect(weeDay, baseANN.weeDay)
    annotation (Line(points={{-120,0},{-42,0}}, color={255,127,0}));
  connect(holInd, baseANN.holInd) annotation (Line(points={{-120,-30},{-64,-30},
          {-64,-3},{-42,-3}}, color={255,127,0}));
  connect(peaSea, baseANN.peaSea) annotation (Line(points={{-120,-60},{-62,-60},
          {-62,-6},{-42,-6}}, color={255,127,0}));
  connect(preHouPow, baseANN.preHouPow) annotation (Line(points={{-120,-90},{-60,
          -90},{-60,-9},{-42,-9}}, color={0,0,127}));
  annotation (defaultComponentName = "heaPumANN",
    Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Bitmap(extent={{-100,-100},{100,100}}, fileName="modelica://GreenVillage/Resources/Icons/ann.jpeg"),
        Text(
          extent={{-152,112},{148,152}},
          lineColor={0,0,255},
          textString="%name")}),
           Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end HeatPumpANN;
