within GreenVillage.Subsystems.HeatPump.DataDrivenModel.Example;
model BaseANN_VariedInput "Base data driven model for modeling"
  extends Modelica.Icons.Example;
  extends GreenVillage.Subsystems.HeatPump.DataDrivenModel.Example.BaseClasses.PartialInputInterface;

  parameter String moduleName = "WSHP_F1"
    "Name of the python module that contains the function";
  parameter String functionName = "model"
    "Name of the python function";
  parameter Modelica.SIunits.Time samplePeriod=3600
    "Sample period of component";

  GreenVillage.Subsystems.HeatPump.DataDrivenModel.BaseANN basANN(
    moduleName=moduleName,
    functionName=functionName,
    samplePeriod=samplePeriod)
    "Base ANN models"
    annotation (Placement(transformation(extent={{20,0},{40,20}})));

equation
  connect(weaBus.HGloHor, basANN.irrGloSol) annotation (Line(
      points={{-40,80},{0,80},{0,19},{18,19}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(fromKelvin.Celsius, basANN.TOutDoo) annotation (Line(points={{-29,60},
          {-2,60},{-2,16},{18,16}}, color={0,0,127}));
  connect(calTim.weekDay, basANN.weeDay) annotation (Line(points={{-59,25},{-6,
          25},{-6,10},{18,10}}, color={255,127,0}));
  connect(calTim.hour, basANN.houInd) annotation (Line(points={{-59,36.2},{-4,
          36.2},{-4,13},{18,13}}, color={255,127,0}));
  connect(holDay.holiday, basANN.holInd) annotation (Line(points={{-9,-10},{-6,
          -10},{-6,7},{18,7}}, color={255,127,0}));
  connect(peaSea.y, basANN.peaSea) annotation (Line(points={{-9,-40},{-4,-40},{
          -4,4},{18,4}}, color={255,127,0}));
  connect(his.y, basANN.preHouPow) annotation (Line(points={{-9,-70},{-2,-70},{
          -2,1},{18,1}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=86400));
end BaseANN_VariedInput;
