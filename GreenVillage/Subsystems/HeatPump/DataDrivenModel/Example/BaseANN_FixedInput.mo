within GreenVillage.Subsystems.HeatPump.DataDrivenModel.Example;
model BaseANN_FixedInput "Base data driven model for modeling"
  extends Modelica.Icons.Example;
  parameter String moduleName = "WSHP_F1"
    "Name of the python module that contains the function";
  parameter String functionName = "model"
    "Name of the python function";
  parameter Modelica.SIunits.Time samplePeriod=3600
    "Sample period of component";

  GreenVillage.Subsystems.HeatPump.DataDrivenModel.BaseANN basANN(
    moduleName=moduleName,
    functionName=functionName,
    samplePeriod=samplePeriod)
    "Base ANN models"
    annotation (Placement(transformation(extent={{20,0},{40,20}})));
  GreenVillage.DateType.History history
    annotation (Placement(transformation(extent={{-30,-80},{-10,-60}})));
  Modelica.Blocks.Sources.Sine pow(
    amplitude=4000,
    freqHz=1/86400,
    phase=4.7123889803847,
    offset=4000) "Sine wave"
    annotation (Placement(transformation(extent={{-80,-80},{-60,-60}})));

  Modelica.Blocks.Sources.Constant sor(k=200) "Solar global irradiance"
    annotation (Placement(transformation(extent={{-100,60},{-80,80}})));
  Modelica.Blocks.Sources.Constant TOut(k=25)
    annotation (Placement(transformation(extent={{-100,22},{-80,42}})));
  Buildings.Controls.OBC.CDL.Integers.Sources.Constant hou(k=13)
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Buildings.Controls.OBC.CDL.Integers.Sources.Constant wee(k=3)
    annotation (Placement(transformation(extent={{-100,-40},{-80,-20}})));
  Buildings.Controls.OBC.CDL.Integers.Sources.Constant hol(k=0)
    annotation (Placement(transformation(extent={{-40,58},{-20,78}})));
  Buildings.Controls.OBC.CDL.Integers.Sources.Constant pea(k=0)
    annotation (Placement(transformation(extent={{-40,-40},{-20,-20}})));
equation
  connect(pow.y, history.u)
    annotation (Line(points={{-59,-70},{-32,-70}}, color={0,0,127}));
  connect(history.y, basANN.preHouPow)
    annotation (Line(points={{-9,-70},{8,-70},{8,1},{18,1}}, color={0,0,127}));
  connect(sor.y, basANN.irrGloSol) annotation (Line(points={{-79,70},{-44,70},{-44,
          19},{18,19}}, color={0,0,127}));
  connect(TOut.y, basANN.TOutDoo) annotation (Line(points={{-79,32},{-48,32},{-48,
          16},{18,16}}, color={0,0,127}));
  connect(hou.y, basANN.houInd) annotation (Line(points={{-79,0},{-60,0},{-60,13},
          {18,13}}, color={255,127,0}));
  connect(wee.y, basANN.weeDay) annotation (Line(points={{-79,-30},{-56,-30},{-56,
          10},{18,10}}, color={255,127,0}));
  connect(pea.y, basANN.peaSea) annotation (Line(points={{-19,-30},{0,-30},{0,4},
          {18,4}}, color={255,127,0}));
  connect(hol.y, basANN.holInd) annotation (Line(points={{-19,68},{0,68},{0,7},{
          18,7}}, color={255,127,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=172800),
    __Dymola_Commands(file="modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/HeatPump/DataDrivenModel/Example/BaseANN_FixedInput.mos"
        "Simulate and Plot"));
end BaseANN_FixedInput;
