within GreenVillage.Subsystems.HeatPump.DataDrivenModel.Example;
model HeatPumpANN
  "Example that shows the usage of data driven model for heat pump system"
  extends Modelica.Icons.Example;
  extends GreenVillage.Subsystems.HeatPump.DataDrivenModel.Example.BaseClasses.PartialInputInterface;

  parameter String moduleName = "WSHP_F1"
    "Name of the python module that contains the function";
  parameter String functionName = "model"
    "Name of the python function";
  parameter Modelica.SIunits.Voltage V_nominal = 120
    "Nominal voltage (V_nominal >= 0)";

  GreenVillage.Subsystems.HeatPump.DataDrivenModel.HeatPumpANN heaPumANN(
    moduleName=moduleName,
    functionName=functionName,
    V_nominal=V_nominal,
    samplePeriod=samplePeriod)
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));
  Grid.Grid gri(V=V_nominal)
   "Grid"
    annotation (Placement(transformation(extent={{60,20},{80,40}})));

equation
  connect(weaBus.HGloHor, heaPumANN.irrGloSol) annotation (Line(
      points={{-60,80},{10,80},{10,9},{18,9}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(fromKelvin.Celsius, heaPumANN.TOutDoo)
    annotation (Line(points={{-49,60},{8,60},{8,6},{18,6}}, color={0,0,127}));
  connect(calTim.hour, heaPumANN.houInd) annotation (Line(points={{-79,36.2},{6,
          36.2},{6,3},{18,3}}, color={255,127,0}));
  connect(calTim.weekDay, heaPumANN.weeDay) annotation (Line(points={{-79,25},{4,
          25},{4,0},{18,0}}, color={255,127,0}));
  connect(holDay.holiday, heaPumANN.holInd) annotation (Line(points={{-29,-10},{
          4,-10},{4,-3},{18,-3}},
                                color={255,127,0}));
  connect(peaSea.y, heaPumANN.peaSea) annotation (Line(points={{-29,-40},{6,-40},
          {6,-6},{18,-6}}, color={255,127,0}));
  connect(his.y, heaPumANN.preHouPow) annotation (Line(points={{-29,-70},{8,-70},
          {8,-9},{18,-9}}, color={0,0,127}));
  connect(gri.terminal, heaPumANN.terminal)
    annotation (Line(points={{70,20.2},{70,0},{39.2,0}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end HeatPumpANN;
