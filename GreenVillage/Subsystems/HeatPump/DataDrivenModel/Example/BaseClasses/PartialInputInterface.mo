within GreenVillage.Subsystems.HeatPump.DataDrivenModel.Example.BaseClasses;
partial model PartialInputInterface
  "Base interface for data driven model testing"


  parameter Modelica.SIunits.Time samplePeriod=3600
    "Sample period of component";

  Buildings.Utilities.Time.CalendarTime calTim(zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017)
    annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam=
        ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"))
    annotation (Placement(transformation(extent={{-100,70},{-80,90}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus "Weather data bus"
    annotation (Placement(transformation(extent={{-70,70},{-50,90}})));
  Modelica.Thermal.HeatTransfer.Celsius.FromKelvin fromKelvin
    annotation (Placement(transformation(extent={{-70,50},{-50,70}})));
  GreenVillage.DateType.HolidayUS holDay(samplePeriod=samplePeriod)
    annotation (Placement(transformation(extent={{-50,-20},{-30,0}})));
  GreenVillage.DateType.PeakSeason peaSea(
    startPeriod=[2017,1,1,0,0; 2017,12,15,0,0],
    endPeriod=[2017,5,15,0,0; 2017,12,31,23,59],
    samplePeriod=samplePeriod)
    annotation (Placement(transformation(extent={{-50,-50},{-30,-30}})));
  GreenVillage.DateType.History his(sampleFrequency=samplePeriod)
    annotation (Placement(transformation(extent={{-50,-80},{-30,-60}})));
  Modelica.Blocks.Sources.Sine pow(
    amplitude=4000,
    freqHz=1/86400,
    phase=4.7123889803847,
    offset=4000) "Sine wave"
    annotation (Placement(transformation(extent={{-100,-80},{-80,-60}})));

equation
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{-80,80},{-60,80}},
      color={255,204,51},
      thickness=0.5));
  connect(weaBus.TDryBul, fromKelvin.Kelvin) annotation (Line(
      points={{-60,80},{-60,72},{-80,72},{-80,60},{-72,60}},
      color={255,204,51},
      thickness=0.5));
  connect(calTim.year, holDay.year) annotation (Line(points={{-79,27.8},{-70,27.8},
          {-70,-5},{-52,-5}}, color={255,127,0}));
  connect(calTim.month, holDay.month) annotation (Line(points={{-79,30.6},{-68,30.6},
          {-68,-10},{-52,-10}}, color={255,127,0}));
  connect(calTim.day, holDay.day) annotation (Line(points={{-79,33.4},{-66,33.4},
          {-66,-15},{-52,-15}}, color={255,127,0}));
  connect(calTim.year, peaSea.year) annotation (Line(points={{-79,27.8},{-70,27.8},
          {-70,-32},{-52,-32}}, color={255,127,0}));
  connect(calTim.month, peaSea.month) annotation (Line(points={{-79,30.6},{-68,30.6},
          {-68,-36},{-52,-36}}, color={255,127,0}));
  connect(calTim.day, peaSea.day) annotation (Line(points={{-79,33.4},{-66,33.4},
          {-66,-40},{-52,-40}}, color={255,127,0}));
  connect(calTim.hour, peaSea.hour) annotation (Line(points={{-79,36.2},{-64,36.2},
          {-64,-44},{-52,-44}}, color={255,127,0}));
  connect(calTim.minute, peaSea.minute) annotation (Line(points={{-79,39},{-62,39},
          {-62,-48},{-52,-48}}, color={0,0,127}));
  connect(pow.y, his.u)
    annotation (Line(points={{-79,-70},{-52,-70}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=86400));
end PartialInputInterface;
