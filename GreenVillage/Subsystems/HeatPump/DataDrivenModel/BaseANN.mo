within GreenVillage.Subsystems.HeatPump.DataDrivenModel;
model BaseANN
  parameter Modelica.SIunits.Time samplePeriod "Sample period of component";
  parameter Modelica.SIunits.Time startTime=0 "First sample time instant";
  parameter String moduleName
    "Name of the python module that contains the function";
  parameter String functionName "Name of the python function";
  parameter Integer nDblWri = 30 "Number of double values to write to Python";
  parameter Integer flaDblWri[nDblWri]=zeros(nDblWri)
    "Flag for double values (0: use current value, 1: use average over interval, 2: use integral over interval)";

  Modelica.Blocks.Interfaces.RealInput irrGloSol(
    final quantity="RadiantEnergyFluenceRate",
    final unit="W/m2") "Global solar irradiance"
    annotation (Placement(transformation(extent={{-140,70},{-100,110}})));
  Modelica.Blocks.Interfaces.RealOutput pow(
    final quantity="Power",
    final unit="W")
    "Power consumption"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));

  Buildings.Utilities.IO.Python27.Real_Real pyt(
    final samplePeriod=samplePeriod,
    final startTime=startTime,
    final moduleName=moduleName,
    final functionName=functionName,
    final flaDblWri=flaDblWri,
    final nDblWri=nDblWri,
    final nDblRea=1)
    "ANN models in python"
    annotation (Placement(transformation(extent={{-12,-10},{8,10}})));

  Modelica.Blocks.Interfaces.RealInput TOutDoo(
    final unit="degC")
    "Outdoor air temperature"
    annotation (Placement(transformation(extent={{-140,40},{-100,80}})));

  Modelica.Blocks.Interfaces.IntegerInput houInd
    "Index of the hour in a day"
    annotation (Placement(transformation(extent={{-140,10},{-100,50}})));
  Modelica.Blocks.Interfaces.IntegerInput weeDay
    "Number of weekday"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.IntegerInput holInd
    "Index of holiday, 0 means False and 1 means True"
    annotation (Placement(transformation(extent={{-140,-50},{-100,-10}})));
  Modelica.Blocks.Interfaces.IntegerInput peaSea
    "Index of peak season, 0 means False and 1 means True"
    annotation (Placement(transformation(extent={{-140,-80},{-100,-40}})));
  Modelica.Blocks.Interfaces.RealInput preHouPow[24](
    final quantity="Power",
    final unit="W")
    "Power consumption in previous 24 hours"
    annotation (Placement(transformation(extent={{-140,-110},{-100,-70}})));

  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaHou "Real to integer"
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaWee "Real to integer"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaHol "Real to integer"
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));
  Buildings.Controls.OBC.CDL.Conversions.IntegerToReal reaPea "Real to integer"
    annotation (Placement(transformation(extent={{-80,-70},{-60,-50}})));
equation
  connect(pow, pow)
    annotation (Line(points={{110,0},{110,0}}, color={0,0,127}));
  connect(pyt.yR[1], pow)
    annotation (Line(points={{9,0},{110,0}}, color={0,0,127}));
  connect(houInd, reaHou.u)
    annotation (Line(points={{-120,30},{-82,30}}, color={255,127,0}));
  connect(irrGloSol, pyt.uR[1]) annotation (Line(points={{-120,90},{-22,90},{-22,
          0},{-14,0}}, color={0,0,127}));
  connect(TOutDoo, pyt.uR[2]) annotation (Line(points={{-120,60},{-24,60},{-24,0},
          {-14,0}}, color={0,0,127}));
  connect(reaHou.y, pyt.uR[3]) annotation (Line(points={{-59,30},{-26,30},{-26,0},
          {-14,0}}, color={0,0,127}));
  connect(reaWee.y, pyt.uR[4])
    annotation (Line(points={{-59,0},{-14,0}}, color={0,0,127}));
  connect(reaHol.y, pyt.uR[5]) annotation (Line(points={{-59,-30},{-28,-30},{-28,
          0},{-14,0}}, color={0,0,127}));
  connect(reaPea.y, pyt.uR[6]) annotation (Line(points={{-59,-60},{-26,-60},{-26,
          0},{-14,0}}, color={0,0,127}));
  connect(weeDay, reaWee.u)
    annotation (Line(points={{-120,0},{-82,0}}, color={255,127,0}));
  connect(holInd, reaHol.u)
    annotation (Line(points={{-120,-30},{-82,-30}}, color={255,127,0}));
  connect(peaSea, reaPea.u) annotation (Line(points={{-120,-60},{-102,-60},{-102,
          -60},{-82,-60}}, color={255,127,0}));

  for i in 1:24 loop
  connect(preHouPow[i], pyt.uR[i+6]) annotation (Line(points={{-120,-90},{-24,-90},
            {-24,0},{-14,0}},         color={0,0,127}));
  end for;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,127},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-50,82},{-76,56}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          lineThickness=0.5),
        Ellipse(
          extent={{-50,12},{-76,-14}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          lineThickness=0.5),
        Ellipse(
          extent={{-50,-56},{-76,-82}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          lineThickness=0.5),
        Ellipse(
          extent={{20,98},{-6,72}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          lineThickness=0.5),
        Ellipse(
          extent={{20,46},{-6,20}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          lineThickness=0.5),
        Ellipse(
          extent={{20,-10},{-6,-36}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          lineThickness=0.5),
        Ellipse(
          extent={{20,-66},{-6,-92}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          lineThickness=0.5),
        Ellipse(
          extent={{82,14},{56,-12}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          lineThickness=0.5),
        Line(
          points={{-100,70},{-76,70}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-100,0},{-76,0}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-100,-70},{-76,-70}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,70},{-8,90}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,70},{-8,38}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,70},{-6,-14}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,70},{-6,-72}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,0},{-8,82}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,0},{-8,30}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,0},{-8,-22}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,0},{-8,-78}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,-70},{-4,74}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,-70},{-4,22}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,-68},{-8,-28}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{-50,-68},{-8,-84}},
          color={0,0,127},
          thickness=0.5),
        Line(
          points={{18,76},{60,12}},
          color={0,0,127},
          arrow={Arrow.None,Arrow.Open},
          thickness=0.5),
        Line(
          points={{20,28},{56,6}},
          color={0,0,127},
          arrow={Arrow.None,Arrow.Open},
          thickness=0.5),
        Line(
          points={{22,-22},{54,-4}},
          color={0,0,127},
          arrow={Arrow.None,Arrow.Open},
          thickness=0.5),
        Line(
          points={{20,-78},{58,-10}},
          color={0,0,127},
          arrow={Arrow.None,Arrow.Open},
          thickness=0.5),
        Line(
          points={{82,0},{100,0}},
          color={0,0,127},
          thickness=0.5),
        Text(
          extent={{-152,112},{148,152}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end BaseANN;
