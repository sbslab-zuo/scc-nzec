within GreenVillage.Subsystems.HeatPump.WSHPPhysics.Example;
model HPLoopWithHeatRecovery
  "Heat pump system with heat recovery interface"
  extends Modelica.Icons.Example;
  package Medium = Buildings.Media.Water "Medium model";

  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri "Grid"
    annotation (Placement(transformation(extent={{-68,-10},{-48,10}})));
  GreenVillage.Subsystems.HeatPump.WSHPPhysics.HPLoopWithHeatRecovery heaPumLoo
    "Heat pump loop"
    annotation (Placement(transformation(extent={{-8,-12},{12,8}})));
  Buildings.Fluid.Sources.MassFlowSource_T sou(
    redeclare package Medium = Medium,
    use_m_flow_in=false,
    nPorts=1,
    m_flow=0.2,
    T=333.15) "Consumption of hot water" annotation (Placement(transformation(
          extent={{-10,-10},{10,10}}, origin={-78,-60})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSou(
    T_start(displayUnit="K"),
    m_flow_nominal=sou.m_flow,
    redeclare package Medium = Medium) "Temperature sensor"
    annotation (Placement(transformation(extent={{-58,-70},{-38,-50}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSin(
    T_start(displayUnit="K"),
    m_flow_nominal=sou.m_flow,
    redeclare package Medium = Medium) "Temperature sensor"
    annotation (Placement(transformation(extent={{42,-70},{62,-50}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      sin(redeclare package Medium = Medium,
      nPorts=1,
    m_flow=-0.2)      "Outlet for hot water draw"
    annotation (Placement(transformation(
      extent={{-10,-10},{10,10}},
      rotation=180,
      origin={82,-60})));
  Buildings.Fluid.FixedResistances.PressureDrop res(
    redeclare package Medium = Medium,
    dp_nominal=6000,
    m_flow_nominal=0.2)
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=-90,
        origin={-28,-38})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/difHorIrr.txt"))
    "Diffuse horizontal irradiation"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/gloHorIrr.txt"))
    "Global horizontal irradiation"
    annotation (Placement(transformation(extent={{-100,50},{-80,70}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/dirNorIrr.txt"))
    "Direct normal irradiation"
    annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
  RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(
    HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
    filNam=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"),
    computeWetBulbTemperature=false)
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-40,70},{-20,90}})));

equation
  connect(gri.terminal, heaPumLoo.terminal) annotation (Line(points={{-58,-10},
          {-58,-20},{2,-20},{2,-11.8}},  color={0,120,120}));
  connect(TSin.port_b, sin.ports[1])
    annotation (Line(points={{62,-60},{72,-60}}, color={0,127,255}));
  connect(TSou.port_a,sou. ports[1])
    annotation (Line(points={{-58,-60},{-68,-60}}, color={0,127,255}));
  connect(TSou.port_b, res.port_a) annotation (Line(points={{-38,-60},{-28,-60},
          {-28,-48}},color={0,127,255}));
  connect(res.port_b, heaPumLoo.port_a)
    annotation (Line(points={{-28,-28},{-28,-16},{-6,-16}},
                                                         color={0,127,255}));
  connect(heaPumLoo.port_b, TSin.port_a) annotation (Line(points={{10,-16},{32,
          -16},{32,-60},{42,-60}},color={0,127,255}));
  connect(difHorIrr.y[1],weaDat. HDifHor_in) annotation (Line(points={{-79,90},
          {-62,90},{-62,70.4},{-41,70.4}},
                                      color={0,0,127}));
  connect(gloHorIrr.y[1],weaDat. HGloHor_in) annotation (Line(points={{-79,60},
          {-62,60},{-62,68},{-41,68}},
                                  color={0,0,127}));
  connect(dirNorIrr.y[1],weaDat. HDirNor_in) annotation (Line(points={{-79,30},
          {-60,30},{-60,66.6},{-41,66.6}},
                                      color={0,0,127}));
  connect(weaDat.weaBus, heaPumLoo.weaBus) annotation (Line(
      points={{-20,80},{-14,80},{-14,6},{-6.8,6}},
      color={255,204,51},
      thickness=0.5));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file=
          "modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/HeatPump/WSHPPhysics/Examples/HPLoopWithHeatRecovery.mos"
        "Simulate and Plot"));
end HPLoopWithHeatRecovery;
