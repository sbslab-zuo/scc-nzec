within GreenVillage.Subsystems.HeatPump.WSHPPhysics.Example;
model HPLoop
  extends Modelica.Icons.Example;

  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri "Grid"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
  GreenVillage.Subsystems.HeatPump.WSHPPhysics.HPLoop heaPumLoo
    "Heat pump loop"
    annotation (Placement(transformation(extent={{20,-12},{40,8}})));
  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/difHorIrr.txt"))
    "Diffuse horizontal irradiation"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/gloHorIrr.txt"))
    "Global horizontal irradiation"
    annotation (Placement(transformation(extent={{-102,50},{-82,70}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/dirNorIrr.txt"))
    "Direct normal irradiation"
    annotation (Placement(transformation(extent={{-102,20},{-82,40}})));
  RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(
    HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
    filNam=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"),
    computeWetBulbTemperature=false)
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{-42,70},{-22,90}})));

equation
  connect(gri.terminal, heaPumLoo.terminal) annotation (Line(points={{-50,-10},
          {-50,-20},{30,-20},{30,-11.8}}, color={0,120,120}));
  connect(difHorIrr.y[1],weaDat. HDifHor_in) annotation (Line(points={{-79,90},
          {-64,90},{-64,70.4},{-43,70.4}},
                                      color={0,0,127}));
  connect(gloHorIrr.y[1],weaDat. HGloHor_in) annotation (Line(points={{-81,60},
          {-64,60},{-64,68},{-43,68}},
                                  color={0,0,127}));
  connect(dirNorIrr.y[1],weaDat. HDirNor_in) annotation (Line(points={{-81,30},
          {-62,30},{-62,66.6},{-43,66.6}},
                                      color={0,0,127}));
  connect(weaDat.weaBus, heaPumLoo.weaBus) annotation (Line(
      points={{-22,80},{21.2,80},{21.2,6}},
      color={255,204,51},
      thickness=0.5));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file="modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/HeatPump/WSHPPhysics/Examples/HPLoop.mos"
        "Simulate and Plot"));
end HPLoop;
