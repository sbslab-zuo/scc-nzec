within GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels;
model SimplifiedRoom "Simplified data center room"
  extends Buildings.BaseClasses.BaseIcon;
  replaceable package Medium = Modelica.Media.Interfaces.PartialMedium
    "Medium model";
  parameter Integer nPorts=0 "Number of parts" annotation (Evaluate=true,
      Dialog(
      connectorSizing=true,
      tab="General",
      group="Ports"));
  parameter Modelica.SIunits.Length rooLen "Length of the room";
  parameter Modelica.SIunits.Length rooWid "Width of the room";
  parameter Modelica.SIunits.Height rooHei "Height of the room";

  Buildings.Fluid.MixingVolumes.MixingVolume rooVol(
    redeclare package Medium = Medium,
    nPorts=nPorts,
    V=rooLen*rooWid*rooHei,
    m_flow_nominal=m_flow_nominal,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    final T_start=293.15,
    final prescribedHeatFlowRate=true) "Volume of air in the room" annotation (Placement(
        transformation(extent={{41,-40},{61,-20}})));
  Modelica.Fluid.Vessels.BaseClasses.VesselFluidPorts_b airPorts[nPorts](
      redeclare each package Medium = Medium) "Fluid inlets and outlets"
    annotation (Placement(transformation(
        extent={{-38,-12},{38,12}},
        rotation=180,
        origin={0,-100}), iconTransformation(
        extent={{-40.5,-13},{40.5,13}},
        rotation=180,
        origin={4.5,-87})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow QSou
    "Heat source of the room"
    annotation (Placement(transformation(extent={{-18,-40},{2,-20}})));
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal
    "Nominal mass flow rate";
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));
  Modelica.Blocks.Interfaces.RealOutput TRooAir(unit="K", displayUnit="degC")
    "Room air temperature" annotation (Placement(transformation(extent={{100,-10},
            {120,10}}), iconTransformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Interfaces.RealInput theLoa(unit="W")
    "Thermal loads (postive for cooling, negative for heating)"
    annotation (Placement(transformation(extent={{-140,20},{-100,60}})));
equation
  connect(rooVol.ports, airPorts) annotation (Line(
      points={{51,-40},{52,-40},{52,-80},{0,-80},{0,-100}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(QSou.port, rooVol.heatPort) annotation (Line(
      points={{2,-30},{41,-30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(TAir.port, rooVol.heatPort) annotation (Line(points={{40,0},{30,0},{
          30,-30},{41,-30}}, color={191,0,0}));
  connect(TAir.T, TRooAir) annotation (Line(points={{60,0},{76,0},{110,0}},
               color={0,0,127}));
  connect(QSou.Q_flow, theLoa) annotation (Line(points={{-18,-30},{-80,-30},{-80,
          40},{-120,40}}, color={0,0,127}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
            100}}), graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid,
          lineThickness=1)}),
    Documentation(info="<html>
<p>
This is a simplified room model for using specifed thermal load signals.
</p></html>", revisions="<html>
<ul>
<li>
April 13, 2018 by Yangyang Fu:<br/>
First implementation.
</li>
</ul>
</html>"));
end SimplifiedRoom;
