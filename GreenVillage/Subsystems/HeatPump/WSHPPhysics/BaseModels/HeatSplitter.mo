within GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels;
model HeatSplitter
  "Split internal heat to convection, radiation, and latent heat"
  parameter Real fraCon = 0.7 "Fraction of convective heat";
  parameter Real fraRad = 0.2 "Fraction of radiative heat";
  parameter Real fraLat = 0.1 "Fraction of latent heat";

  Modelica.Blocks.Interfaces.RealInput QTot(unit="W/m2") "Total heat"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Buildings.Controls.OBC.CDL.Interfaces.RealOutput QCon(unit="W/m2")
    "Convective heat flow"
    annotation (Placement(transformation(extent={{100,50},{120,70}})));
  Buildings.Controls.OBC.CDL.Interfaces.RealOutput QRad(unit="W/m2") "Radiative heatflow"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Buildings.Controls.OBC.CDL.Interfaces.RealOutput QLat(unit="W/m2") "Laten heat flow"
    annotation (Placement(transformation(extent={{100,-70},{120,-50}})));
initial equation
  fraLat = 1 - fraCon - fraLat;

equation
  {QCon, QRad, QLat} = {fraCon, fraRad, fraLat}.*QTot;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
                                        Text(
        extent={{-150,150},{150,110}},
        textString="%name",
        lineColor={0,0,255})}),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<>
Model that splits total heat flow into three parts (convective, laten, and laten heat) based on prescribed fraction for each part.
<>
</html>"));
end HeatSplitter;
