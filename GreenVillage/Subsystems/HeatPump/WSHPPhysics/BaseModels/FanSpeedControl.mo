within GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels;
model FanSpeedControl "Fan speed control in heat pumps"
  Buildings.Controls.Continuous.LimPID conPID(
    controllerType=controllerType,
    k=k,
    Ti=Ti,
    Td=Td,
    yMax=yMax,
    yMin=yMin,
    reverseAction=reverseAction)
    annotation (Placement(transformation(extent={{-40,30},{-20,50}})));
  Buildings.Controls.OBC.CDL.Interfaces.BooleanInput on "On or off signal"
    annotation (Placement(transformation(extent={{-140,50},{-100,90}})));
  Buildings.Controls.OBC.CDL.Logical.Switch swi
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));
  Buildings.Controls.OBC.CDL.Continuous.Sources.Constant zer(k=0) "Off"
    annotation (Placement(transformation(extent={{-40,-70},{-20,-50}})));
  Modelica.Blocks.Interfaces.RealInput u_s "Setpoint"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealInput u_m "Measurement"
    annotation (Placement(transformation(extent={{-140,-90},{-100,-50}})));
  Buildings.Controls.OBC.CDL.Interfaces.RealOutput spe "Fan speed signal"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  parameter Modelica.Blocks.Types.SimpleController controllerType=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller";
  parameter Real k=1 "Gain of controller";
  parameter Modelica.SIunits.Time Ti=0.5 "Time constant of Integrator block";
  parameter Modelica.SIunits.Time Td=0.1 "Time constant of Derivative block";
  parameter Real yMax=1 "Upper limit of output";
  parameter Real yMin=0 "Lower limit of output";
  parameter Boolean reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller";
equation
  connect(conPID.y, swi.u1)
    annotation (Line(points={{-19,40},{0,40},{0,8},{38,8}}, color={0,0,127}));
  connect(zer.y, swi.u3) annotation (Line(points={{-19,-60},{0,-60},{0,-8},{38,
          -8}}, color={0,0,127}));
  connect(on, swi.u2) annotation (Line(points={{-120,70},{-60,70},{-60,0},{38,0}},
        color={255,0,255}));
  connect(conPID.u_s, u_s) annotation (Line(points={{-42,40},{-80,40},{-80,0},{
          -120,0}}, color={0,0,127}));
  connect(conPID.u_m, u_m) annotation (Line(points={{-30,28},{-30,-20},{-80,-20},
          {-80,-70},{-120,-70}}, color={0,0,127}));
  connect(swi.y, spe)
    annotation (Line(points={{61,0},{110,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
                                        Text(
        extent={{-150,150},{150,110}},
        textString="%name",
        lineColor={0,0,255})}), Diagram(coordinateSystem(preserveAspectRatio=
            false)));
end FanSpeedControl;
