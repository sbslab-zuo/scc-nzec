within GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels.Examples;
model SingleWSHP
  "Single water source heat pump for single zone or room"
 extends Modelica.Icons.Example;
 package Medium = Buildings.Media.Water "Medium model";

 parameter Modelica.SIunits.Power desCooLoa = 3517*3
    "Heat generation of the computer room";
 parameter Modelica.SIunits.MassFlowRate m_flow_nominal = datCoi.sta[datCoi.nSta].nomVal.m_flow_nominal
    "Nominal mass flow rate";
 parameter Modelica.SIunits.PressureDifference dpAir_nominal=200
    "Pressure difference over evaporator at nominal flow rate";
 parameter Modelica.SIunits.PressureDifference dpWat_nominal=100000
    "Pressure difference over condenser at nominal flow rate";

  GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels.SingleWSHP sinHP(
    redeclare package Medium = Medium,
    m_flow_nominal=m_flow_nominal,
    minSpeRat=0.05,
    dpEva_nominal(displayUnit="Pa") = dpAir_nominal,
    dpCon_nominal=dpWat_nominal,
    perCom=datCoi,
    perFan(pressure(V_flow=m_flow_nominal/1.29*{0.1,0.5,1,1.2}, dp=
            dpAir_nominal*{1.2,1.1,1,0.5})),
    rooLen=rooLen,
    rooWid=rooWid,
    rooHei=rooHei,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    AExt=AExt,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=0.126522,
    CExt={5259993.23},
    AInt=AInt,
    alphaInt=2.12,
    nInt=1,
    RInt={0.000668895},
    CInt={123911363.86},
    alphaWinOut=20*14,
    alphaWalOut=25*11.5)
    "Single heat pump system"
    annotation (Placement(transformation(extent={{20,-40},{40,-20}})));

  Buildings.Fluid.Sources.MassFlowSource_T sou(nPorts=1,
    redeclare package Medium = Medium,
    m_flow=0.5)
    "Source"
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));
  Buildings.Fluid.Sources.Boundary_pT sin(nPorts=1, redeclare package Medium =
        Medium)
    "Sink"
    annotation (Placement(transformation(extent={{80,-40},{60,-20}})));
  Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.DXCoil datCoi(nSta=1,
      sta={
        Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.BaseClasses.Stage(
        spe=1800/60,
        nomVal=
          Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-3517*3,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=0.75,
          mCon_flow_nominal=0.5,
          TEvaIn_nominal=273.15 + 26.67,
          TConIn_nominal=273.15 + 29.4),
        perCur=
          Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Examples.PerformanceCurves.Curve_I())})  "Coil data"
    annotation (Placement(transformation(extent={{80,80},{100,100}})));
  Modelica.Blocks.Sources.Step comSpe(
    height=1,
    offset=0,
    startTime=1200) "Compressor speed signal"
    annotation (Placement(transformation(extent={{-80,-70},{-60,-50}})));
  parameter Modelica.SIunits.Length rooLen=8 "Length of the room";
  parameter Modelica.SIunits.Length rooWid=5 "Width of the room";
  parameter Modelica.SIunits.Height rooHei=10 "Height of the room";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaRad = 2
    "Coefficient of heat transfer for linearized radiation exchange between walls";
  parameter Integer nOrientations=2 "Number of orientations";
  parameter Modelica.SIunits.Area AWin[nOrientations]={4,4}
    "Vector of areas of windows by orientations";
  parameter Modelica.SIunits.Area ATransparent[nOrientations]={4,4} "Vector of areas of transparent (solar radiation transmittend) elements by
    orientations";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaWinIn=2.7
    "Convective coefficient of heat transfer of windows (indoor)";
  parameter Modelica.SIunits.ThermalResistance RWin=0.01642857143
    "Resistor for windows";
  parameter Modelica.SIunits.TransmissionCoefficient gWin=0.6
    "Total energy transmittance of windows";
  parameter Real ratioWinConRad=0.09
    "Ratio for windows between indoor convective and radiative heat emission";

  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaExtWalIn=2.7
    "Convective coefficient of heat transfer of exterior walls (indoor)";
  parameter Integer nExt=1 "Number of RC-elements of exterior walls";
  parameter Modelica.SIunits.ThermalResistance RExt[nExt]={0.0033142}
    "Vector of resistances of exterior walls, from inside to outside";

  Modelica.Blocks.Sources.CombiTimeTable difHorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/difHorIrr.txt"))
    "Diffuse horizontal irradiation"
    annotation (Placement(transformation(extent={{-38,80},{-18,100}})));
  Modelica.Blocks.Sources.CombiTimeTable gloHorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/gloHorIrr.txt"))
    "Global horizontal irradiation"
    annotation (Placement(transformation(extent={{-40,50},{-20,70}})));
  Modelica.Blocks.Sources.CombiTimeTable dirNorIrr(
    tableOnFile=true,
    tableName="table",
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/PVPanels/dirNorIrr.txt"))
    "Direct normal irradiation"
    annotation (Placement(transformation(extent={{-40,20},{-20,40}})));
  RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor,
      filNam=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"),
    computeWetBulbTemperature=false)
    "Weather data reader with radiation data obtained from input connector"
    annotation (Placement(transformation(extent={{20,70},{40,90}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=120*10)
                                                     "Convective heat gain"
    annotation (Placement(transformation(extent={{-100,40},{-80,60}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=120)
                                                     "Radiative heat gain"
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{-102,2},{-82,22}})));
  Modelica.Blocks.Routing.Multiplex3 multiplex3_1
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
  Modelica.Blocks.Sources.Step fanSpe(
    height=1,
    offset=0,
    startTime=1200) "Fan speed signal"
    annotation (Placement(transformation(extent={{-80,-100},{-60,-80}})));
equation
  connect(sou.ports[1], sinHP.port_a) annotation (Line(points={{-60,-30},{20,-30}},
                           color={0,127,255}));
  connect(sinHP.port_b, sin.ports[1]) annotation (Line(points={{40,-30},{60,-30}},
                          color={0,127,255}));
  connect(comSpe.y, sinHP.speRatCom) annotation (Line(points={{-59,-60},{-22,-60},
          {-22,-28},{18,-28}},
                            color={0,0,127}));
  connect(difHorIrr.y[1],weaDat. HDifHor_in) annotation (Line(points={{-17,90},{
          -2,90},{-2,70.4},{19,70.4}},color={0,0,127}));
  connect(gloHorIrr.y[1],weaDat. HGloHor_in) annotation (Line(points={{-19,60},{
          -2,60},{-2,68},{19,68}},color={0,0,127}));
  connect(dirNorIrr.y[1],weaDat. HDirNor_in) annotation (Line(points={{-19,30},{
          0,30},{0,66.6},{19,66.6}},  color={0,0,127}));
  connect(weaDat.weaBus, sinHP.weaBus) annotation (Line(
      points={{40,80},{40,34},{21.2,34},{21.2,-22}},
      color={255,204,51},
      thickness=0.5));
  connect(qRadGai_flow.y,multiplex3_1. u1[1])  annotation (Line(
      points={{-79,90},{-70,90},{-70,7},{-62,7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qConGai_flow.y,multiplex3_1. u2[1]) annotation (Line(
      points={{-79,50},{-72,50},{-72,0},{-62,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qLatGai_flow.y,multiplex3_1. u3[1])  annotation (Line(
      points={{-81,12},{-74,12},{-74,-7},{-62,-7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex3_1.y, sinHP.qGai_flow) annotation (Line(points={{-39,0},{0,0},
          {0,-22},{18,-22}},     color={0,0,127}));
  connect(fanSpe.y, sinHP.speRatFan) annotation (Line(points={{-59,-90},{0,-90},
          {0,-34},{18,-34}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StartTime=17366400, StopTime=17452800),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Subsystems/HeatPump/WSHPPhysics/BaseModels/Examples/SingleWSHP.mos"
        "Simulate and Plot"));
end SingleWSHP;
