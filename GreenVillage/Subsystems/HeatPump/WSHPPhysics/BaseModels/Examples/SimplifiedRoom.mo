within GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels.Examples;
model SimplifiedRoom "A room with prescribed thermal loads"
 extends Modelica.Icons.Example;
 package Medium = Buildings.Media.Air "Air";
 parameter Modelica.SIunits.Power desCooLoa = 3517*3
    "Heat generation of the computer room";

 parameter Modelica.SIunits.MassFlowRate m_flow_nominal = desCooLoa/1001/9
    "Nominal mass flow rate";

  GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels.SimplifiedRoom simRoo(
    nPorts=2,
    redeclare package Medium = Medium,
    rooLen=10,
    rooWid=10,
    rooHei=2,
    m_flow_nominal=m_flow_nominal) "Simplified room"
    annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Buildings.Fluid.Sources.Boundary_pT sin(nPorts=1,
    redeclare package Medium = Medium)
    "Sink"
    annotation (Placement(transformation(extent={{80,-40},{60,-20}})));
  Buildings.Fluid.Sources.MassFlowSource_T sou(nPorts=1,
    redeclare package Medium = Medium,
    m_flow=m_flow_nominal)
    "Source"
    annotation (Placement(transformation(extent={{-60,-40},{-40,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable cooLoa(
    extrapolation=Modelica.Blocks.Types.Extrapolation.HoldLastPoint,
    smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    tableOnFile=true,
    tableName="table1",
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/HeatPump/WSHPPhysics/BaseModels/Examples/SimplifiedRoom/cooLoa.txt"))
                        "Cooling load reading from external files"
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));
equation
  connect(sou.ports[1], simRoo.airPorts[1]) annotation (Line(points={{-40,-30},{
          4.475,-30},{4.475,-8.7}}, color={0,127,255}));
  connect(sin.ports[1], simRoo.airPorts[2]) annotation (Line(points={{60,-30},{0.425,
          -30},{0.425,-8.7}}, color={0,127,255}));
  connect(cooLoa.y[1], simRoo.theLoa) annotation (Line(points={{-59,30},{-32,30},
          {-32,4},{-10,4}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=86400),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Subsystems/HeatPump/WSHPPhysics/BaseModels/Examples/SimplifiedRoom.mos"
        "Simulate and Plot"));
end SimplifiedRoom;
