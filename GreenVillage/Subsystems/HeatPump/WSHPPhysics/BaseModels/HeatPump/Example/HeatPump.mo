within GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels.HeatPump.Example;
model HeatPump "Test heat pump model without control"
  import GreenVillage;
 extends Modelica.Icons.Example;

 parameter Modelica.SIunits.Power P_nominal=5000
    "Nominal compressor power (at y=1)";
  parameter Modelica.SIunits.TemperatureDifference dTEva_nominal=-10
    "Temperature difference evaporator inlet-outlet";
  parameter Modelica.SIunits.TemperatureDifference dTCon_nominal=5
    "Temperature difference condenser outlet-inlet";
  parameter Real COPc_nominal=3 "Chiller COP";

  parameter Modelica.SIunits.MassFlowRate m2_flow_nominal=
     P_nominal*COPc_nominal/dTEva_nominal/4200
    "Nominal mass flow rate at chilled water side";
  parameter Modelica.SIunits.MassFlowRate m1_flow_nominal=
    m2_flow_nominal*(COPc_nominal+1)/COPc_nominal
    "Nominal mass flow rate at condenser water wide";
  parameter Modelica.SIunits.Temperature TCon_nominal= 302.59 "Condenser temperature";
  parameter Modelica.SIunits.Temperature TEva_nominal=278.15 "Evaporator temperature";
  parameter Real a[:] = {1}
    "Coefficients for efficiency curve (need p(a=a, y=1)=1)";
  parameter Modelica.SIunits.Pressure dp_nominal=1000 "Nominal pressure difference";
  parameter Real k_hp=1 "Gain of PI controllers in heat pump";
  parameter Real ti_hp=60 "Integral time of PI controller in heat pump";

package Medium = Buildings.Media.Water;
  GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels.HeatPump.HeatPump HP(
    dTEva_nominal=dTEva_nominal,
    dTCon_nominal=dTCon_nominal,
    COPc_nominal=COPc_nominal,
    P_nominal=P_nominal,
    k=k_hp,
    ti=ti_hp,
    redeclare package Medium = Medium,
    m1_flow_nominal=m1_flow_nominal,
    m2_flow_nominal=m2_flow_nominal,
    TCon_nominal=TCon_nominal,
    TEva_nominal=TEva_nominal,
    dp_nominal=dp_nominal)
    annotation (Placement(transformation(extent={{-4,-4},{24,16}})));
  Buildings.Fluid.Sources.Boundary_pT sink1(
    nPorts=1,
    use_T_in=false,
    p(displayUnit="Pa") = 1000,
    redeclare package Medium = Medium)
    annotation (Placement(transformation(extent={{-40,36},{-20,56}})));
  Buildings.Fluid.Sources.Boundary_pT sink2(
    nPorts=1,
    use_T_in=false,
    p(displayUnit="Pa") = 2000,
    T=273.15 + 5,
    redeclare package Medium = Medium)
    annotation (Placement(transformation(extent={{-40,-44},{-20,-24}})));
  Buildings.Fluid.Sources.MassFlowSource_T source1(nPorts=1,
    redeclare package Medium = Medium,
    m_flow=10)
    annotation (Placement(transformation(extent={{80,36},{60,56}})));
  Buildings.Fluid.Sources.MassFlowSource_T source2(nPorts=1,
    redeclare package Medium = Medium,
    m_flow=10)
    annotation (Placement(transformation(extent={{80,-44},{60,-24}})));
  Modelica.Blocks.Sources.Step cooOn(height=0, offset=1)
    annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
  Modelica.Blocks.Sources.Step on(height=0, offset=1)
    annotation (Placement(transformation(extent={{-100,-20},{-80,0}})));
  Modelica.Blocks.Sources.Constant const(k=273.15 + 7)
    annotation (Placement(transformation(extent={{-100,-60},{-80,-40}})));
equation
  connect(sink1.ports[1], HP.port_b2) annotation (Line(
      points={{-20,46},{-10,46},{4,46},{4,16}},
      color={0,127,255},
      thickness=1));
  connect(sink2.ports[1], HP.port_b1) annotation (Line(
      points={{-20,-34},{4,-34},{4,-4}},
      color={0,127,255},
      thickness=1));
  connect(source1.ports[1], HP.port_a2) annotation (Line(
      points={{60,46},{40,46},{16,46},{16,16}},
      color={0,127,255},
      thickness=1));
  connect(source2.ports[1], HP.port_a1) annotation (Line(
      points={{60,-34},{16,-34},{16,-4}},
      color={0,127,255},
      thickness=1));
  connect(cooOn.y, HP.CoolOn) annotation (Line(
      points={{-79,30},{-60,30},{-60,14},{-6,14}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(on.y, HP.On) annotation (Line(
      points={{-79,-10},{-60,-10},{-60,6},{-6,6}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(const.y, HP.TSetPoi) annotation (Line(
      points={{-79,-50},{-60,-50},{-60,-2},{-6,-2}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end HeatPump;
