within GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels;
model SingleWSHP "Single WSHP system for a single zone or room"
  final package Medium1 = Buildings.Media.Air
    "Medium model";
  replaceable package Medium = Modelica.Media.Interfaces.PartialMedium
    "Medium model";
  replaceable parameter
    Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.DXCoil perCom
    constrainedby
    Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.DXCoil
    "Performance data for compressor"
    annotation (Placement(transformation(extent={{84,144},{96,156}})));
  replaceable parameter Buildings.Fluid.Movers.Data.Generic perFan
    constrainedby Buildings.Fluid.Movers.Data.Generic
    "Record with performance data" annotation (choicesAllMatching=true,
      Placement(transformation(extent={{52,142},{66,156}})));

  parameter Modelica.SIunits.Length rooLen "Length of the room";
  parameter Modelica.SIunits.Length rooWid "Width of the room";
  parameter Modelica.SIunits.Height rooHei "Height of the room";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = perCom.sta[perCom.nSta].nomVal.m_flow_nominal
    "Nominal mass flow rate";
  parameter Boolean computeReevaporation=true
    "Set to true to compute reevaporation of water that accumulated on coil";
  parameter Real minSpeRat "Minimum speed ratio";
  parameter Modelica.SIunits.PressureDifference dpEva_nominal
    "Pressure difference over evaporator at nominal flow rate";
  parameter Modelica.SIunits.PressureDifference dpCon_nominal
    "Pressure difference over condenser at nominal flow rate";
  parameter Boolean addPowerToMedium=false
    "Set to false to avoid any power (=heat and flow work) being added to medium (may give simpler equations)";

  Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.VariableSpeed varSpeHeaPum(
    redeclare final package MediumEva = Medium1,
    redeclare package MediumCon = Medium,
    dpEva_nominal=dpEva_nominal,
    dpCon_nominal=dpCon_nominal,
    computeReevaporation=computeReevaporation,
    minSpeRat=minSpeRat,
    datCoi=perCom)
    "Variable speed heat pump "
    annotation (Placement(transformation(extent={{10,-70},{-10,-50}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senSupAir(redeclare final package
      Medium = Medium1, final m_flow_nominal=m_flow_nominal)
    "Supply air temperature sensor"
    annotation (Placement(transformation(extent={{-36,-70},{-56,-50}})));
  Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium = Medium)
    "Fluid connector a of condenser (positive design flow direction is from port_a2 to port_b2)"
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium = Medium)
    "Fluid connector b of condenser (positive design flow direction is from port_a2 to port_b2)"
    annotation (Placement(transformation(extent={{90,-10},{110,10}})));
  Modelica.Blocks.Interfaces.RealInput speRatCom(min=0, max=1)
    "Speed ratio for heat pump"
    annotation (Placement(transformation(extent={{-140,0},{-100,40}})));
  Modelica.Blocks.Interfaces.RealOutput TSupAir(unit="K", displayUnit="degC")
    "Temperature of the passing fluid"
    annotation (Placement(transformation(extent={{100,30},{120,50}})));
  Modelica.Blocks.Interfaces.RealOutput TRooAir(unit="K", displayUnit="degC")
   "Room air temperature"
    annotation (Placement(transformation(extent={{100,60},{120,80}})));

  Modelica.Blocks.Interfaces.RealOutput PCom(quantity="Power", unit="W")
    "Electrical power consumed by the unit"
    annotation (Placement(transformation(extent={{100,-50},{120,-30}})));
  Buildings.Fluid.Movers.SpeedControlled_y fan(redeclare package Medium =
        Medium1, addPowerToMedium=addPowerToMedium,
    per=perFan)
    annotation (Placement(transformation(extent={{-50,-30},{-30,-10}})));
  Modelica.Blocks.Interfaces.RealInput speRatFan
    "Constant normalized rotational speed for supply fan"
    annotation (Placement(transformation(extent={{-140,-60},{-100,-20}})));

  Modelica.Blocks.Interfaces.RealOutput PFan(quantity="Power", unit="W")
   "Electrical power consumed"
    annotation (Placement(transformation(extent={{100,-90},{120,-70}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senConWatIn(redeclare final
      package Medium = Medium, final m_flow_nominal=perCom.sta[perCom.nSta].nomVal.mCon_flow_nominal)
    "Inlet condenser water temperature"
    annotation (Placement(transformation(extent={{-60,-96},{-40,-76}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senConWatOut(redeclare final
      package Medium = Medium, final m_flow_nominal=perCom.sta[perCom.nSta].nomVal.mCon_flow_nominal)
    "Inlet condenser water temperature"
    annotation (Placement(transformation(extent={{40,-96},{60,-76}})));
  Buildings.ThermalZones.ReducedOrder.RC.TwoElements theZon(
    nPorts=2,
    VAir=VAir,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    AExt=AExt,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    AInt=AInt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    redeclare package Medium = Medium1,
    alphaWin=alphaWinIn,
    alphaExt=alphaExtWalIn)
    "Thermal zones"
    annotation (Placement(transformation(extent={{50,68},{70,88}})));
  Buildings.HeatTransfer.Sources.PrescribedTemperature preTem
    "Prescribed temperature for exterior walls outdoor surface temperature"
    annotation (Placement(transformation(extent={{-50,50},{-30,70}})));
  Buildings.HeatTransfer.Sources.PrescribedTemperature preTem1
    "Prescribed temperature for windows outdoor surface temperature"
    annotation (Placement(transformation(extent={{-50,80},{-30,100}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus "Weather data bus"
    annotation (Placement(transformation(extent={{-110,60},{-76,92}}),
    iconTransformation(extent={{-98,70},{-78,90}})));
  Buildings.BoundaryConditions.SolarIrradiation.DiffusePerez HDifTil[
    nOrientations](
    each outSkyCon=true,
    each outGroCon=true,
    each til=1.5707963267949,
    each lat=0.87266462599716,
    azi={3.1415926535898,4.7123889803847})
    "Calculates diffuse solar radiation on titled surface for both directions"
    annotation (Placement(transformation(extent={{-80,108},{-60,128}})));
  Buildings.BoundaryConditions.SolarIrradiation.DirectTiltedSurface HDirTil[
    nOrientations](
    each til=1.5707963267949,
    each lat=0.87266462599716,
    azi={3.1415926535898,4.7123889803847})
    "Calculates direct solar radiation on titled surface for both directions"
    annotation (Placement(transformation(extent={{-80,140},{-60,160}})));
  Buildings.ThermalZones.ReducedOrder.SolarGain.CorrectionGDoublePane corGDouPan(
     UWin=2.1, n=nOrientations)
    "Correction factor for solar transmission"
    annotation (Placement(transformation(extent={{-20,140},{0,160}})));
  Modelica.Thermal.HeatTransfer.Components.Convection theConWin
    "Outdoor convective heat transfer of windows"
    annotation (Placement(transformation(extent={{30,86},{10,106}})));
  Modelica.Blocks.Sources.Constant alpWinOut(k=alphaWinOut)
    "Outdoor coefficient of heat transfer for windows" annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-10,120})));
  Modelica.Blocks.Sources.Constant alpWalOut(k=alphaWalOut)
    "Outdoor coefficient of heat transfer for walls"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
                                                                rotation=180,
    origin={-10,30})));
  Modelica.Thermal.HeatTransfer.Components.Convection theConWall
    "Outdoor convective heat transfer of walls"
    annotation (Placement(transformation(extent={{30,68},{10,48}})));
  final parameter Modelica.SIunits.Volume VAir = rooLen*rooWid*rooHei "Air volume of the zone";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaRad
    "Coefficient of heat transfer for linearized radiation exchange between walls";
  parameter Integer nOrientations = 2 "Number of orientations";
  parameter Modelica.SIunits.Area AWin[nOrientations]
    "Vector of areas of windows by orientations";
  parameter Modelica.SIunits.Area ATransparent[nOrientations]
    "Vector of areas of transparent (solar radiation transmittend) elements by
    orientations";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaWinIn
    "Convective coefficient of heat transfer of windows (indoor)";
  parameter Modelica.SIunits.ThermalResistance RWin "Resistor for windows";
  parameter Modelica.SIunits.TransmissionCoefficient gWin
    "Total energy transmittance of windows";
  parameter Real ratioWinConRad
    "Ratio for windows between indoor convective and radiative heat emission";
  final parameter Modelica.SIunits.Area AExt[nOrientations]={rooLen*rooHei,rooWid*
      rooHei} "Vector of areas of exterior walls by orientations";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaExtWalIn
    "Convective coefficient of heat transfer of exterior walls (indoor)";
  parameter Integer nExt "Number of RC-elements of exterior walls";
  parameter Modelica.SIunits.ThermalResistance RExt[nExt]
    "Vector of resistances of exterior walls, from inside to outside";
  parameter Modelica.SIunits.ThermalResistance RExtRem
    "Resistance of remaining resistor RExtRem between capacity n and outside";
  parameter Modelica.SIunits.HeatCapacity CExt[nExt]
    "Vector of heat capacities of exterior walls, from inside to outside";
  final parameter Modelica.SIunits.Area AInt=2*rooHei*rooLen "Area of interior walls";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaInt
    "Convective coefficient of heat transfer of interior walls (indoor)";
  parameter Integer nInt "Number of RC-elements of interior walls";
  parameter Modelica.SIunits.ThermalResistance RInt[nInt]
    "Vector of resistances of interior walls, from port to center";
  parameter Modelica.SIunits.HeatCapacity CInt[nInt]
    "Vector of heat capacities of interior walls, from port to center";

  parameter Real alphaWinOut "Convective coefficient of heat transfer of windows (outdoor)";
  parameter Real alphaWalOut "Convective coefficient of heat transfer of exterior walls (outdoor)";

protected
  Buildings.ThermalZones.Detailed.BaseClasses.HeatGain heaGai(final AFlo=rooLen*
        rooWid)
    "Model to convert internal heat gains"
    annotation (Placement(transformation(extent={{60,120},{80,140}})));
public
  Modelica.Blocks.Interfaces.RealInput qGai_flow[3](each unit="W/m2")
    "Radiant, convective and latent heat input into room (positive if heat gain)"
    annotation (Placement(transformation(extent={{-140,30},{-100,70}}),
        iconTransformation(extent={{-140,60},{-100,100}})));
protected
  Buildings.HeatTransfer.Sources.PrescribedHeatFlow
                                          conQCon_flow
    "Converter for convective heat flow rate"
    annotation (Placement(transformation(extent={{100,100},{120,120}})));
  Buildings.HeatTransfer.Sources.PrescribedHeatFlow conQRad_flow
    "Converter for radiative heat flow rate"
    annotation (Placement(transformation(extent={{100,120},{120,140}})));
equation
  connect(varSpeHeaPum.port_b, senSupAir.port_a)
    annotation (Line(points={{-10,-60},{-24,-60},{-36,-60}},
                                                 color={0,127,255}));
  connect(varSpeHeaPum.speRat, speRatCom) annotation (Line(points={{11.2,-52},{20,
          -52},{20,-34},{-74,-34},{-74,2},{-98,2},{-98,20},{-120,20}},
                                                     color={0,0,127}));
  connect(senSupAir.T, TSupAir)
    annotation (Line(points={{-46,-49},{-46,-36},{78,-36},{78,40},{110,40}},
                                                          color={0,0,127}));
  connect(varSpeHeaPum.P, PCom) annotation (Line(points={{-11,-51},{-20,-51},{-20,
          -40},{84,-40},{84,-40},{110,-40}}, color={0,0,127}));
  connect(senSupAir.port_b, fan.port_a) annotation (Line(points={{-56,-60},{-60,
          -60},{-60,-20},{-50,-20}},
                                   color={0,127,255}));
  connect(fan.y, speRatFan) annotation (Line(points={{-40,-8},{-40,-2},{-76,-2},
          {-76,-40},{-120,-40}}, color={0,0,127}));
  connect(fan.P, PFan) annotation (Line(points={{-29,-11},{-24,-11},{-24,-38},{-20,
          -38},{-20,-38},{82,-38},{82,-80},{110,-80}},
                                    color={0,0,127}));
  connect(varSpeHeaPum.portCon_a, senConWatIn.port_b)
    annotation (Line(points={{-6,-70},{-6,-86},{-40,-86}}, color={0,127,255}));
  connect(senConWatIn.port_a, port_a) annotation (Line(points={{-60,-86},{-80,-86},
          {-80,0},{-100,0}},      color={0,127,255}));
  connect(varSpeHeaPum.portCon_b, senConWatOut.port_a)
    annotation (Line(points={{6,-70},{6,-86},{40,-86}}, color={0,127,255}));
  connect(senConWatOut.port_b, port_b) annotation (Line(points={{60,-86},{80,-86},
          {80,0},{100,0}},      color={0,127,255}));
  connect(corGDouPan.solarRadWinTrans, theZon.solRad) annotation (Line(points={{1,150},
          {40,150},{40,86.3333},{49.5833,86.3333}},        color={0,0,127}));

for i in 1:nOrientations loop
  connect(weaBus, HDirTil[i].weaBus) annotation (Line(
      points={{-93,76},{-93,150},{-80,150}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HDifTil[i].weaBus) annotation (Line(
      points={{-93,76},{-93,118},{-80,118}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(HDirTil[i].H, corGDouPan.HDirTil[i]) annotation (Line(points={{-59,150},{-40,
          150},{-40,156},{-22,156}}, color={0,0,127}));
  connect(HDirTil[i].inc, corGDouPan.inc[i]) annotation (Line(points={{-59,146},{-40,146},
          {-40,144},{-22,144}}, color={0,0,127}));
  connect(HDifTil[i].HSkyDifTil, corGDouPan.HSkyDifTil[i]) annotation (Line(points={{-59,
          124},{-40,124},{-36,124},{-36,152},{-22,152}}, color={0,0,127}));
  connect(HDifTil[i].HGroDifTil, corGDouPan.HGroDifTil[i]) annotation (Line(points={{-59,
          112},{-34,112},{-34,148},{-22,148}}, color={0,0,127}));

end for;
  connect(alpWinOut.y, theConWin.Gc)
    annotation (Line(points={{1,120},{20,120},{20,106}}, color={0,0,127}));
  connect(preTem1.port, theConWin.fluid) annotation (Line(points={{-30,90},{-8,90},
          {-8,96},{10,96}}, color={191,0,0}));
  connect(theConWin.solid, theZon.window) annotation (Line(points={{30,96},{36,
          96},{36,80.2222},{50,80.2222}},
                                      color={191,0,0}));
  connect(alpWalOut.y, theConWall.Gc)
    annotation (Line(points={{1,30},{20,30},{20,48}}, color={0,0,127}));
  connect(theConWall.solid, theZon.extWall) annotation (Line(points={{30,58},{
          36,58},{36,75.7778},{50,75.7778}},
                                          color={191,0,0}));
  connect(preTem.port, theConWall.fluid) annotation (Line(points={{-30,60},{-8,60},
          {-8,58},{10,58}}, color={191,0,0}));
  connect(fan.port_b, theZon.ports[1]) annotation (Line(points={{-30,-20},{0,
          -20},{0,0},{65.6146,0},{65.6146,68.0278}},    color={0,127,255}));
  connect(varSpeHeaPum.port_a, theZon.ports[2]) annotation (Line(points={{10,-60},
          {66.8854,-60},{66.8854,68.0278}}, color={0,127,255}));
  connect(theZon.TAir, TRooAir) annotation (Line(points={{70.4167,86.8889},{80,
          86.8889},{80,70},{110,70}},
                             color={0,0,127}));
  connect(weaBus.TDryBul, preTem1.T) annotation (Line(
      points={{-93,76},{-76,76},{-76,90},{-52,90}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, preTem.T) annotation (Line(
      points={{-93,76},{-76,76},{-76,60},{-52,60}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(qGai_flow,heaGai. qGai_flow) annotation (Line(
      points={{-120,50},{-96,50},{-96,166},{42,166},{42,130},{58,130}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(heaGai.QRad_flow, conQRad_flow.Q_flow) annotation (Line(points={{82,136},
          {90,136},{90,130},{100,130}}, color={0,0,127}));
  connect(heaGai.QCon_flow, conQCon_flow.Q_flow) annotation (Line(points={{82,130},
          {88,130},{88,110},{100,110}}, color={0,0,127}));
  connect(conQRad_flow.port, theZon.intGainsRad) annotation (Line(points={{120,130},
          {130,130},{130,82.4444},{70,82.4444}}, color={191,0,0}));
  connect(conQCon_flow.port, theZon.intGainsConv) annotation (Line(points={{120,110},
          {128,110},{128,84},{74,84},{74,80.2222},{70,80.2222}},      color={191,
          0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false),graphics={
        Rectangle(
          extent={{-74,20},{80,-74}},
          lineColor={0,0,255},
          pattern=LinePattern.None,
          fillColor={95,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-99,4},{102,-6}},
          lineColor={0,0,255},
          pattern=LinePattern.None,
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-102,-6},{-2,4}},
          lineColor={0,0,255},
          pattern=LinePattern.None,
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),                                 Line(
            points={{-100,80},{-68,80},{-68,20}}, color={28,108,200}),
        Line(
          points={{0,96},{-40,80}},
          color={0,0,0},
          thickness=1),
        Line(
          points={{0,96},{40,80}},
          color={0,0,0},
          thickness=1),
        Line(
          points={{-54,72},{-20,72}},
          color={28,108,200},
          thickness=1),
        Ellipse(
          extent={{-46,58},{-62,42}},
          lineColor={28,108,200},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-54,72},{-54,58}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{-54,42},{-54,20}},
          color={28,108,200},
          thickness=1),
        Line(
          points={{20,72},{60,72},{60,20}},
          color={28,108,200},
          thickness=1),
        Polygon(
          points={{-20,88},{-20,40},{20,40},{20,88},{0,96},{-20,88}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={85,255,85},
          fillPattern=FillPattern.Forward),
        Polygon(
          points={{-54,58},{-60,44},{-48,44},{-54,58}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),Text(
          extent={{-180,174},{168,128}},
          lineColor={0,0,255},
          textString="%name")}),Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,160}})),
    Documentation(revisions="<html>
<ul>
<li>
April 13, 2018 by Yangyang Fu:<br/>
First implementation.
</li>
</ul>
</html>", info="<html>
This model describes a single water source heat pump for a signle zone or room. The control signal for the loop can be input to the model by input connectors.
</html>"));
end SingleWSHP;
