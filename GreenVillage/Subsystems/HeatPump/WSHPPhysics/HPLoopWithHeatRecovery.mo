within GreenVillage.Subsystems.HeatPump.WSHPPhysics;
model HPLoopWithHeatRecovery "Water source heat pump loop for HGV"

 extends GreenVillage.Subsystems.HeatPump.WSHPPhysics.HPLoop;

  Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium =
        Medium)
    "Fluid connector b (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{70,-150},{90,-130}})));
  Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
        Medium)
    "Fluid connector a of condenser (positive design flow direction is from port_a2 to port_b2)"
    annotation (Placement(transformation(extent={{-90,-150},{-70,-130}})));
  Buildings.Fluid.FixedResistances.PressureDrop res2(
    redeclare package Medium = Medium,
    dp_nominal=100,
    from_dp=true,
    m_flow_nominal=datHP9.sta[datHP9.nSta].nomVal.mCon_flow_nominal)
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=-90,
        origin={-40,-84})));
  Buildings.Fluid.FixedResistances.PressureDrop res3(
    redeclare package Medium = Medium,
    dp_nominal=100,
    from_dp=true,
    m_flow_nominal=datHP8.sta[datHP8.nSta].nomVal.mCon_flow_nominal)
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=-90,
        origin={-64,-84})));
equation
  connect(port_a, res2.port_a) annotation (Line(points={{-80,-140},{-40,-140},{
          -40,-94}}, color={0,127,255}));
  connect(res2.port_b, HP9.port_a) annotation (Line(points={{-40,-74},{-40,-48},
          {218,-48},{218,50}}, color={0,127,255}));
  connect(port_a, res3.port_a) annotation (Line(points={{-80,-140},{-64,-140},{
          -64,-94}}, color={0,127,255}));
  connect(res3.port_b, HP8.port_a) annotation (Line(points={{-64,-74},{-64,-46},
          {176,-46},{176,50}}, color={0,127,255}));
  connect(pum9.port_b, port_b) annotation (Line(points={{248,6},{258,6},{258,
          -140},{80,-140}}, color={0,127,255}));
  connect(pum8.port_b, port_b) annotation (Line(points={{200,6},{228,6},{228,
          -140},{80,-140}}, color={0,127,255}));

end HPLoopWithHeatRecovery;
