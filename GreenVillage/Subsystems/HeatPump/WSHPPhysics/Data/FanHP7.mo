within GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data;
record FanHP7 "Performance data for the pump in heat pump 7"
   parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.96288;
   parameter Modelica.SIunits.Power P_nominal = 560;
   parameter Modelica.SIunits.Pressure dp_nominal = 1700;
   Buildings.Fluid.Movers.Data.Generic per(
    use_powerCharacteristic=true,
    speed_rpm_nominal=3000,
    power(V_flow=m_flow_nominal/1.2*{0.122180451,0.233082707,0.347744361,0.458646617,0.588345865,0.731203008,0.862781955,1,1.103383459,1.137218045},
          P=P_nominal*{0.513542642,0.593525017,0.687007461,0.757952829,0.847974132,0.925872941,0.968371459,1,1.017436783,1.022438541}),
    pressure(V_flow=m_flow_nominal/1.2*{0.122180451,0.233082707,0.347744361,0.458646617,0.588345865,0.731203008,0.862781955,1,1.103383459,1.137218045},
          dp=dp_nominal*{1.550824534,1.536424663,1.514654242,1.485488454,1.423161395,1.316580015,1.184121056,1, 0.85637354, 0.801113028}));

end FanHP7;
