within GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data;
record HeatPump5 =
  Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.DXCoil (
  final nSta=1,
  sta={ Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.BaseClasses.Stage(
        spe=1800,
        nomVal=
          Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-11066.32,
          COP_nominal=5,
          SHR_nominal=0.76,
          m_flow_nominal=0.67968,
          TEvaIn_nominal=273.15 + 27,
          TConIn_nominal=273.15 + 25,
          mCon_flow_nominal=7.947,
          gamma = 0.5),
    perCur=
          Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Examples.PerformanceCurves.Curve_I())})
  "Performance data for heat pump 5";
