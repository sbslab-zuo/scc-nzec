within GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data;
record HeatPump3 =
  Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.DXCoil (
  final nSta=1,
  sta={ Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.BaseClasses.Stage(
        spe=1800,
        nomVal=
          Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-8223.544,
          COP_nominal=5.086,
          SHR_nominal=0.77,
          m_flow_nominal=0.5381,
          TEvaIn_nominal=273.15 + 27,
          TConIn_nominal=273.15 + 25,
          mCon_flow_nominal=7.064,
          gamma = 0.5),
    perCur=
          Buildings.Fluid.HeatExchangers.DXCoils.WaterCooled.Examples.PerformanceCurves.Curve_I())})
  "Performance data for heat pump 3";
