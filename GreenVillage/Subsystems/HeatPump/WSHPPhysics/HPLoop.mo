within GreenVillage.Subsystems.HeatPump.WSHPPhysics;
model HPLoop "Water source heat pump loop for HGV"
 package Medium = Buildings.Media.Water "Medium model";

  replaceable package PhaseSystem =
      Buildings.Electrical.PhaseSystems.OnePhase
      constrainedby Buildings.Electrical.PhaseSystems.PartialPhaseSystem;

// parameter Modelica.SIunits.Power desCooLoa = 3517*3
//    "Heat generation of the computer room";
// parameter Modelica.SIunits.MassFlowRate mAir_flow_nominal = 0.75
//    "Nominal mass flow rate";


 parameter Modelica.SIunits.PressureDifference dpAir_hp_nominal = 1000
    "Pressure difference over evaporator at nominal flow rate";
 parameter Modelica.SIunits.PressureDifference dpWat_hp_nominal = 100000
    "Pressure difference over condenser at nominal flow rate";


 //Parameters for the heat exchanger
  parameter Modelica.SIunits.Efficiency eps = 0.8 "Heat exchanger effectiveness";

  parameter Modelica.SIunits.PressureDifference dp1_hx_nominal = 10000
    "Pressure difference";
  parameter Modelica.SIunits.PressureDifference dp2_hx_nominal = 10000
    "Pressure difference";

 //Parameters for underground water
   parameter Modelica.SIunits.MassFlowRate m_well_flow_nominal= m2_hx_flow_nominal
    "Nominal mass flow rate";



 // performance data for pumps
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum1 "Performance data for pump 1";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum2 "Performance data for pump 2";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum3 "Performance data for pump 3";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum4 "Performance data for pump 4";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum5 "Performance data for pump 5";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum6 "Performance data for pump 6";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum7 "Performance data for pump 7";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum8 "Performance data for pump 8";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.PumpHP1[1] perPum9 "Performance data for pump 9";

  // performance data for fans
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP1 perFan1 "Performance data for fan 1";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP2 perFan2 "Performance data for fan 2";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP3 perFan3 "Performance data for fan 3";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP4 perFan4 "Performance data for fan 4";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP5 perFan5 "Performance data for fan 5";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP6 perFan6 "Performance data for fan 6";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP7 perFan7 "Performance data for fan 7";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP8 perFan8 "Performance data for fan 8";
  replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.FanHP9 perFan9 "Performance data for fan 9";

  //performance data for heat pumps

 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump1 datHP1
    "Peformance data for heat pump 1"
    annotation (Placement(transformation(extent={{-204,126},{-184,146}})));

 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump2 datHP2
    "Peformance data for heat pump 2"
    annotation (Placement(transformation(extent={{-134,126},{-114,146}})));
 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump3 datHP3
    "Peformance data for heat pump 3"
    annotation (Placement(transformation(extent={{-80,126},{-60,146}})));
 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump4 datHP4
    "Peformance data for heat pump 4"
    annotation (Placement(transformation(extent={{-22,128},{-2,148}})));

 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump5 datHP5
    "Peformance data for heat pump 5"
    annotation (Placement(transformation(extent={{40,126},{60,146}})));
 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump6 datHP6
    "Peformance data for heat pump 6"
    annotation (Placement(transformation(extent={{90,126},{110,146}})));

 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump7 datHP7
    "Peformance data for heat pump 7"
    annotation (Placement(transformation(extent={{136,126},{156,146}})));
 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump8 datHP8
    "Peformance data for heat pump 8"
    annotation (Placement(transformation(extent={{182,126},{202,146}})));
 replaceable parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump9 datHP9
    "Peformance data for heat pump 9"
    annotation (Placement(transformation(extent={{220,124},{240,144}})));



 // parameters for room
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaRad = 2
    "Coefficient of heat transfer for linearized radiation exchange between walls";
  parameter Integer nOrientations=2 "Number of orientations";
  parameter Modelica.SIunits.Area AWin[nOrientations]={4,4}
    "Vector of areas of windows by orientations";
  parameter Modelica.SIunits.Area ATransparent[nOrientations]={4,4} "Vector of areas of transparent (solar radiation transmittend) elements by
    orientations";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaWinIn=2.7
    "Convective coefficient of heat transfer of windows (indoor)";
  parameter Modelica.SIunits.ThermalResistance RWin=0.01642857143
    "Resistor for windows";
  parameter Modelica.SIunits.TransmissionCoefficient gWin=0.6
    "Total energy transmittance of windows";
  parameter Real ratioWinConRad=0.09
    "Ratio for windows between indoor convective and radiative heat emission";

  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaExtWalIn=2.7
    "Convective coefficient of heat transfer of exterior walls (indoor)";
  parameter Integer nExt=1 "Number of RC-elements of exterior walls";
  parameter Modelica.SIunits.ThermalResistance RExt[nExt]=fill(0.33142,nExt)
    "Vector of resistances of exterior walls, from inside to outside";

    parameter Modelica.SIunits.ThermalResistance RInt[nInt]=fill(0.0168895,nInt)
    "Vector of resistances of interior walls, from port to center";
  parameter Modelica.SIunits.HeatCapacity CInt[nInt]=fill(123911363.86,nInt)
    "Vector of heat capacities of interior walls, from port to center";
  parameter Modelica.SIunits.ThermalResistance RExtRem=0.126522
    "Resistance of remaining resistor RExtRem between capacity n and outside";
  parameter Modelica.SIunits.HeatCapacity CExt[nExt]=fill(5259993.23,nExt)
    "Vector of heat capacities of exterior walls, from inside to outside";
  parameter Integer nInt=1 "Number of RC-elements of interior walls";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer alphaInt=2.12
    "Convective coefficient of heat transfer of interior walls (indoor)";
  parameter Real alphaWinOut = 150
    "Convective coefficient of heat transfer of windows (outdoor)";
  parameter Real alphaWalOut= 150
    "Convective coefficient of heat transfer of exterior walls (outdoor)";

 //final parameters
 final parameter Modelica.SIunits.MassFlowRate m1_hx_flow_nominal = datHP1.sta[datHP1.nSta].nomVal.mCon_flow_nominal+
      datHP2.sta[datHP2.nSta].nomVal.mCon_flow_nominal+
      datHP3.sta[datHP3.nSta].nomVal.mCon_flow_nominal+
      datHP4.sta[datHP4.nSta].nomVal.mCon_flow_nominal+
      datHP5.sta[datHP5.nSta].nomVal.mCon_flow_nominal+
      datHP6.sta[datHP6.nSta].nomVal.mCon_flow_nominal+
      datHP7.sta[datHP7.nSta].nomVal.mCon_flow_nominal+
      datHP8.sta[datHP8.nSta].nomVal.mCon_flow_nominal+
      datHP9.sta[datHP9.nSta].nomVal.mCon_flow_nominal
    "Nominal flowrate in heat exchanger";
 final parameter Modelica.SIunits.MassFlowRate m2_hx_flow_nominal = m1_hx_flow_nominal
    "Nominal mass flow rate";


  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum1(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    m_flow_nominal=perPum1[1].m_flow_nominal,
    per=perPum1.per,
    use_inputFilter=false)
    "Pump 1" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-172,20})));
  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum2(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    per=perPum2.per,
    m_flow_nominal=perPum2[1].m_flow_nominal,
    use_inputFilter=false)
    "Pump 2"
          annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-106,20})));
  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum3(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    per=perPum3.per,
    m_flow_nominal=perPum3[1].m_flow_nominal,
    use_inputFilter=false)
    "Pump 3"
          annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-50,20})));
  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum4(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    per=perPum4.per,
    m_flow_nominal=perPum4[1].m_flow_nominal,
    use_inputFilter=false)
    "Pump 4"
          annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={10,18})));
  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum5(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    per=perPum5.per,
    m_flow_nominal=perPum5[1].m_flow_nominal,
    use_inputFilter=false)
    "Pump 5"
          annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={68,20})));
  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum6(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    per=perPum6.per,
    m_flow_nominal=perPum6[1].m_flow_nominal,
    use_inputFilter=false)
    "Pump 6"
          annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={120,18})));
  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum7(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    per=perPum7.per,
    m_flow_nominal=perPum7[1].m_flow_nominal,
    use_inputFilter=false)
    "Pump 7"
          annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={160,18})));
  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum8(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    per=perPum8.per,
    m_flow_nominal=perPum8[1].m_flow_nominal,
    use_inputFilter=false)
    "Pump 8"
          annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={200,16})));
  Buildings.Applications.DataCenters.ChillerCooled.Equipment.FlowMachine_y pum9(
    num=1,
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    per=perPum9.per,
    m_flow_nominal=perPum9[1].m_flow_nominal,
    use_inputFilter=false)
    "Pump 9"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={248,16})));

 GreenVillage.Subsystems.HeatPump.WSHPPhysics.BaseModels.SingleWSHP HP1(
    redeclare package Medium = Medium,
    perCom=datHP1,
    minSpeRat=0.05,
    m_flow_nominal=datHP1.sta[datHP1.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan=perFan1.per,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut,
    rooLen=8,
    rooWid=8,
    rooHei=10)
    "Heat pump for commericial building F1 and F2. F1 is Bakery, and F2 is Gallery."
    annotation (Placement(transformation(extent={{-206,40},{-186,60}})));
  Buildings.Fluid.Sources.Boundary_pT bou(
    nPorts=1,
    redeclare package Medium = Medium)
    annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={184,-106})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary(nPorts=1,
    redeclare package Medium = Medium,
    m_flow=m_well_flow_nominal,
    T=299.85)
    annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={182,-62})));
  Buildings.Fluid.HeatExchangers.ConstantEffectiveness hex(
    redeclare package Medium1 = Medium,
    redeclare package Medium2 = Medium,
    m1_flow_nominal=m1_hx_flow_nominal,
    m2_flow_nominal=m2_hx_flow_nominal,
    eps=eps,
    dp1_nominal=dp1_hx_nominal,
    dp2_nominal=dp2_hx_nominal)
    annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={142,-82})));
  Buildings.Fluid.Storage.ExpansionVessel exp(
    redeclare package Medium = Medium,
      V_start=2)
    annotation (Placement(transformation(extent={{90,-84},{110,-64}})));
  Buildings.Controls.SetPoints.OccupancySchedule occSchCom(occupancy=3600*{6,18})
    "Occupancy schedule for commercial buildings"
    annotation (Placement(transformation(extent={{-430,100},{-410,120}})));
  Buildings.Controls.OBC.CDL.Conversions.BooleanToReal booToRea
    annotation (Placement(transformation(extent={{-372,62},{-352,82}})));
  BaseModels.FanSpeedControl           supAirCon1(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{-204,90},{-184,110}})));
  Buildings.Controls.OBC.CDL.Continuous.Sources.Constant rooAirSet(k=273.15 + 24)
    "Room air set point"
    annotation (Placement(transformation(extent={{-244,94},{-224,114}})));

 BaseModels.SingleWSHP                                              HP2(
    redeclare package Medium = Medium,
    minSpeRat=0.05,
    m_flow_nominal=datHP2.sta[datHP2.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan = perFan2.per,
    perCom=datHP2,
    rooLen=5,
    rooWid=5,
    rooHei=5,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut)      "Heat pump for commericial building Pilsbury G1"
    annotation (Placement(transformation(extent={{-140,40},{-120,60}})));
 BaseModels.SingleWSHP                                              HP3(
    redeclare package Medium = Medium,
    minSpeRat=0.05,
    m_flow_nominal=datHP3.sta[datHP3.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan = perFan3.per,
    perCom=datHP3,
    rooLen=5,
    rooWid=5,
    rooHei=5,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut)      "Heat pump for residential building Pilsbury G2"
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
 BaseModels.SingleWSHP                                              HP4(
    redeclare package Medium = Medium,
    minSpeRat=0.05,
    m_flow_nominal=datHP4.sta[datHP4.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan = perFan4.per,
    perCom=datHP4,
    rooLen=5,
    rooWid=5,
    rooHei=5,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut)      "Heat pump for commercial building Themlma by the Sea A1-W"
    annotation (Placement(transformation(extent={{-16,40},{4,60}})));
 BaseModels.SingleWSHP                                              HP5(
    redeclare package Medium = Medium,
    minSpeRat=0.05,
    m_flow_nominal=datHP5.sta[datHP5.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan = perFan5.per,
    perCom=datHP5,
    rooLen=8,
    rooWid=5,
    rooHei=10,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut)     "Heat pump for commercial building Themlma by the Sea A1-E"
    annotation (Placement(transformation(extent={{40,40},{60,60}})));
 BaseModels.SingleWSHP                                              HP6(
    redeclare package Medium = Medium,
    minSpeRat=0.05,
    m_flow_nominal=datHP6.sta[datHP6.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan = perFan6.per,
    perCom=datHP6,
    rooLen=8,
    rooWid=5,
    rooHei=10,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut)     "Heat pump for residential building Themlma by the Sea A2"
    annotation (Placement(transformation(extent={{92,40},{112,60}})));
 BaseModels.SingleWSHP                                              HP7(
    redeclare package Medium = Medium,
    rooLen=10,
    minSpeRat=0.05,
    m_flow_nominal=datHP7.sta[datHP7.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan = perFan7.per,
    perCom=datHP7,
    rooWid=5,
    rooHei=10,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut)     "Heat pump for commercial building Sears D"
    annotation (Placement(transformation(extent={{136,40},{156,60}})));
 BaseModels.SingleWSHP                                              HP8(
    redeclare package Medium = Medium,
    rooLen=10,
    minSpeRat=0.05,
    m_flow_nominal=datHP8.sta[datHP8.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan = perFan8.per,
    perCom=datHP8,
    rooWid=5,
    rooHei=10,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut)     "Heat pump for commercial building Rosedale C1"
    annotation (Placement(transformation(extent={{176,40},{196,60}})));
 BaseModels.SingleWSHP                                              HP9(
    redeclare package Medium = Medium,
    rooLen=10,
    rooWid=5,
    rooHei=10,
    minSpeRat=0.05,
    m_flow_nominal=datHP9.sta[datHP9.nSta].nomVal.m_flow_nominal,
    dpEva_nominal=dpAir_hp_nominal,
    dpCon_nominal=dpWat_hp_nominal,
    perFan = perFan9.per,
    perCom=datHP9,
    alphaRad=alphaRad,
    nOrientations=nOrientations,
    AWin=AWin,
    ATransparent=ATransparent,
    alphaWinIn=alphaWinIn,
    RWin=RWin,
    gWin=gWin,
    ratioWinConRad=ratioWinConRad,
    alphaExtWalIn=alphaExtWalIn,
    nExt=nExt,
    RExt=RExt,
    RExtRem=RExtRem,
    CExt=CExt,
    alphaInt=alphaInt,
    nInt=nInt,
    RInt=RInt,
    CInt=CInt,
    alphaWinOut=alphaWinOut,
    alphaWalOut=alphaWalOut)
    "Heat pump for building Rosedale C2 - not sure what kind of building it is now"
    annotation (Placement(transformation(extent={{218,40},{238,60}})));
  BaseModels.FanSpeedControl           supAirCon2(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{-140,90},{-120,110}})));
  BaseModels.FanSpeedControl           supAirCon3(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{-80,90},{-60,110}})));
  BaseModels.FanSpeedControl           supAirCon4(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{-20,90},{0,110}})));
  BaseModels.FanSpeedControl           supAirCon5(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{40,90},{60,110}})));
  BaseModels.FanSpeedControl           supAirCon6(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{90,90},{110,110}})));
  BaseModels.FanSpeedControl           supAirCon7(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{140,90},{160,110}})));
  BaseModels.FanSpeedControl           supAirCon8(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{180,90},{200,110}})));
  BaseModels.FanSpeedControl           supAirCon9(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    reverseAction=true,
    Ti=60) "Supply air temperature control"
    annotation (Placement(transformation(extent={{220,90},{240,110}})));
  Buildings.Controls.OBC.CDL.Continuous.Product pro[3]
    annotation (Placement(transformation(extent={{-340,20},{-320,40}})));
  Modelica.Blocks.Sources.RealExpression powCom(y=HP1.PCom+HP2.PCom+HP3.PCom+HP4.PCom+HP5.PCom+HP6.PCom+HP7.PCom+HP8.PCom+HP9.PCom)
    "Power consumed by HP compressors" annotation (Placement(transformation(
          extent={{-10,-10},{10,10}}, origin={-270,-70})));
  Modelica.Blocks.Sources.RealExpression powFan(y=HP1.PFan+HP2.PFan+HP3.PFan+HP4.PFan+HP5.PFan+HP6.PFan+HP7.PFan+HP8.PFan+HP9.PFan)
    "Power consumed by HP compressors" annotation (Placement(transformation(
          extent={{-10,-10},{10,10}}, origin={-270,-90})));
  Modelica.Blocks.Sources.RealExpression powPum(y=pum1.P[1]+pum2.P[1]+pum3.P[1]+pum4.P[1]+pum5.P[1]+pum6.P[1]+pum7.P[1]+pum8.P[1]+pum9.P[1])
    "Power consumed by HP associated pumps" annotation (Placement(
        transformation(extent={{-10,-10},{10,10}}, origin={-270,-120})));
  Buildings.Controls.OBC.CDL.Continuous.MultiSum totPow(nin=3) "Total power"
    annotation (Placement(transformation(extent={{-220,-100},{-200,-80}})));
  Modelica.Blocks.Math.Gain powCon(k=-1)
    "Consumed power (negative for cosumption, positive for generation)"
    annotation (Placement(transformation(extent={{-160,-100},{-140,-80}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive loa(mode=
        Buildings.Electrical.Types.Load.VariableZ_P_input)
    annotation (Placement(transformation(extent={{-98,-100},{-118,-80}})));
  replaceable
    Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n           terminal
    constrainedby Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n
    annotation (Placement(transformation(extent={{-10,-150},{10,-130}}),
        iconTransformation(extent={{-10,-108},{10,-88}})));
  Buildings.Fluid.Sensors.MassFlowRate senMasFlo8(redeclare package Medium =
        Medium) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={168,-6})));
  Buildings.Controls.OBC.CDL.Continuous.Product pro1[3]
    annotation (Placement(transformation(extent={{-340,-80},{-320,-60}})));
  Buildings.Controls.OBC.CDL.Conversions.BooleanToReal booToRea1
    annotation (Placement(transformation(extent={{-370,-26},{-350,-6}})));
  Buildings.Controls.SetPoints.OccupancySchedule occSchRes(occupancy=3600*{6,18},
      firstEntryOccupied=false) "Occupancy schedule for residential buildings"
    annotation (Placement(transformation(extent={{-430,-20},{-410,0}})));
  Buildings.Electrical.AC.OnePhase.Sensors.GeneralizedSensor met "Power meter"
    annotation (Placement(transformation(extent={{-40,-130},{-60,-110}})));
  System.Baseclasss.ControlBus infBus "Information bus" annotation (Placement(
        transformation(extent={{-120,-148},{-100,-128}}), iconTransformation(
          extent={{60,-108},{80,-88}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus "Weather data bus"
    annotation (Placement(transformation(extent={{-414,128},{-380,160}}),
    iconTransformation(extent={{-98,70},{-78,90}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=120*2*0.8)
                                                     "Convective heat gain"
    annotation (Placement(transformation(extent={{-442,-96},{-422,-76}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=120*2*0.2)
                                                     "Radiative heat gain"
    annotation (Placement(transformation(extent={{-442,-56},{-422,-36}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{-442,-134},{-422,-114}})));

equation
  connect(boundary.ports[1], hex.port_a2)
    annotation (Line(points={{172,-62},{148,-62},{148,-72}},
                                                          color={0,127,255},
      thickness=0.5));
  connect(bou.ports[1], hex.port_b2)
    annotation (Line(points={{174,-106},{148,-106},{148,-92}},
                                                          color={0,127,255},
      thickness=0.5));
  connect(HP1.port_a, hex.port_b1) annotation (Line(
      points={{-206,50},{-230,50},{-230,-40},{136,-40},{136,-72}},
      color={0,127,255},
      thickness=0.5));
  connect(hex.port_a1, pum1.port_b) annotation (Line(
      points={{136,-92},{136,-112},{-28,-112},{-28,-22},{-172,-22},{-172,10}},
      color={238,46,47},
      thickness=0.5));
  connect(pum1.port_a, HP1.port_b) annotation (Line(
      points={{-172,30},{-172,50},{-186,50}},
      color={238,46,47},
      thickness=0.5));
  connect(exp.port_a, hex.port_a1) annotation (Line(points={{100,-84},{100,-106},
          {136,-106},{136,-92}},
                          color={0,127,255},
      thickness=0.5));
  connect(occSchCom.occupied, booToRea.u) annotation (Line(points={{-409,104},{-392,
          104},{-392,72},{-374,72}}, color={255,0,255}));
  connect(booToRea.y, pum1.u[1]) annotation (Line(points={{-351,72},{-236,72},{-236,
          38},{-176,38},{-176,32}}, color={0,0,127}));
  connect(booToRea.y, HP1.speRatFan) annotation (Line(points={{-351,72},{-228,72},
          {-228,46},{-208,46}},
                              color={0,0,127}));
  connect(rooAirSet.y, supAirCon1.u_s)
    annotation (Line(points={{-223,104},{-214,104},{-214,100},{-206,100}},
                                                     color={0,0,127}));
  connect(HP2.port_b, pum2.port_a) annotation (Line(
      points={{-120,50},{-106,50},{-106,30}},
      color={238,46,47},
      thickness=0.5));
  connect(pum2.port_b, hex.port_a1) annotation (Line(
      points={{-106,10},{-106,-22},{-28,-22},{-28,-112},{136,-112},{136,-92}},
      color={238,46,47},
      thickness=0.5));
  connect(HP3.port_b, pum3.port_a) annotation (Line(
      points={{-60,50},{-50,50},{-50,30}},
      color={238,46,47},
      thickness=0.5));
  connect(pum3.port_b, hex.port_a1) annotation (Line(
      points={{-50,10},{-50,-22},{-28,-22},{-28,-112},{136,-112},{136,-92}},
      color={238,46,47},
      thickness=0.5));
  connect(HP4.port_b, pum4.port_a) annotation (Line(
      points={{4,50},{10,50},{10,28}},
      color={238,46,47},
      thickness=0.5));
  connect(pum4.port_b, hex.port_a1) annotation (Line(
      points={{10,8},{10,-22},{-28,-22},{-28,-112},{136,-112},{136,-92}},
      color={238,46,47},
      thickness=0.5));
  connect(HP5.port_b, pum5.port_a) annotation (Line(
      points={{60,50},{68,50},{68,30}},
      color={238,46,47},
      thickness=0.5));
  connect(pum5.port_b, hex.port_a1) annotation (Line(
      points={{68,10},{68,-22},{-28,-22},{-28,-112},{136,-112},{136,-92}},
      color={238,46,47},
      thickness=0.5));
  connect(hex.port_b1, HP2.port_a) annotation (Line(points={{136,-72},{136,-40},
          {-150,-40},{-150,50},{-140,50}}, color={0,127,255},
      thickness=0.5));
  connect(hex.port_b1, HP3.port_a) annotation (Line(points={{136,-72},{136,-40},
          {-86,-40},{-86,50},{-80,50}}, color={0,127,255},
      thickness=0.5));
  connect(hex.port_b1, HP4.port_a) annotation (Line(points={{136,-72},{136,-40},
          {-24,-40},{-24,50},{-16,50}}, color={0,127,255}));
  connect(hex.port_b1, HP5.port_a) annotation (Line(points={{136,-72},{136,-40},
          {32,-40},{32,50},{40,50}}, color={0,127,255}));
  connect(hex.port_b1, HP6.port_a) annotation (Line(points={{136,-72},{136,-40},
          {84,-40},{84,50},{92,50}}, color={0,127,255}));
  connect(hex.port_b1, HP7.port_a) annotation (Line(points={{136,-72},{136,-40},
          {128,-40},{128,50},{136,50}}, color={0,127,255}));
  connect(hex.port_b1, HP9.port_a) annotation (Line(points={{136,-72},{136,-40},
          {208,-40},{208,50},{218,50}}, color={0,127,255}));
  connect(HP6.port_b, pum6.port_a) annotation (Line(
      points={{112,50},{120,50},{120,28}},
      color={238,46,47},
      thickness=0.5));
  connect(pum6.port_b, hex.port_a1) annotation (Line(
      points={{120,8},{120,-22},{-28,-22},{-28,-112},{136,-112},{136,-92}},
      color={238,46,47},
      thickness=0.5));
  connect(HP7.port_b, pum7.port_a) annotation (Line(
      points={{156,50},{160,50},{160,28}},
      color={238,46,47},
      thickness=0.5));
  connect(pum7.port_b, hex.port_a1) annotation (Line(
      points={{160,8},{160,-22},{-28,-22},{-28,-112},{136,-112},{136,-92}},
      color={238,46,47},
      thickness=0.5));
  connect(HP8.port_b, pum8.port_a) annotation (Line(
      points={{196,50},{200,50},{200,26}},
      color={238,46,47},
      thickness=0.5));
  connect(pum8.port_b, hex.port_a1) annotation (Line(
      points={{200,6},{200,-22},{-28,-22},{-28,-112},{136,-112},{136,-92}},
      color={238,46,47},
      thickness=0.5));
  connect(HP9.port_b, pum9.port_a) annotation (Line(
      points={{238,50},{248,50},{248,26}},
      color={238,46,47},
      thickness=0.5));
  connect(pum9.port_b, hex.port_a1) annotation (Line(
      points={{248,6},{248,-22},{-28,-22},{-28,-112},{136,-112},{136,-92}},
      color={238,46,47},
      thickness=0.5));
  connect(booToRea.y, HP2.speRatFan) annotation (Line(points={{-351,72},{-152,72},
          {-152,46},{-142,46}}, color={0,0,127}));
  connect(booToRea.y, HP4.speRatFan) annotation (Line(points={{-351,72},{-32,72},
          {-32,46},{-18,46}}, color={0,0,127}));
  connect(booToRea.y, pum2.u[1]) annotation (Line(points={{-351,72},{-152,72},{-152,
          38},{-110,38},{-110,32}}, color={0,0,127}));
  connect(booToRea.y, pum4.u[1]) annotation (Line(points={{-351,72},{-32,72},{-32,
          36},{6,36},{6,30}}, color={0,0,127}));
  connect(booToRea.y, HP5.speRatFan) annotation (Line(points={{-351,72},{26,72},
          {26,46},{38,46}}, color={0,0,127}));
  connect(booToRea.y, pum5.u[1]) annotation (Line(points={{-351,72},{26,72},{26,
          38},{64,38},{64,32}}, color={0,0,127}));
  connect(booToRea.y, HP7.speRatFan) annotation (Line(points={{-351,72},{126,72},
          {126,46},{134,46}}, color={0,0,127}));
  connect(booToRea.y, pum7.u[1]) annotation (Line(points={{-351,72},{126,72},{126,
          38},{156,38},{156,30}}, color={0,0,127}));
  connect(booToRea.y, HP8.speRatFan) annotation (Line(points={{-351,72},{166,72},
          {166,46},{174,46}}, color={0,0,127}));
  connect(booToRea.y, pum8.u[1]) annotation (Line(points={{-351,72},{166,72},{166,
          38},{196,38},{196,28}}, color={0,0,127}));
  connect(booToRea.y, HP9.speRatFan) annotation (Line(points={{-351,72},{206,72},
          {206,46},{216,46}}, color={0,0,127}));
  connect(booToRea.y, pum9.u[1]) annotation (Line(points={{-351,72},{206,72},{206,
          38},{244,38},{244,28}}, color={0,0,127}));
  connect(rooAirSet.y, supAirCon2.u_s) annotation (Line(points={{-223,104},{-214,
          104},{-214,122},{-152,122},{-152,100},{-142,100}}, color={0,0,127}));
  connect(rooAirSet.y, supAirCon3.u_s) annotation (Line(points={{-223,104},{-214,
          104},{-214,122},{-90,122},{-90,100},{-82,100}}, color={0,0,127}));
  connect(rooAirSet.y, supAirCon4.u_s) annotation (Line(points={{-223,104},{-214,
          104},{-214,122},{-40,122},{-40,100},{-22,100}}, color={0,0,127}));
  connect(rooAirSet.y, supAirCon5.u_s) annotation (Line(points={{-223,104},{-214,
          104},{-214,122},{26,122},{26,100},{38,100}}, color={0,0,127}));
  connect(rooAirSet.y, supAirCon6.u_s) annotation (Line(points={{-223,104},{-214,
          104},{-214,122},{80,122},{80,100},{88,100}}, color={0,0,127}));
  connect(rooAirSet.y, supAirCon7.u_s) annotation (Line(points={{-223,104},{-214,
          104},{-214,122},{128,122},{128,100},{138,100}}, color={0,0,127}));
  connect(rooAirSet.y, supAirCon8.u_s) annotation (Line(points={{-223,104},{-214,
          104},{-214,122},{172,122},{172,100},{178,100}}, color={0,0,127}));
  connect(rooAirSet.y, supAirCon9.u_s) annotation (Line(points={{-223,104},{-214,
          104},{-214,122},{212,122},{212,100},{218,100}}, color={0,0,127}));
          for i in 1:3 loop
  connect(booToRea.y, pro[i].u1) annotation (Line(points={{-351,72},{-256,72},{-256,
          52},{-360,52},{-360,36},{-342,36}}, color={0,0,127}));
  connect(booToRea1.y, pro1[i].u1) annotation (Line(points={{-349,-16},{-320,-16},{
          -320,-40},{-362,-40},{-362,-64},{-342,-64}}, color={0,0,127}));

          end for;
 connect(powCom.y, totPow.u[1]) annotation (Line(points={{-259,-70},{-240,-70},
          {-240,-85.3333},{-222,-85.3333}}, color={0,0,127}));
  connect(powFan.y, totPow.u[2])
    annotation (Line(points={{-259,-90},{-222,-90}}, color={0,0,127}));
  connect(powPum.y, totPow.u[3]) annotation (Line(points={{-259,-120},{-240,
          -120},{-240,-94.6667},{-222,-94.6667}}, color={0,0,127}));
  connect(totPow.y, powCon.u)
    annotation (Line(points={{-198.3,-90},{-162,-90}}, color={0,0,127}));
  connect(loa.Pow, powCon.y)
    annotation (Line(points={{-118,-90},{-139,-90}}, color={0,0,127}));
  connect(hex.port_b1, senMasFlo8.port_a) annotation (Line(points={{136,-72},{
          136,-40},{168,-40},{168,-16}}, color={0,127,255}));
  connect(senMasFlo8.port_b, HP8.port_a)
    annotation (Line(points={{168,4},{168,50},{176,50}}, color={0,127,255}));
  connect(occSchCom.occupied, supAirCon1.on) annotation (Line(points={{-409,104},
          {-396,104},{-396,126},{-212,126},{-212,107},{-206,107}}, color={255,0,
          255}));
  connect(occSchCom.occupied, supAirCon2.on) annotation (Line(points={{-409,104},
          {-398,104},{-398,126},{-150,126},{-150,107},{-142,107}}, color={255,0,
          255}));
  connect(occSchCom.occupied, supAirCon4.on) annotation (Line(points={{-409,104},
          {-396,104},{-396,126},{-38,126},{-38,107},{-22,107}}, color={255,0,255}));
  connect(occSchCom.occupied, supAirCon5.on) annotation (Line(points={{-409,104},
          {-400,104},{-400,126},{28,126},{28,107},{38,107}}, color={255,0,255}));
  connect(occSchCom.occupied, supAirCon8.on) annotation (Line(points={{-409,104},
          {-398,104},{-398,126},{174,126},{174,107},{178,107}}, color={255,0,255}));
  connect(occSchCom.occupied, supAirCon7.on) annotation (Line(points={{-409,104},
          {-398,104},{-398,126},{130,126},{130,107},{138,107}}, color={255,0,255}));
  connect(occSchCom.occupied, supAirCon9.on) annotation (Line(points={{-409,104},
          {-398,104},{-398,126},{214,126},{214,108},{218,108},{218,107}}, color=
         {255,0,255}));
  connect(occSchRes.occupied, booToRea1.u) annotation (Line(points={{-409,-16},{
          -372,-16}},                       color={255,0,255}));
  connect(supAirCon1.spe, HP1.speRatCom) annotation (Line(points={{-183,100},{-172,
          100},{-172,76},{-222,76},{-222,52},{-208,52}}, color={0,0,127}));
  connect(supAirCon2.spe, HP2.speRatCom) annotation (Line(points={{-119,100},{-106,
          100},{-106,76},{-150,76},{-150,52},{-142,52}}, color={0,0,127}));
  connect(supAirCon4.spe, HP4.speRatCom) annotation (Line(points={{1,100},{14,100},
          {14,76},{-30,76},{-30,52},{-18,52}}, color={0,0,127}));
  connect(supAirCon5.spe, HP5.speRatCom) annotation (Line(points={{61,100},{68,100},
          {68,76},{28,76},{28,52},{38,52}}, color={0,0,127}));
  connect(supAirCon6.spe, HP6.speRatCom) annotation (Line(points={{111,100},{122,
          100},{122,78},{82,78},{82,52},{90,52}}, color={0,0,127}));
  connect(supAirCon7.spe, HP7.speRatCom) annotation (Line(points={{161,100},{164,
          100},{164,78},{128,78},{128,52},{134,52}}, color={0,0,127}));
  connect(supAirCon8.spe, HP8.speRatCom) annotation (Line(points={{201,100},{204,
          100},{204,76},{168,76},{168,52},{174,52}}, color={0,0,127}));
  connect(supAirCon9.spe, HP9.speRatCom) annotation (Line(points={{241,100},{248,
          100},{248,76},{208,76},{208,52},{216,52}}, color={0,0,127}));
  connect(supAirCon3.spe, HP3.speRatCom) annotation (Line(points={{-59,100},{-50,
          100},{-50,76},{-88,76},{-88,52},{-82,52}}, color={0,0,127}));
  connect(occSchRes.occupied, supAirCon3.on) annotation (Line(points={{-409,-16},
          {-396,-16},{-396,126},{-88,126},{-88,107},{-82,107}}, color={255,0,255}));
  connect(occSchRes.occupied, supAirCon6.on) annotation (Line(points={{-409,-16},
          {-396,-16},{-396,126},{82,126},{82,107},{88,107}}, color={255,0,255}));
  connect(booToRea1.y, pum3.u[1]) annotation (Line(points={{-349,-16},{-242,-16},
          {-242,72},{-92,72},{-92,38},{-54,38},{-54,32}}, color={0,0,127}));
  connect(booToRea1.y, pum6.u[1]) annotation (Line(points={{-349,-16},{-242,-16},
          {-242,72},{78,72},{78,40},{116,40},{116,30}}, color={0,0,127}));
  connect(booToRea1.y, HP3.speRatFan) annotation (Line(points={{-349,-16},{-242,
          -16},{-242,72},{-90,72},{-90,46},{-82,46}}, color={0,0,127}));
  connect(booToRea1.y, HP6.speRatFan) annotation (Line(points={{-349,-16},{-240,
          -16},{-240,72},{80,72},{80,46},{90,46}}, color={0,0,127}));
  connect(loa.terminal, met.terminal_p) annotation (Line(points={{-98,-90},{-76,
          -90},{-76,-120},{-60,-120}}, color={0,120,120}));
  connect(met.terminal_n, terminal) annotation (Line(points={{-40,-120},{0,-120},
          {0,-140}}, color={0,120,120}));
  connect(met.S, infBus.heaPumAppPow) annotation (Line(points={{-44,-129},{-44,
          -138},{-110,-138}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus, HP1.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{-204.8,80},{-204.8,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HP2.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{-138.8,80},{-138.8,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HP3.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{-78.8,80},{-78.8,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HP4.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{-14.8,80},{-14.8,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HP5.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{41.2,80},{41.2,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HP6.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{93.2,80},{93.2,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HP7.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{137.2,80},{137.2,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HP8.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{177.2,80},{177.2,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, HP9.weaBus) annotation (Line(
      points={{-397,144},{-254,144},{-254,80},{219.2,80},{219.2,58}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(HP1.TRooAir, supAirCon1.u_m) annotation (Line(points={{-185,57},{-174,
          57},{-174,84},{-214,84},{-214,93},{-206,93}}, color={0,0,127}));
  connect(HP2.TRooAir, supAirCon2.u_m) annotation (Line(points={{-119,57},{-114,
          57},{-114,86},{-152,86},{-152,93},{-142,93}}, color={0,0,127}));
  connect(HP3.TRooAir, supAirCon3.u_m) annotation (Line(points={{-59,57},{-52,57},
          {-52,86},{-90,86},{-90,93},{-82,93}}, color={0,0,127}));
  connect(HP4.TRooAir, supAirCon4.u_m) annotation (Line(points={{5,57},{12,57},{
          12,86},{-40,86},{-40,93},{-22,93}}, color={0,0,127}));
  connect(HP5.TRooAir, supAirCon5.u_m) annotation (Line(points={{61,57},{66,57},
          {66,86},{26,86},{26,93},{38,93}}, color={0,0,127}));
  connect(HP6.TRooAir, supAirCon6.u_m) annotation (Line(points={{113,57},{118,57},
          {118,86},{80,86},{80,93},{88,93}}, color={0,0,127}));
  connect(HP7.TRooAir, supAirCon7.u_m) annotation (Line(points={{157,57},{160,57},
          {160,86},{128,86},{128,93},{138,93}}, color={0,0,127}));
  connect(HP8.TRooAir, supAirCon8.u_m) annotation (Line(points={{197,57},{202,57},
          {202,86},{172,86},{172,93},{178,93}}, color={0,0,127}));
  connect(HP9.TRooAir, supAirCon9.u_m) annotation (Line(points={{239,57},{246,57},
          {246,84},{212,84},{212,93},{218,93}}, color={0,0,127}));
  connect(qRadGai_flow.y, pro1[1].u2) annotation (Line(points={{-421,-46},{-368,
          -46},{-368,-76},{-342,-76}}, color={0,0,127}));
  connect(qConGai_flow.y, pro1[2].u2) annotation (Line(points={{-421,-86},{-368,
          -86},{-368,-76},{-342,-76}}, color={0,0,127}));
  connect(qLatGai_flow.y, pro1[3].u2) annotation (Line(points={{-421,-124},{-368,
          -124},{-368,-76},{-342,-76}}, color={0,0,127}));
  connect(qConGai_flow.y, pro[2].u2) annotation (Line(points={{-421,-86},{-392,-86},
          {-392,24},{-342,24}}, color={0,0,127}));
  connect(qLatGai_flow.y, pro[3].u2) annotation (Line(points={{-421,-124},{-390,
          -124},{-390,24},{-342,24}}, color={0,0,127}));
  connect(qRadGai_flow.y, pro[1].u2) annotation (Line(points={{-421,-46},{-394,-46},
          {-394,24},{-342,24}}, color={0,0,127}));
  connect(pro.y, HP1.qGai_flow) annotation (Line(points={{-319,30},{-244,30},{-244,
          72},{-216,72},{-216,58},{-208,58}}, color={0,0,127}));
  connect(pro.y, HP2.qGai_flow) annotation (Line(points={{-319,30},{-244,30},{-244,
          72},{-148,72},{-148,58},{-142,58}}, color={0,0,127}));
  connect(pro.y, HP4.qGai_flow) annotation (Line(points={{-319,30},{-244,30},{-244,
          72},{-26,72},{-26,58},{-18,58}}, color={0,0,127}));
  connect(pro.y, HP5.qGai_flow) annotation (Line(points={{-319,30},{-246,30},{-246,
          72},{32,72},{32,58},{38,58}}, color={0,0,127}));
  connect(pro.y, HP7.qGai_flow) annotation (Line(points={{-319,30},{-244,30},{-244,
          72},{130,72},{130,58},{134,58}}, color={0,0,127}));
  connect(pro.y, HP8.qGai_flow) annotation (Line(points={{-319,30},{-246,30},{-246,
          72},{170,72},{170,58},{174,58}}, color={0,0,127}));
  connect(pro.y, HP9.qGai_flow) annotation (Line(points={{-319,30},{-242,30},{-242,
          72},{210,72},{210,58},{216,58}}, color={0,0,127}));
  connect(pro1.y, HP3.qGai_flow) annotation (Line(points={{-319,-70},{-298,-70},
          {-298,72},{-82,72},{-82,58}}, color={0,0,127}));
  connect(pro1.y, HP6.qGai_flow) annotation (Line(points={{-319,-70},{-298,-70},
          {-298,72},{86,72},{86,58},{90,58}}, color={0,0,127}));
  annotation (Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-440,-140},{260,160}})),
    __Dymola_Commands,
    Icon(graphics={Text(
          extent={{-180,174},{168,128}},
          lineColor={0,0,255},
          textString="%name"), Bitmap(extent={{-98,-102},{102,100}},
          fileName="modelica://GreenVillage/Resources/Icons/WSHP.png")}));
end HPLoop;
