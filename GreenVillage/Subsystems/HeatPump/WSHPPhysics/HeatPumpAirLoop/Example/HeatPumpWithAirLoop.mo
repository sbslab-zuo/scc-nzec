within GreenVillage.Subsystems.HeatPump.WSHPPhysics.HeatPumpAirLoop.Example;
model HeatPumpWithAirLoop
  import GreenVillage;
   extends Modelica.Icons.Example;
   package Medium_Air = Buildings.Media.Air;
   package Medium_Water = Buildings.Media.Water;

   parameter String Load1=Modelica.Utilities.Files.loadResource("modelica://GreenVillage/Resources/WSHP/Inputs/Cooling load for each pump/CoolingLoad_HP1.txt");

   // parameters for airside

   parameter Real P[:]={0,50^2,100^2} "Supply fan power array";
   parameter Real v_flow[:]={0,mOut_flow_nominal/1.2,mOut_flow_nominal*2/1.2}
    "Volume flow rate curve";
   parameter Real p_flow[:]={600,300,0} "Pressure curve";
   parameter Real ti_SF=60 "ti for supply fan controller";
   parameter Real k_SF=1 "k for supply fan controller";
   parameter Real S_min_SF=0 "The minimum speed for supply fan";
   parameter Modelica.SIunits.MassFlowRate mOut_flow_nominal=m2_flow_nominal*4.2*7/20
    "Mass flow rate of the outside air damper";
   parameter Modelica.SIunits.MassFlowRate mRec_flow_nominal=m2_flow_nominal*4.2*7/20
    "Mass flow rate of the recirculation air damper";
   parameter Modelica.SIunits.MassFlowRate mExh_flow_nominal=m2_flow_nominal*4.2*7/20
    "Mass flow rate of the exhaust air damper";
   parameter Modelica.SIunits.MassFlowRate mAir_CC_flow_nominal=m2_flow_nominal*4.2*7/20
    "The nominal mass flow rate of the cooling air";
   parameter Modelica.SIunits.MassFlowRate mWater_CC_flow_nominal=m2_flow_nominal
    "The nominal mass flow rate of the cooling water";
   parameter Modelica.SIunits.Pressure dpWater_CC_nominal=1000
    "The pressure drop of the cooling water";
   parameter Modelica.SIunits.Pressure dpOut_nominal=50
   "Pressure drop of the recirculation air damper";
   parameter Modelica.SIunits.Pressure dpRec_nominal=50
   "Pressure drop of the recirculation air damper (fully open)";
   parameter Modelica.SIunits.Pressure dpExh_nominal=50
   "Pressure drop of the exhaust air damper";
   parameter Modelica.SIunits.Pressure dpAir_CC_nominal=50
   "The pressure drop of the cooling air";
   parameter Modelica.SIunits.Temperature TAir_CC_a_nominal=298.15
    "The nominal temperature of the air entering the cooling water coil";
   parameter Modelica.SIunits.Temperature TAir_CC_b_nominal=288.15
    "The nominal temperature of the air leaving the cooling water coil";
   parameter Modelica.SIunits.Temperature TWater_CC_a_nominal=278.15
    "The nominal temperature of the water entering the cooling water coil";
   parameter Modelica.SIunits.Temperature TWater_CC_b_nominal=285.15
    "The nominal temperature of the water leaving the cooling water coil";
   parameter Real k_coil=1 "Gain of coil temperature PI controllers in air side";
   parameter Real ti_coil=60 "Integral time of coil temperature PI controller in air side";

   // Parameters for heat pump

  parameter Modelica.SIunits.Power P_nominal=5000
    "Nominal compressor power (at y=1)";
  parameter Modelica.SIunits.TemperatureDifference dTEva_nominal
    "Temperature difference evaporator inlet-outlet";
  parameter Modelica.SIunits.TemperatureDifference dTCon_nominal=5
    "Temperature difference condenser outlet-inlet";
  parameter Real COPc_nominal=3 "Chiller COP";

  parameter Modelica.SIunits.MassFlowRate m2_flow_nominal=
     P_nominal*COPc_nominal/dTEva_nominal/4200
    "Nominal mass flow rate at chilled water side";
  parameter Modelica.SIunits.MassFlowRate m1_flow_nominal=
    m2_flow_nominal*(COPc_nominal+1)/COPc_nominal
    "Nominal mass flow rate at condenser water wide";
  parameter Modelica.SIunits.Temperature TCon_nominal= 302.59 "Condenser temperature";
  parameter Modelica.SIunits.Temperature TEva_nominal=278.15 "Evaporator temperature";
  parameter Real a[:] = {1}
    "Coefficients for efficiency curve (need p(a=a, y=1)=1)";
  parameter Modelica.SIunits.Pressure dp_nominal=1000 "Nominal pressure difference";
  parameter Real k_hp=1 "Gain of PI controllers in heat pump";
  parameter Real ti_hp=60 "Integral time of PI controller in heat pump";

  // parameters for pump 1 and 2
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal_pump1=m1_flow_nominal;
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal_pump2=m2_flow_nominal;
  parameter Modelica.SIunits.Pressure dp_nominal_pump1=1000;
  parameter Modelica.SIunits.Pressure dp_nominal_pump2=dpWater_CC_nominal;

  GreenVillage.Subsystems.HeatPump.WSHPPhysics.HeatPumpAirLoop.HeatPumpWithAirLoop
    HPAirLoop(
    redeclare package Medium_Air = Medium_Air,
    redeclare package Medium_Water = Medium_Water,
    P=P,
    v_flow=v_flow,
    p_flow=p_flow,
    ti_SF=ti_SF,
    k_SF=k_SF,
    S_min_SF=S_min_SF,
    mOut_flow_nominal=mOut_flow_nominal,
    mRec_flow_nominal=mRec_flow_nominal,
    mExh_flow_nominal=mExh_flow_nominal,
    mAir_CC_flow_nominal=mAir_CC_flow_nominal,
    mWater_CC_flow_nominal=mWater_CC_flow_nominal,
    dpWater_CC_nominal=dpWater_CC_nominal,
    dpOut_nominal=dpOut_nominal,
    dpRec_nominal=dpRec_nominal,
    dpExh_nominal=dpExh_nominal,
    dpAir_CC_nominal=dpAir_CC_nominal,
    TAir_CC_a_nominal=TAir_CC_a_nominal,
    TAir_CC_b_nominal=TAir_CC_a_nominal,
    TWater_CC_a_nominal=TWater_CC_a_nominal,
    TWater_CC_b_nominal=TWater_CC_b_nominal,
    k_coil=k_coil,
    ti_coil=ti_coil,
    dTEva_nominal=dTEva_nominal,
    dTCon_nominal=dTCon_nominal,
    COPc_nominal=COPc_nominal,
    m2_flow_nominal=m2_flow_nominal,
    m1_flow_nominal=m1_flow_nominal,
    TCon_nominal=TCon_nominal,
    TEva_nominal=TEva_nominal,
    a=a,
    dp_nominal=dp_nominal,
    k_hp=k_hp,
    ti_hp=ti_hp,
    m_flow_nominal_pump1=m_flow_nominal_pump1,
    m_flow_nominal_pump2=m_flow_nominal_pump2,
    dp_nominal_pump1=dp_nominal_pump1,
    dp_nominal_pump2=dp_nominal_pump2,
    P_nominal=P_nominal)
    annotation (Placement(transformation(extent={{6,-16},{44,22}})));

  Modelica.Blocks.Sources.Constant CoolOn(k=1)
    annotation (Placement(transformation(extent={{-80,-60},{-60,-40}})));
  Modelica.Blocks.Sources.Constant TsetPoi_Air(k=273.15 + 15)
    annotation (Placement(transformation(extent={{-80,-24},{-60,-4}})));
  Modelica.Blocks.Sources.Constant TOut(k=273.15 + 25)
    annotation (Placement(transformation(extent={{-80,40},{-60,60}})));
  Modelica.Blocks.Sources.Constant TsetPoi(k=273.15 + 5) annotation (Placement(transformation(extent={{-80,-90},
            {-60,-70}})));
  Modelica.Blocks.Sources.Constant On(k=1)
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  Buildings.Fluid.Sources.FixedBoundary bou(nPorts=1, redeclare package Medium
      =                Medium_Water)
    annotation (Placement(transformation(extent={{62,-80},{42,-60}})));
  Buildings.Fluid.Sources.FixedBoundary bou1(nPorts=1, redeclare package Medium
      =                Medium_Water,
    T=302.59)
    annotation (Placement(transformation(extent={{-12,-80},{8,-60}})));
  Modelica.Blocks.Sources.CombiTimeTable Load_HP1(table=[3600,0; 7200,0; 10800,
        0; 14400,0; 18000,0; 21600,0; 25200,0; 28800,1.7585; 32400,3.517; 36000,
        8.7925; 39600,8.7925; 43200,12.3095; 46800,12.3095; 50400,12.3095;
        54000,12.3095; 57600,14.068; 61200,12.3095; 64800,8.7925; 68400,8.7925;
        72000,5.2755; 75600,5.2755; 79200,0; 82800,0; 86400,0],
    extrapolation=Modelica.Blocks.Types.Extrapolation.HoldLastPoint,
    smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    tableName="table1",
    fileName=Load1,
    tableOnFile=true)
    annotation (Placement(transformation(extent={{-80,6},{-60,26}})));
equation
  connect(On.y, HPAirLoop.On) annotation (Line(points={{-59,80},{-20,80},{-20,
          20.4167},{4.64286,20.4167}},
                              color={0,0,127},
      pattern=LinePattern.Dash));
  connect(TsetPoi_Air.y, HPAirLoop.SetPoi) annotation (Line(points={{-59,-14},{
          -48,-14},{-40,-14},{-40,1.41667},{4.64286,1.41667}},
                                                           color={0,0,127},
      pattern=LinePattern.Dash));
  connect(TsetPoi.y, HPAirLoop.TSetPoi) annotation (Line(points={{-59,-80},{-20,
          -80},{-20,-11.25},{4.64286,-11.25}}, color={0,0,127},
      pattern=LinePattern.Dash));
  connect(CoolOn.y, HPAirLoop.CoolOn) annotation (Line(points={{-59,-50},{-32,
          -50},{-32,-4.91667},{4.64286,-4.91667}},
                                              color={0,0,127},
      pattern=LinePattern.Dash));
  connect(HPAirLoop.port_b, bou.ports[1]) annotation (Line(points={{30.4286,-16},
          {30,-16},{30,-60},{30,-70},{42,-70}}, color={0,127,255}));
  connect(HPAirLoop.port_a, bou1.ports[1]) annotation (Line(points={{19.5714,
          -16},{18,-16},{18,-70},{8,-70}}, color={0,127,255}));
  connect(TOut.y, HPAirLoop.T_in) annotation (Line(points={{-59,50},{-42,50},{
          -42,34},{-20,34},{-20,14.0833},{4.64286,14.0833}},
                                                         color={0,0,127},
      pattern=LinePattern.Dash));
  connect(Load_HP1.y[1], HPAirLoop.Q) annotation (Line(
      points={{-59,16},{-40,16},{-40,7.75},{4.64286,7.75}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file="Resources/Scripts/HeatPumpWithAirLoop.mos"));
end HeatPumpWithAirLoop;
