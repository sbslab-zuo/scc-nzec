within GreenVillage.Subsystems.HeatPump.Example;
model Comparsion
  extends Modelica.Icons.Example
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));

  DataDrivenModel.WSHP_ANN hpANN "Heat pump ANN models"
    annotation (Placement(transformation(extent={{20,-40},{40,-20}})));
  Grid.Grid gri
    annotation (Placement(transformation(extent={{-20,20},{20,60}})));
  WSHPPhysics.HPLoop hpPhy "Heat pump physical models"
    annotation (Placement(transformation(extent={{-40,-20},{-20,-40}})));
equation
  connect(gri.terminal, hpANN.terminal) annotation (Line(points={{0,20.4},{0,
          -10},{30,-10},{30,-20.1111}}, color={0,120,120}));
  connect(hpPhy.terminal, gri.terminal) annotation (Line(points={{-30,-20.2},{
          -30,-10},{0,-10},{0,20.4}}, color={0,120,120}));
end Comparsion;
