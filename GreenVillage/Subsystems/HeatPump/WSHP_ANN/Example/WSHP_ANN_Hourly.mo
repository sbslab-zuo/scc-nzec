within GreenVillage.Subsystems.HeatPump.WSHP_ANN.Example;
model WSHP_ANN_Hourly
  import GreenVillage;
  GreenVillage.Subsystems.HeatPump.WSHP_ANN.WSHP_ANN WSHPANN
    annotation (Placement(transformation(extent={{-40,0},{-20,20}})));
  GreenVillage.Subsystems.Grid.Grid grid
    annotation (Placement(transformation(extent={{-40,40},{-20,60}})));
equation
  connect(grid.terminal, WSHPANN.terminal)
    annotation (Line(points={{-30,40.2},{-30,19.8889}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Ellipse(lineColor = {75,138,73},
                fillColor={255,255,255},
                fillPattern = FillPattern.Solid,
                extent={{-100,-100},{100,100}}),
        Polygon(lineColor = {0,0,255},
                fillColor = {75,138,73},
                pattern = LinePattern.None,
                fillPattern = FillPattern.Solid,
                points={{-38,54},{60,-2},{-38,-52},{-38,54}})}), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file="Resources/Scripts/WSHP_hourly_ANN.mos"
        "Simulate and plot"));
end WSHP_ANN_Hourly;
