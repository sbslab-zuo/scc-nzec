within GreenVillage.Subsystems;
package HeatPump "This package contains heat pump systems in the NZEC"



annotation (Documentation(info="<html>
<p>Ground heat pump system is composed of 9 heat pumps. The heat pump absorps heat from the buildings and dumps heat to the ground well. 
The schematic drawings are shown in the following figure. 
</p>
<p><img src=\"modelica://GreenVillage/Resources/Images/Subsystems/HeatPump/HeatPumpSystem.PNG\"/></p>
</html>"));
end HeatPump;
