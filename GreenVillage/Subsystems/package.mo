within GreenVillage;
package Subsystems "This package contains HGV's subsystems"
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>There are 5 subsystems included in this package:</p>
<ul>
<li>Renewable energy subsystem</li>
<li>Grid subsystem</li>
<li>Domestic hot water subsystem</li>
<li>Water source heat pump subsystem</li>
<li>Building load subsystem</li>
</ul>
</html>", revisions="<html>
<ul>
<li>
January 2, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"));
end Subsystems;
