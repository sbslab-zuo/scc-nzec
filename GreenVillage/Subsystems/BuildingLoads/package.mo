within GreenVillage.Subsystems;
package BuildingLoads "This package contains load appliances in the building side"

annotation (Documentation(revisions="<html>
<ul>
<li>
March 6, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>", info="<html>
<p>This package is about building load subsystem. Models about the buildings&apos; power demands are included. The data which is used to obtain the power demand pattern is during 2017.</p>
</html>"));
end BuildingLoads;
