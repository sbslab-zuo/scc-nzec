within GreenVillage.Subsystems.BuildingLoads.SingleBuilding;
model SingleChannel "Single channel model"
  parameter Buildings.Utilities.Time.Types.ZeroTime zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017
    "Enumeration for choosing how reference time (time = 0) should be defined";
  parameter Integer yearRef=2017
    "Year when time = 0, used if zerTim=Custom";
  parameter String fileName=ModelicaServices.ExternalReferences.loadResource(
    "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/BuildingF/BuilidngF1/CH22/CH22.txt")
    "File where matrix is stored";
  parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments
    "Smoothness of table interpolation";
  parameter Modelica.SIunits.Frequency f_cut
    "Cut-off frequency";
  Buildings.Utilities.Time.CalendarTime calTim(zerTim=zerTim, yearRef=yearRef)
    "Date and time"
    annotation (Placement(transformation(extent={{-10,30},{10,50}})));
  Modelica.Blocks.Sources.CombiTimeTable powDem(
    final tableOnFile=true,
    final tableName="table",
    fileName=fileName,
    smoothness=smoothness,
    final extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    final columns=2:15)    "Power demand models"
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  Modelica.Blocks.Sources.RealExpression powPat(y=if calTim.month < 4 then
        if calTim.weekDay==1 then powDem.y[1] elseif calTim.weekDay == 2
          then powDem.y[2] elseif calTim.weekDay
         ==3 then powDem.y[3] elseif calTim.weekDay == 4 then powDem.y[4]
        elseif calTim.weekDay == 5 then  powDem.y[5] elseif calTim.weekDay == 6
           then  powDem.y[6] else  powDem.y[7] else
         if calTim.weekDay==1 then powDem.y[8] elseif calTim.weekDay == 2
           then powDem.y[9] elseif calTim.weekDay
         ==3 then powDem.y[10] elseif calTim.weekDay == 4 then powDem.y[11]
         elseif calTim.weekDay == 5 then  powDem.y[12] elseif calTim.weekDay == 6
           then  powDem.y[13] else  powDem.y[14])
    "Conditions to determine consumption patterns"
    annotation (Placement(transformation(extent={{-10,-30},{10,-10}})));
  Modelica.Blocks.Interfaces.RealOutput pow "Power demand"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Continuous.Filter fil(f_cut=f_cut) "Filter"
    annotation (Placement(transformation(extent={{30,-30},{50,-10}})));

equation
  connect(powPat.y, fil.u)
    annotation (Line(points={{11,-20},{28,-20}}, color={0,0,127}));
  connect(fil.y, pow) annotation (Line(points={{51,-20},{60,-20},{60,0},{110,0}},
        color={0,0,127}));
 annotation (Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(revisions="<html>
<ul>
<li>
February 27, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>", info="<html>
<p>This model is used to choose the right consumption patterns according to the simulation time.</p>
</html>"),
    Icon(graphics={Bitmap(extent={{-100,-100},{100,100}},
    fileName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/SingleBuilding/singleChannel.png")}));
end SingleChannel;
