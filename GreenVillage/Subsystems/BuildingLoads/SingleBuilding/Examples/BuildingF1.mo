within GreenVillage.Subsystems.BuildingLoads.SingleBuilding.Examples;
model BuildingF1 "Power demand of Building F1"
  extends Modelica.Icons.Example;
  GreenVillage.Subsystems.BuildingLoads.SingleBuilding.VectorizedChannel buiF1(
    zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017,
    yearRef=2017,
    num=20,
    f_cut=1/120,
    fileName={
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH4-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH9-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH10.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH13.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH14.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH15.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH16.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH17.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH18.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH19.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH20.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH21.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH22.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH23.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_CH24.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1/F1_Unmo.txt")})
            "Power demand of Building F1 (Bakery)"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Buildings.Electrical.AC.OnePhase.Loads.Inductive loa(mode=Buildings.Electrical.Types.Load.VariableZ_P_input)
    "Power demand"
    annotation (Placement(transformation(extent={{40,-10},{20,10}})));
  GreenVillage.Subsystems.Grid.Grid gri "Electrical grid"
    annotation (Placement(transformation(extent={{60,-8},{80,12}})));
equation
  connect(loa.terminal, gri.terminal)
    annotation (Line(points={{40,0},{50,0},{50,-20},{70,-20},{70,-7.8}}, color={0,120,120}));
  connect(buiF1.pow, loa.Pow)
    annotation (Line(points={{11,0},{20,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(revisions="<html>
<ul>
<li>
April 9, 2018 by Yangyang Fu:<br/>
Add scripts.
</li>
<li>
February 27, 2018 by Danlin Hou:<br/>
First implementation.
</li>
</ul>
</html>", info="<html>
<p>This model illustrates how to use the model of <a href=\"modelica://GreenVillage.Subsystems.BuildingLoads.SingleBuilding.VectorizedChannel\">GreenVillage.Subsystems.BuildingLoads.SingleBuilding.VectorizedChannel</a>. The output of this model is the total power demand of all channels in Building F1 according to the simulation time. In additon, each channel&apos;s power demand is calculated.</p>
</html>"),
    experiment(StopTime=86400),
    __Dymola_Commands(file=
          "Modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1.mos"
        "Simulate and Plot"));
end BuildingF1;
