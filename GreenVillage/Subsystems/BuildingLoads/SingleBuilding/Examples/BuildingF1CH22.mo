within GreenVillage.Subsystems.BuildingLoads.SingleBuilding.Examples;
model BuildingF1CH22 "Power demand of CH22 in Building F1"
  extends Modelica.Icons.Example;
  GreenVillage.Subsystems.BuildingLoads.SingleBuilding.SingleChannel CH22(
    zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017,
    yearRef=2017,
    f_cut=1/120,
    smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1CH22/F1_CH22.txt"))
    "Channel 22 of Building F1"
    annotation (Placement(transformation(extent={{-20,-10},{0,10}})));
  GreenVillage.Subsystems.Grid.Grid gri "Electrical grid"
    annotation (Placement(transformation(extent={{60,-8},{80,12}})));
  Buildings.Electrical.AC.OnePhase.Loads.Inductive loa(mode=Buildings.Electrical.Types.Load.VariableZ_P_input)
    "Power demand"
    annotation (Placement(transformation(extent={{40,-10},{20,10}})));
equation
  connect(CH22.pow, loa.Pow)
    annotation (Line(points={{1,0},{20,0}}, color={0,0,127}));
  connect(loa.terminal, gri.terminal)
    annotation (Line(points={{40,0},{50,0},{50,-20},{70,-20},{70,-7.8}}, color={0,120,120}));
  annotation (Documentation(revisions="<html>
<ul>
<li>
April 9, 2018 by Yangyang Fu:<br/>
Add scripts
</li>
<li>
February 27, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>",info="<html>
<p>This model illustrates how to to use the model of 
<a href=\"modelica://GreenVillage.Subsystems.BuildingLoads.SingleBuilding.SingleChannel\">
GreenVillage.Subsystems.BuildingLoads.SingleBuilding.SingleChannel</a>. 
The output of this model is the power demand of the Channel 22 of Building F1 according to the simulation time. </p>
</html>"),
    __Dymola_Commands(file="modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/BuildingLoads/SingleBuilding/Examples/BuildingF1CH22.mos"
        "Simulate and Plot"),
    experiment(StopTime=86400));
end BuildingF1CH22;
