within GreenVillage.Subsystems.BuildingLoads.SingleBuilding;
package Examples "Test models"
  extends Modelica.Icons.ExamplesPackage;


  annotation (Documentation(info="<html>
<p>This package contains examples for the use of models that can be found in <a href=\"modelica://GreenVillage.Subsystems.BuildingLoads.SingleBuilding\">GreenVillage.Subsystems.BuildingLoads.SingleBuilding</a>.</p>
</html>", revisions="<html>
<ul>
<li>
February 27, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"));
end Examples;
