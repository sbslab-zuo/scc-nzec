within GreenVillage.Subsystems.BuildingLoads;
package SingleBuilding "Consumption of single building"
annotation (Documentation(info="<html>
<p>This package contains two models: </p>
<ul>
<li>One is used for the single channel&apos;s power demand</li>
<li>The other is used for the multi channels which is based on the SingleChannel model</li>
</ul>
<p>The outputs of these two models are power demand.</p>
</html>", revisions="<html>
<ul>
<li>
February 27, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"));
end SingleBuilding;
