within GreenVillage.Subsystems.BuildingLoads.SingleBuilding;
model VectorizedChannel "Multi-channels model"
  parameter Integer num(min=1) = 1
    "Number of channels";
  parameter Buildings.Utilities.Time.Types.ZeroTime zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017
    "Enumeration for choosing how reference time (time = 0) should be defined";
  parameter Integer yearRef=2017
    "Year when time = 0, used if zerTim=Custom";
  parameter String fileName[num]
    "File where matrix is stored";
  parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments
    "Smoothness of table interpolation";
  parameter Modelica.SIunits.Frequency f_cut=1/120
    "Cut-off frequency";
  parameter String figureName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/SingleBuilding/singleChannel.png"
    "Icon path";
  GreenVillage.Subsystems.BuildingLoads.SingleBuilding.SingleChannel sinCha[num](
    each zerTim=zerTim,
    each yearRef=yearRef,
    fileName=fileName,
    each smoothness=smoothness,
    each f_cut=f_cut)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Interfaces.RealOutput pow "Power demand"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Math.MultiSum totPow(nu=num) "Total power demand "
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));
equation
  connect(sinCha[1:num].pow, totPow.u[1:num])
    annotation (Line(points={{11,0},{40,0}}, color={0,0,127}));
  connect(totPow.y, pow)
    annotation (Line(points={{61.7,0},{110,0}}, color={0,0,127}));
  annotation (Documentation(revisions="<html>
<ul>
<li>
February 27, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>", info="<html>
<p>
This model implements a vectorized Channels which is developed from the model of 
<a href=\"modelica://GreenVillage.Subsystems.BuildingLoads.SingleBuilding.SingleChannel\">GreenVillage.Subsystems.BuildingLoads.SingleBuilding.SingleChannel</a>.
The number of channels can be specified by the parameter
<code>num</code>. 
</p>
</html>"), Icon(graphics={Bitmap(extent={{-100,-100},{100,100}}, fileName=figureName),
        Text(extent={{-148,-148},{152,-108}},lineColor={0,0,255},textString="%name")}));
end VectorizedChannel;
