within GreenVillage.Subsystems.BuildingLoads.Examples;
model CampusLoads "Test the Loads model"
  extends Modelica.Icons.Example;
  GreenVillage.Subsystems.BuildingLoads.Loads HGVCam "All building loads"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  GreenVillage.Subsystems.Grid.Grid gri "Electrical grid"
    annotation (Placement(transformation(extent={{50,-8},{70,12}})));
equation
  connect(HGVCam.terminal, gri.terminal) annotation (Line(points={{11,0},{30,0},
          {30,-20},{60,-20},{60,-7.8}}, color={0,120,120}));
  annotation (
    Documentation(info="<html>
    <p>This model is used to test whether the model of <a href=\"modelica://GreenVillage.
    Subsystems.BuildingLoads.Loads\">GreenVillage.Subsystems.BuildingLoads.Loads</a> could connect to grid successfully. </p>
</html>", revisions="<html>
<ul>
<li>
April 9, 2018 by Yangyang Fu:<br/>
Add scripts.
</li>
<li>
March 6, 2018 by Danlin Hou:<br/>
First implementation.
</li>
</ul>
</html>"),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Subsystems/BuildingLoads/Examples/CampusLoads.mos"
        "Simulate and Plot"),
    experiment(StartTime=15552000, StopTime=15984000));
end CampusLoads;
