within GreenVillage.Subsystems.BuildingLoads;
model Loads "All building loads"
  parameter String figureName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/Loads/buildingA.jpeg"
   "Icon path of each submodel";
  parameter Integer num=22 "Number of channels";
  GreenVillage.Subsystems.BuildingLoads.SingleBuilding.VectorizedChannel buiA(
    num=22,
    yearRef=2017,
    f_cut=1/120,
    zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017,
    smoothness=Modelica.Blocks.Types.Smoothness.MonotoneContinuousDerivative2,
    figureName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/Loads/buildingA.jpg",
    fileName={
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH5-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH7-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH10.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH13.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH14.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH15.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH16.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_CH17.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A1_Unmo.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_CH3-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_CH4-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_CH5-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_CH9-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/A2_Unmo.txt")})
    "Power demand of Building A (Thelma Commercial and residential)"
    annotation (Placement(transformation(extent={{-80,70},{-60,90}})));
  GreenVillage.Subsystems.BuildingLoads.SingleBuilding.VectorizedChannel buiC(
    num=40,
    yearRef=2017,
    f_cut=1/120,
    zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017,
    smoothness=Modelica.Blocks.Types.Smoothness.MonotoneContinuousDerivative1,
    figureName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/Loads/buildingC.jpg",
    fileName={
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH4-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH7-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH9-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH10.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH13.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH14.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH15.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH16.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH17.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH18.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH19.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH20.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH21.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH22.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH23.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH24.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_Unmo.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH4-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH5-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH7-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH9-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH10.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH13.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH14.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH15.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH16.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH17.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH18.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH19.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH20.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH21.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_CH22.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C2_Unmo.txt")})
    "Power demand of Building C (General Store)"
    annotation (Placement(transformation(extent={{-80,30},{-60,50}})));
  GreenVillage.Subsystems.BuildingLoads.SingleBuilding.VectorizedChannel buiD(
    num=8,
    yearRef=2017,
    f_cut=1/120,
    zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017,
    smoothness=Modelica.Blocks.Types.Smoothness.MonotoneContinuousDerivative1,
    figureName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/Loads/buildingD.jpg",
    fileName={
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/D_CH4-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/D_CH5-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/D_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/D_ch7_.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/D_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/D_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/D_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/D_Unmo.txt")})
         "Power demand of Building D (Book Store)"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
    GreenVillage.Subsystems.BuildingLoads.SingleBuilding.VectorizedChannel buiF(
    num=29,
    yearRef=2017,
    f_cut=1/120,
    zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017,
    smoothness=Modelica.Blocks.Types.Smoothness.MonotoneContinuousDerivative1,
    figureName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/Loads/buildingF.jpg",
    fileName={
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH4-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH9-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH10.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH13.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH14.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH15.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH16.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH17.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH18.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH19.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH20.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH21.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH22.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH23.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_CH24.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F1_Unmo.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_CH3-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_CH5-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_CH7-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_CH9-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_CH10.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/F2_Unmo.txt")})
    "Power demand of Building F (Bakery and Gallery)"
    annotation (Placement(transformation(extent={{-80,-50},{-60,-30}})));
   GreenVillage.Subsystems.BuildingLoads.SingleBuilding.VectorizedChannel buiG(
    num=40,
    yearRef=2017,
    f_cut=1/120,
    zerTim=Buildings.Utilities.Time.Types.ZeroTime.NY2017,
    smoothness=Modelica.Blocks.Types.Smoothness.MonotoneContinuousDerivative1,
    figureName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/Loads/buildingG.jpg",
    fileName={
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH4-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH6-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH7-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH9-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH10.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH13.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH14.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH15.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH16.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH17.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH18.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH19.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH20.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH21.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH22.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH23.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_CH24.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/C1_Unmo.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH3-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH5-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH7-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH8-.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH10.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH11.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH12.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH13.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH14.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH15.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH17.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH18.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH19.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH20.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH21.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH22.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH23.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_CH24.txt"),
        ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/BuildingLoads/Loads/G2_Unmo.txt")})
    "Power demand of Building G (Pilsbury Commercial and residential)"
    annotation (Placement(transformation(extent={{-80,-90},{-60,-70}})));
  Buildings.Electrical.AC.OnePhase.Loads.Inductive loa(mode=Buildings.Electrical.Types.Load.VariableZ_P_input)
    "All campus power demand"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));
  Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n terminal
    "Terminal n for AC one phase systems"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Math.MultiSum pow(nu=5)
    "Total power demand "
    annotation (Placement(transformation(extent={{-20,-10},{0,10}})));
  Modelica.Blocks.Math.Gain powCon(k=-1)
    "Consumed power (negative for cosumption, positive for generation)"
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));
  Buildings.Electrical.AC.OnePhase.Sensors.GeneralizedSensor met "Power meter"
    annotation (Placement(transformation(extent={{96,-10},{76,10}})));
  System.Baseclasss.ControlBus infBus "Information bus" annotation (Placement(
        transformation(extent={{60,-60},{80,-40}}), iconTransformation(extent={
            {-8,-106},{12,-86}})));
equation
  connect(buiA.pow, pow.u[1])
   annotation (Line(points={{-59,80},{-40,80},{-40,0},{-20,0},{-20,5.6}},
                                                                    color={0,0,127}));
  connect(buiC.pow, pow.u[2])
   annotation (Line(points={{-59,40},{-40,40},{-40,0},{-20,0},{-20,2.8}},
                                                                    color={0,0,127}));
  connect(buiD.pow, pow.u[3])
   annotation (Line(points={{-59,0},{-20,0}},color={0,0,127}));
  connect(buiF.pow, pow.u[4])
   annotation (Line(points={{-59,-40},{-40,-40},{-40,0},{-20,0},{-20,-2.8}},
                                                                       color={0,0,127}));
  connect(buiG.pow, pow.u[5])
   annotation (Line(points={{-59,-80},{-40,-80},{-40,0},{-20,0},{-20,-5.6}},
                                                                       color={0,0,127}));
  connect(pow.y, powCon.u)
    annotation (Line(points={{1.7,0},{18,0}}, color={0,0,127}));
  connect(powCon.y, loa.Pow)
    annotation (Line(points={{41,0},{50,0}}, color={0,0,127}));
  connect(loa.terminal, met.terminal_p)
    annotation (Line(points={{70,0},{76,0}}, color={0,120,120}));
  connect(met.terminal_n, terminal)
    annotation (Line(points={{96,0},{110,0}}, color={0,120,120}));
  connect(met.S, infBus.buiLoaAppPow) annotation (Line(points={{92,-9},{92,-26},
          {70,-26},{70,-50}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false),
              graphics={Bitmap(extent={{-100,-100},{100,100}},
              fileName="modelica://GreenVillage/Resources/Images/Subsystems/BuildingLoads/Loads/HGVCampus.jpg"),
              Text(extent={{-152,120},{148,160}},  lineColor={0,0,255},textString="%name")}),
              Diagram(coordinateSystem(preserveAspectRatio=false)),
              Documentation(
info="<html>
<p>Power demand of each building in HGV could be simulated in this model. Also,the total power demand of the whole campus is output.</p>
</html>", revisions="<html>
<ul>
<li>
March 6, 2018 by Danlin Hou:<br/>
First implementation
</li>
</ul>
</html>"));
end Loads;
