within GreenVillage.Subsystems.OccupantBehavior.Schedule;
model Schedule
  "Module containing all schedules for occupancy, heat pump, hot water use and building loads."

  parameter Integer numBui=9 "Number of buildings";

  Modelica.Blocks.Sources.Constant hotWatUse(k=-0.01)
    "Hot water usage mass flow rate"
    annotation (Placement(transformation(extent={{-20,20},{0,40}})));
  GreenVillage.Subsystems.OccupantBehavior.BaseClasses.ScheduleBus schBus
    "Schedule bus"                                                                       annotation (
      Placement(transformation(extent={{90,-10},{110,10}}), iconTransformation(
          extent={{-10,-110},{10,-90}})));
  GreenVillage.Subsystems.OccupantBehavior.BaseClasses.Presence occPre
    "Building occupant presence"
    annotation (Placement(transformation(extent={{-20,-20},{0,0}})));

  GreenVillage.Subsystems.OccupantBehavior.BaseClasses.TStatControl airSetCon "Room air setpoint control"
    annotation (Placement(transformation(extent={{-20,-60},{0,-40}})));
equation
  connect(hotWatUse.y, schBus.schDHW) annotation (Line(points={{1,30},{50,30},{50,
          0},{100,0}},     color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(occPre.y[1], schBus.schF1F2) annotation (Line(points={{1,-10.8889},{
          60,-10.8889},{60,0},{100,0}},
                                     color={255,0,255}));
  connect(occPre.y[2], schBus.schG1) annotation (Line(points={{1,-10.6667},{60,
          -10.6667},{60,0},{100,0}},
                           color={255,0,255}));
  connect(occPre.y[3], schBus.schG2) annotation (Line(points={{1,-10.4444},{60,
          -10.4444},{60,0},{100,0}},
                           color={255,0,255}));
  connect(occPre.y[4], schBus.schA1W) annotation (Line(points={{1,-10.2222},{60,
          -10.2222},{60,0},{100,0}}, color={255,0,255}));
  connect(occPre.y[5], schBus.schA1E) annotation (Line(points={{1,-10},{60,-10},
          {60,0},{100,0}}, color={255,0,255}));
  connect(occPre.y[6], schBus.schA2) annotation (Line(points={{1,-9.77778},{60,-9.77778},
          {60,0},{100,0}}, color={255,0,255}));
  connect(occPre.y[7], schBus.schD) annotation (Line(points={{1,-9.55556},{60,-9.55556},
          {60,0},{100,0}}, color={255,0,255}));
  connect(occPre.y[8], schBus.schC1) annotation (Line(points={{1,-9.33333},{60,-9.33333},
          {60,0},{100,0}}, color={255,0,255}));
  connect(occPre.y[9], schBus.schC2) annotation (Line(points={{1,-9.11111},{60,-9.11111},
          {60,0},{100,0}}, color={255,0,255}));
  connect(occPre.y, airSetCon.occ) annotation (Line(points={{1,-10},{6,-10},{6,
          -36},{-26,-36},{-26,-50},{-22,-50}},
                                          color={255,0,255}));
  connect(airSetCon.TRooAirSet[1], schBus.airSet1) annotation (Line(points={{1,
          -50.8889},{50,-50.8889},{50,0},{100,0}},
                                         color={0,0,127}));
  connect(airSetCon.TRooAirSet[2], schBus.airSet2) annotation (Line(points={{1,
          -50.6667},{50,-50.6667},{50,0},{100,0}},
                                         color={0,0,127}));
  connect(airSetCon.TRooAirSet[3], schBus.airSet3) annotation (Line(points={{1,
          -50.4444},{50,-50.4444},{50,0},{100,0}},
                                         color={0,0,127}));
  connect(airSetCon.TRooAirSet[4], schBus.airSet4) annotation (Line(points={{1,
          -50.2222},{50,-50.2222},{50,0},{100,0}},
                                         color={0,0,127}));
  connect(airSetCon.TRooAirSet[5], schBus.airSet5) annotation (Line(points={{1,-50},
            {50,-50},{50,0},{100,0}},    color={0,0,127}));
  connect(airSetCon.TRooAirSet[6], schBus.airSet6) annotation (Line(points={{1,
          -49.7778},{50,-49.7778},{50,0},{100,0}},
                                         color={0,0,127}));
  connect(airSetCon.TRooAirSet[7], schBus.airSet7) annotation (Line(points={{1,
          -49.5556},{50,-49.5556},{50,0},{100,0}},
                                         color={0,0,127}));
  connect(airSetCon.TRooAirSet[8], schBus.airSet8) annotation (Line(points={{1,
          -49.3333},{50,-49.3333},{50,0},{100,0}},
                                         color={0,0,127}));
  connect(airSetCon.TRooAirSet[9], schBus.airSet9) annotation (Line(points={{1,
          -49.1111},{50,-49.1111},{50,0},{100,0}},
                                         color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                Rectangle(
        extent={{-100,-100},{100,100}},
        lineColor={28,108,200},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
        Ellipse(extent={{-54,74},{-22,44}}, lineColor={0,0,255}),
        Line(
          points={{-38,36},{-6,20}},
          color={0,0,255}),
        Line(
          points={{-66,22},{-38,36}},
          color={0,0,255}),
        Line(
          points={{-38,-20},{-38,44}},
          color={0,0,255}),
        Line(
          points={{-62,-68},{-38,-20},{-14,-70}},
          color={0,0,255}),
        Rectangle(
          extent={{22,12},{78,-70}},
          lineColor={28,108,200},
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Line(points={{22,-18},{78,-18}}, color={28,108,200}),
        Rectangle(
          extent={{26,-2},{30,-16}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{26,-22},{30,-54}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{38,80},{52,72}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{38,72},{52,60}},
          lineColor={28,108,200},
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
              Text(extent={{-150,112},{150,152}},  lineColor={0,0,255},textString
            =                                                                     "%name")}),
                                            Diagram(coordinateSystem(
          preserveAspectRatio=false)),
    Documentation(info="<html>
<p>This model contains information of occupancy, appliance and hot water use schedules.</p>
</html>", revisions="<html>
<ul>
<li>December 16, 2018, by Jing Wang:<br>
First implementation. </li>
</ul>

</html>"));
end Schedule;
