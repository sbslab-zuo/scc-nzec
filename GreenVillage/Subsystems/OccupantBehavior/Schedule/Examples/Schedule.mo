within GreenVillage.Subsystems.OccupantBehavior.Schedule.Examples;
model Schedule "Test the schedule module."
  extends Modelica.Icons.Example;

  GreenVillage.Subsystems.OccupantBehavior.Schedule.Schedule occSch
    annotation (Placement(transformation(extent={{-40,40},{-20,60}})));
  HeatPump.WSHPPhysics.BaseModels.FanSpeedControl fanSpeCon(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    Ti=60,
    reverseAction=true) "Fan speed controller"
    annotation (Placement(transformation(extent={{20,0},{40,20}})));
  BaseClasses.ScheduleBus schBus "Schedule bus"
    annotation (Placement(transformation(extent={{-20,0},{0,20}})));
  Modelica.Blocks.Sources.Sine TRooMea(
    amplitude=5,
    freqHz=1/86400,
    offset=273.15 + 26) "Measured room temperature"
    annotation (Placement(transformation(extent={{-20,-40},{0,-20}})));
equation
  connect(occSch.schBus, schBus) annotation (Line(
      points={{-30,40},{-30,0},{-10,0},{-10,10}},
      color={255,204,51},
      thickness=0.5));
  connect(schBus.airSet1, fanSpeCon.u_s) annotation (Line(
      points={{-10,10},{18,10}},
      color={255,204,51},
      thickness=0.5));
  connect(schBus.schF1F2, fanSpeCon.on) annotation (Line(
      points={{-10,10},{6,10},{6,17},{18,17}},
      color={255,204,51},
      thickness=0.5));
  connect(TRooMea.y, fanSpeCon.u_m) annotation (Line(points={{1,-30},{6,-30},{6,
          2},{18,2},{18,3}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
    <p>This model tests the connection of model <a href=\"modelica://GreenVillage.Application.Resilience.System.BaseClasses.OccupancySchedule\">
    GreenVillage.Application.Resilience.System.BaseClasses.OccupancySchedule</a> and model 
    <a href=\"modelica://GreenVillage.Application.Resilience.Subsystem.Building.Loads\">
    GreenVillage.Application.Resilience.Subsystem.Building.Loads</a>.</p>
</html>", revisions="<html>
<ul>
<li>December 16, 2018, by Jing Wang:<br>
First implementation.</li>
</ul>
</html>"),
    experiment(
      StopTime=172800,
      Tolerance=1e-06,
      __Dymola_Algorithm="Dassl"),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Subsystems/OccupantBehavior/Schedule/Schedule.mos"
        "Simulate and Plot"));
end Schedule;
