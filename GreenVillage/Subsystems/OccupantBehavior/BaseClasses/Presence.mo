within GreenVillage.Subsystems.OccupantBehavior.BaseClasses;
model Presence "Model for simulating occupant presence."

  parameter Integer numBui=9 "Number of buildings";
  Modelica.Blocks.Interfaces.BooleanOutput y[numBui]
    "Building occupant presence"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));

  Modelica.Blocks.Sources.CombiTimeTable occF1F2(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_F1.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60)
    "Occupant presence of building F1, F2"
    annotation (Placement(transformation(extent={{-22,74},{-8,88}})));

  Modelica.Blocks.Sources.CombiTimeTable occC1(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_C1.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building C1"
    annotation (Placement(transformation(extent={{-22,-66},{-8,-52}})));

  Modelica.Blocks.Sources.CombiTimeTable occG1(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_G1.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building G1"
    annotation (Placement(transformation(extent={{-22,54},{-8,68}})));

  Modelica.Blocks.Sources.CombiTimeTable occC2(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_C2.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building C2."
    annotation (Placement(transformation(extent={{-22,-86},{-8,-72}})));

  Modelica.Blocks.Sources.CombiTimeTable occA1w(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_A1w.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building A1w."
    annotation (Placement(transformation(extent={{-22,14},{-8,28}})));

  Modelica.Blocks.Sources.CombiTimeTable occA1e(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_A1e.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building A1e."
    annotation (Placement(transformation(extent={{-22,-6},{-8,8}})));

  Modelica.Blocks.Sources.CombiTimeTable occD(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_D.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building D."
    annotation (Placement(transformation(extent={{-22,-46},{-8,-32}})));

  Modelica.Blocks.Sources.CombiTimeTable occG2(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_G2.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building G2."
    annotation (Placement(transformation(extent={{-22,34},{-8,48}})));

  Modelica.Blocks.Sources.CombiTimeTable occA2(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/presence_A2.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building A2."
    annotation (Placement(transformation(extent={{-22,-26},{-8,-12}})));

  Modelica.Blocks.Math.RealToBoolean reaToBoo1
  annotation (Placement(transformation(extent={{10,74},{24,88}})));
  Modelica.Blocks.Math.RealToBoolean reaToBoo2
  annotation (Placement(transformation(extent={{10,54},{24,68}})));
  Modelica.Blocks.Math.RealToBoolean reaToBoo3
  annotation (Placement(transformation(extent={{10,34},{24,48}})));
  Modelica.Blocks.Math.RealToBoolean reaToBoo4
  annotation (Placement(transformation(extent={{10,14},{24,28}})));
  Modelica.Blocks.Math.RealToBoolean reaToBoo5
  annotation (Placement(transformation(extent={{10,-6},{24,8}})));
  Modelica.Blocks.Math.RealToBoolean reaToBoo6
  annotation (Placement(transformation(extent={{10,-26},{24,-12}})));
  Modelica.Blocks.Math.RealToBoolean reaToBoo7
  annotation (Placement(transformation(extent={{10,-46},{24,-32}})));
  Modelica.Blocks.Math.RealToBoolean reaToBoo8
  annotation (Placement(transformation(extent={{10,-66},{24,-52}})));
  Modelica.Blocks.Math.RealToBoolean reaToBoo9
  annotation (Placement(transformation(extent={{10,-86},{24,-72}})));
equation
  connect(occF1F2.y[1], reaToBoo1.u) annotation (Line(points={{-7.3,81},{8.6,81}},
                                   color={0,0,127}));
  connect(occG1.y[1], reaToBoo2.u) annotation (Line(points={{-7.3,61},{8.6,61}},
                               color={0,0,127}));
  connect(occG2.y[1], reaToBoo3.u) annotation (Line(points={{-7.3,41},{8.6,41}}, color={0,0,127}));
  connect(occA1w.y[1], reaToBoo4.u) annotation (Line(points={{-7.3,21},{8.6,21}}, color={0,0,127}));
  connect(occA1e.y[1], reaToBoo5.u) annotation (Line(points={{-7.3,1},{8.6,1}}, color={0,0,127}));
  connect(occA2.y[1], reaToBoo6.u) annotation (Line(points={{-7.3,-19},{8.6,-19}}, color={0,0,127}));
  connect(occD.y[1], reaToBoo7.u) annotation (Line(points={{-7.3,-39},{8.6,-39}}, color={0,0,127}));
  connect(occC1.y[1], reaToBoo8.u) annotation (Line(points={{-7.3,-59},{8.6,-59}}, color={0,0,127}));
  connect(occC2.y[1], reaToBoo9.u) annotation (Line(points={{-7.3,-79},{8.6,-79}}, color={0,0,127}));
  connect(reaToBoo1.y, y[1]);
  connect(reaToBoo2.y, y[2]);
  connect(reaToBoo3.y, y[3]);
  connect(reaToBoo4.y, y[4]);
  connect(reaToBoo5.y, y[5]);
  connect(reaToBoo6.y, y[6]);
  connect(reaToBoo7.y, y[7]);
  connect(reaToBoo8.y, y[8]);
  connect(reaToBoo9.y, y[9]);

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                     Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-154,-16},{146,24}},
          lineColor={0,0,255},
          textString="%name")}), Diagram(coordinateSystem(preserveAspectRatio=
            false)),
    experiment(
      StartTime=18576000,
      StopTime=18835200,
      __Dymola_Algorithm="Dassl"));
end Presence;
