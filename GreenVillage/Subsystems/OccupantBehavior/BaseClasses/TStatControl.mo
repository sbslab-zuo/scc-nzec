within GreenVillage.Subsystems.OccupantBehavior.BaseClasses;
model TStatControl
  "TStat control model for commercial and residential buildings."
  parameter Integer numBui=9 "Number of buildings";

  Modelica.Blocks.Interfaces.BooleanInput occ[numBui]
    "Indoor occupancy, true for occupied"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));

  Modelica.Blocks.Logical.Switch swi[numBui] "Switch between two setpoints"
  annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  Modelica.Blocks.Interfaces.RealOutput TRooAirSet[numBui]
    "Room air temperature setpoint"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Sources.Constant rooAirSetOcc(k=273.15 +
        24) "Room air set point when occupied"
    annotation (Placement(transformation(extent={{-60,20},{-40,40}})));
  Modelica.Blocks.Sources.Constant rooAirSetUnOcc(k=273.15
         + 28) "Room air set point when unoccupied"
    annotation (Placement(transformation(extent={{-60,-40},{-40,-20}})));
equation
  connect(rooAirSetOcc.y, swi[1].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},        color={0,0,127}));
  connect(rooAirSetOcc.y, swi[2].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},        color={0,0,127}));
  connect(rooAirSetOcc.y, swi[3].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},        color={0,0,127}));
  connect(rooAirSetOcc.y, swi[4].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},        color={0,0,127}));
  connect(rooAirSetOcc.y, swi[5].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},      color={0,0,127}));
  connect(rooAirSetOcc.y, swi[6].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},          color={0,0,127}));
  connect(rooAirSetOcc.y, swi[7].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},          color={0,0,127}));
  connect(rooAirSetOcc.y, swi[8].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},          color={0,0,127}));
  connect(rooAirSetOcc.y, swi[9].u1) annotation (Line(points={{-39,30},{-28,30},
          {-28,8},{-12,8}},          color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[1].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}}, color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[2].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}}, color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[3].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}}, color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[4].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}}, color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[5].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}}, color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[6].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}},   color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[7].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}},   color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[8].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}},   color={0,0,127}));
  connect(rooAirSetUnOcc.y, swi[9].u3) annotation (Line(points={{-39,-30},{-20,
          -30},{-20,-8},{-12,-8}},   color={0,0,127}));
  connect(occ, swi.u2)
    annotation (Line(points={{-120,0},{-12,0}}, color={255,0,255}));
  connect(swi.y, TRooAirSet)
    annotation (Line(points={{11,0},{110,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                     Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-154,-16},{146,24}},
          lineColor={0,0,255},
          textString="%name")}), Diagram(coordinateSystem(preserveAspectRatio=false)));
end TStatControl;
