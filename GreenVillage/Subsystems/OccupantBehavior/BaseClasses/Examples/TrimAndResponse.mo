within GreenVillage.OccupantBehavior.BaseClasses.Examples;
model TrimAndResponse
  extends Modelica.Icons.Example;
  Buildings.Controls.OBC.CDL.Continuous.Sources.Constant PAct(k=600)
    "Actual HVAC power"
    annotation (Placement(transformation(extent={{-60,-30},{-40,-10}})));
  Buildings.Controls.OBC.CDL.Continuous.Sources.Constant PAll(k=10000)
    "Allowed HVAC power"
    annotation (Placement(transformation(extent={{-60,20},{-40,40}})));
  GreenVillage.OccupantBehavior.BaseClasses.TrimAndRespond triRes
    "Trim and response controller for T setpoint"
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));
equation
  connect(PAll.y,triRes. u1) annotation (Line(points={{-39,30},{-32,30},{-32,4},
          {-2,4}},         color={0,0,127}));
  connect(PAct.y, triRes.u2) annotation (Line(points={{-39,-20},{-20,-20},{-20,-4},
          {-2,-4}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=600));
end TrimAndResponse;
