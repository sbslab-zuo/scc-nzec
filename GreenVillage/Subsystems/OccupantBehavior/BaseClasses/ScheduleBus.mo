within GreenVillage.Subsystems.OccupantBehavior.BaseClasses;
expandable connector ScheduleBus
  "Data bus that stores occupancy and equipment schedules."
  extends Modelica.Icons.SignalBus;

  annotation (
    defaultComponentName="weaBus",
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
            100}}), graphics={Rectangle(
          extent={{-20,2},{22,-2}},
          lineColor={255,204,51},
          lineThickness=0.5)}),
    Documentation(info="<html>
<p>This component model is a bus model that connects <a href=\"modelica://GreenVillage.Application.Resilience.System.BaseClasses.OccupancySchedule\">GreenVillage.Application.Resilience.System.BaseClasses.OccupancySchedule</a> with other models that need schedule information.</p>
</html>", revisions="<html>
<ul>
<li>December 16, 2018, by Jing Wang:<br>First implementation. </li>
</ul>
</html>"));
end ScheduleBus;
