within GreenVillage.OccupantBehavior.Lighting.Examples;
model LightsC1 "A lighting model."
  extends GreenVillage.BuildingLoads.Dynamic.BaseClasses.PartialAppliance(
    nomPow=1158,
    kHea=0.9,kRad=0.8,kCon=0.2,kLat=0,
    powCon(nu=2));

  Buildings.Controls.OBC.CDL.Conversions.BooleanToReal booToRea
    annotation (Placement(transformation(extent={{-56,0},{-40,16}})));
  OccupantBehavior.Lighting.Hunt1979Light ligCon "Lighting control model."
    annotation (Placement(transformation(extent={{-82,0},{-66,16}})));
  Modelica.Blocks.Sources.CombiTimeTable occC1(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Calibration/presence_C1.txt"),
    tableName="table",
    timeScale(displayUnit="s") = 60)   "Occupant presence of building C1."
    annotation (Placement(transformation(extent={{-100,40},{-86,54}})));

  Modelica.Blocks.Math.RealToBoolean reaToBoo
    annotation (Placement(transformation(extent={{-68,40},{-54,54}})));
  Modelica.Blocks.Sources.CombiTimeTable illC1(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Subsystems/OccupantBehavior/Schedule/ill.txt"),
    tableName="table")
    "Indoor daylighting illuminance level (testing, unit: lux)."
    annotation (Placement(transformation(extent={{-100,-40},{-86,-26}})));

  GreenVillage.OccupantBehavior.Lighting.Metrics.NMBE nmbe
    annotation (Placement(transformation(extent={{-20,-80},{0,-60}})));
  Modelica.Blocks.Sources.CombiTimeTable ligPowC1(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource(
        "modelica://GreenVillage/Resources/Input/Calibration/lig_power_C1.txt"),
    tableName="table",
    timeScale(displayUnit="s") = 60) "Lighting power data in Watts."
    annotation (Placement(transformation(extent={{-60,-80},{-46,-66}})));

equation
  connect(ligCon.on, booToRea.u)
    annotation (Line(points={{-65.2,8},{-57.6,8}}, color={255,0,255}));
  connect(booToRea.y, powCon.u[2]) annotation (Line(points={{-39.2,8},{-18,8},{
          -18,0},{0,0}}, color={0,0,127}));
  connect(occC1.y[1], reaToBoo.u)
    annotation (Line(points={{-85.3,47},{-69.4,47}}, color={0,0,127}));
  connect(reaToBoo.y, ligCon.occ) annotation (Line(points={{-53.3,47},{-48,47},
          {-48,26},{-92,26},{-92,12.8},{-83.6,12.8}}, color={255,0,255}));
  connect(illC1.y[1], ligCon.ill) annotation (Line(points={{-85.3,-33},{-70,-33},
          {-70,-10},{-92,-10},{-92,3.2},{-83.6,3.2}}, color={0,0,127}));
  connect(powCon.y,nmbe. u1) annotation (Line(points={{21.7,0},{30,0},{30,-54},
          {-30,-54},{-30,-64},{-22,-64}}, color={0,0,127}));
  connect(ligPowC1.y[1],nmbe. u2) annotation (Line(points={{-45.3,-73},{-30,-73},
          {-30,-76},{-22,-76}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end LightsC1;
