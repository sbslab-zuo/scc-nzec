within GreenVillage.OccupantBehavior.Lighting.Examples;
model LightsF1_two_stage_model "A lighting model."

  parameter Real nomPow1=440.31  "Basic power of the lighting";
  parameter Real nomPow2=45.67   "Secondary power of the lighting";

  GreenVillage.OccupantBehavior.Lighting.Metrics.NMBE nmbe
    annotation (Placement(transformation(extent={{60,-50},{80,-30}})));
  Modelica.Blocks.Sources.CombiTimeTable ligPowF1(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Calibration/lig_power_F1.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Lighting power data in Watts."
    annotation (Placement(transformation(extent={{-20,-50},{0,-30}})));

  GreenVillage.OccupantBehavior.Lighting.PresenceBase pre1(
    beta0A=-11.9311,
    beta1A=0.02541,
    beta0L=13.7769,
    beta1L=-0.01250)
    annotation (Placement(transformation(extent={{-90,60},{-70,80}})));
  Buildings.Controls.OBC.CDL.Conversions.BooleanToReal booToRea
    annotation (Placement(transformation(extent={{-50,60},{-30,80}})));
  GreenVillage.OccupantBehavior.Lighting.Metrics.Energy eneErr
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
  Modelica.Blocks.Sources.RealExpression pow1(y=nomPow1) "Basic lighting power"
    annotation (Placement(transformation(extent={{-50,40},{-30,60}})));
  Modelica.Blocks.Math.MultiProduct powCon1(nu=2)
    "Actual power consumption of the appliance"
    annotation (Placement(transformation(extent={{-10,50},{10,70}})));
  Modelica.Blocks.Interfaces.RealOutput elePow "Electric power consumption [W]"
    annotation (Placement(transformation(extent={{100,30},{120,50}})));
  Modelica.Blocks.Interfaces.RealOutput reaPow "Electric power consumption [W]"
    annotation (Placement(transformation(extent={{100,-70},{120,-50}})));
  GreenVillage.OccupantBehavior.Lighting.PresenceExtra pre2(samplePeriod=86400)
    annotation (Placement(transformation(extent={{-90,10},{-70,30}})));
  Buildings.Controls.OBC.CDL.Conversions.BooleanToReal booToRea1
    annotation (Placement(transformation(extent={{-50,10},{-30,30}})));
  Modelica.Blocks.Sources.RealExpression pow2(y=nomPow2)
    "Secondary lighting power"
    annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
  Modelica.Blocks.Math.MultiProduct powCon2(nu=3)
    "Actual power consumption of the appliance"
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  Buildings.Controls.OBC.CDL.Continuous.Add add(k1=1, k2=1)
    annotation (Placement(transformation(extent={{26,20},{46,40}})));
equation

  connect(pre1.occ, booToRea.u)
    annotation (Line(points={{-69,70},{-52,70}}, color={255,0,255}));
  connect(ligPowF1.y[1], eneErr.u2) annotation (Line(points={{1,-40},{28,-40},{28,
          -6},{58,-6}},
                      color={0,0,127}));
  connect(ligPowF1.y[1],nmbe. u2) annotation (Line(points={{1,-40},{32,-40},{32,
          -46},{58,-46}}, color={0,0,127}));
  connect(booToRea.y, powCon1.u[1]) annotation (Line(points={{-29,70},{-20,70},{
          -20,63.5},{-10,63.5}}, color={0,0,127}));
  connect(pow1.y, powCon1.u[2]) annotation (Line(points={{-29,50},{-20,50},{-20,
          56.5},{-10,56.5}}, color={0,0,127}));
  connect(ligPowF1.y[1], reaPow) annotation (Line(points={{1,-40},{20,-40},{20,-60},
          {110,-60}},      color={0,0,127}));
  connect(booToRea1.y, powCon2.u[1]) annotation (Line(points={{-29,20},{-20,20},
          {-20,14.6667},{-10,14.6667}},
                                  color={0,0,127}));
  connect(pow2.y, powCon2.u[2]) annotation (Line(points={{-29,0},{-20,0},{-20,
          10},{-10,10}},     color={0,0,127}));
  connect(powCon1.y, add.u1) annotation (Line(points={{11.7,60},{18,60},{18,36},
          {24,36}}, color={0,0,127}));
  connect(powCon2.y, add.u2) annotation (Line(points={{11.7,10},{18,10},{18,24},
          {24,24}}, color={0,0,127}));
  connect(add.y, eneErr.u1) annotation (Line(points={{47,30},{50,30},{50,6},{58,
          6}},  color={0,0,127}));
  connect(add.y,nmbe. u1) annotation (Line(points={{47,30},{50,30},{50,-34},{58,
          -34}}, color={0,0,127}));
  connect(pre2.on, booToRea1.u)
    annotation (Line(points={{-69,20},{-52,20}}, color={255,0,255}));
  connect(booToRea.y, powCon2.u[3]) annotation (Line(points={{-29,70},{-26,70},
          {-26,5.33333},{-10,5.33333}}, color={0,0,127}));
  connect(add.y, elePow) annotation (Line(points={{47,30},{60,30},{60,40},{110,
          40}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                     Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-154,-16},{146,24}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Calibration/LightsF1_two_stage_model.mos"
        "Simulate and Plot", file=
          "Resources/Scripts/Dymola/Calibration/LightsF1_two_stage_model.mos"
        "Simulate and Plot"),
    experiment(
      StartTime=19180800,
      StopTime=19267200,
      Interval=299.999808));
end LightsF1_two_stage_model;
