within GreenVillage.OccupantBehavior.Lighting.Examples.Metrics;
block NMBE "Normalized mean bias error"
  extends Modelica.Blocks.Interfaces.SI2SO;
  // u1 is simulated data
  // u2 is real data
  Buildings.Controls.OBC.CDL.Continuous.Add add(k1=1, k2=-1)
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Continuous.Integrator integrator1
    annotation (Placement(transformation(extent={{20,10},{40,30}})));
  Modelica.Blocks.Continuous.Integrator integrator2(y_start=1)
    annotation (Placement(transformation(extent={{-40,-70},{-20,-50}})));
  Modelica.Blocks.Math.Division division2
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
equation

  connect(u1, add.u1)
    annotation (Line(points={{-120,60},{-90,60},{-90,56},{-62,56}},
                                                           color={0,0,127}));
  connect(u2, add.u2) annotation (Line(points={{-120,-60},{-80,-60},{-80,44},{
          -62,44}}, color={0,0,127}));
  connect(u2, integrator2.u) annotation (Line(points={{-120,-60},{-42,-60}},
                                     color={0,0,127}));
  connect(integrator1.y, division2.u1) annotation (Line(points={{41,20},{48,20},
          {48,20},{48,20},{48,6},{58,6}}, color={0,0,127}));
  connect(integrator2.y, division2.u2) annotation (Line(points={{-19,-60},{20,
          -60},{20,-6},{58,-6}},            color={0,0,127}));
  connect(division2.y, y) annotation (Line(points={{81,0},{110,0}},
                          color={0,0,127}));
  connect(add.y, integrator1.u) annotation (Line(points={{-39,50},{0,50},{0,20},
          {18,20}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={Text(
          extent={{-62,34},{64,-42}},
          lineColor={238,46,47},
          textString="NMBE")}),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end NMBE;
