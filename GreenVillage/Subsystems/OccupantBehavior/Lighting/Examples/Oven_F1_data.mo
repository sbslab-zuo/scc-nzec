within GreenVillage.OccupantBehavior.Lighting.Examples;
model Oven_F1_data

  parameter Real nomPow=75.78 "Nominal power of the lighting";

  GreenVillage.OccupantBehavior.Lighting.Metrics.NMBE nmbe
    annotation (Placement(transformation(extent={{40,-40},{60,-20}})));
  Modelica.Blocks.Sources.CombiTimeTable ovePowF1(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Calibration/oven_power_F1.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Oven power data in Watts."
    annotation (Placement(transformation(extent={{-20,-40},{0,-20}})));

  GreenVillage.OccupantBehavior.Lighting.Metrics.Energy eneErr
    annotation (Placement(transformation(extent={{40,0},{60,20}})));
  Modelica.Blocks.Sources.RealExpression pow(y=nomPow)
    "Nominal power demand of the appliance [W]"
    annotation (Placement(transformation(extent={{-40,10},{-20,30}})));
  Modelica.Blocks.Math.MultiProduct powCon(nu=2)
    "Actual power consumption of the appliance"
    annotation (Placement(transformation(extent={{0,40},{20,60}})));
  Modelica.Blocks.Interfaces.RealOutput elePow "Electric power consumption [W]"
    annotation (Placement(transformation(extent={{100,40},{120,60}})));
  Modelica.Blocks.Interfaces.RealOutput reaPow "Electric power consumption [W]"
    annotation (Placement(transformation(extent={{100,-60},{120,-40}})));
  Modelica.Blocks.Sources.CombiTimeTable occBasF1(
    tableOnFile=true,
    table=fill(
          0.0,
          0,
          2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/Calibration/presence_F1_base.txt"),
    tableName="table",
    timeScale(displayUnit="min") = 60) "Occupant presence of building F1."
    annotation (Placement(transformation(extent={{-40,60},{-20,80}})));

equation

  connect(occBasF1.y[1], powCon.u[1]);
  connect(pow.y,powCon. u[2]) annotation (Line(points={{-19,20},{-10,20},{-10,
          46.5},{0,46.5}},     color={0,0,127}));
  connect(powCon.y, elePow)
    annotation (Line(points={{21.7,50},{110,50}}, color={0,0,127}));
  connect(powCon.y, eneErr.u1) annotation (Line(points={{21.7,50},{28,50},{28,16},
          {38,16}}, color={0,0,127}));
  connect(ovePowF1.y[1], eneErr.u2) annotation (Line(points={{1,-30},{28,-30},{28,
          4},{38,4}}, color={0,0,127}));
  connect(powCon.y,nmbe. u1) annotation (Line(points={{21.7,50},{32,50},{32,-24},
          {38,-24}}, color={0,0,127}));
  connect(ovePowF1.y[1],nmbe. u2) annotation (Line(points={{1,-30},{32,-30},{32,
          -36},{38,-36}}, color={0,0,127}));
  connect(ovePowF1.y[1], reaPow) annotation (Line(points={{1,-30},{20,-30},{20,
          -50},{110,-50}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                     Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={28,108,200},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-154,-16},{146,24}},
          lineColor={0,0,255},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file="Resources/Scripts/Dymola/Calibration/Oven_F1_data.mos"
        "Simulate and Plot"),
    experiment(
      StartTime=18316800,
      StopTime=20995200,
      Interval=299.999808));
end Oven_F1_data;
