within GreenVillage.OccupantBehavior.Lighting.Metrics;
block Energy "Energy difference calculation"
  // u1 is simulated data
  // u2 is real data
  Buildings.Controls.OBC.CDL.Continuous.Add add(k1=1, k2=-1)
    annotation (Placement(transformation(extent={{-20,-10},{0,10}})));
  Modelica.Blocks.Continuous.Integrator int2(y_start=1)
    annotation (Placement(transformation(extent={{-60,-70},{-40,-50}})));
  Modelica.Blocks.Math.Division div
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
  Modelica.Blocks.Math.Abs abs
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));
  Modelica.Blocks.Continuous.Integrator int1
    annotation (Placement(transformation(extent={{-60,50},{-40,70}})));
  Modelica.Blocks.Interfaces.RealOutput absErr
    "Connector of Real output signal"
    annotation (Placement(transformation(extent={{100,10},{120,30}})));
  Modelica.Blocks.Interfaces.RealInput
            u1 "Connector of Real input signal 1" annotation (Placement(
        transformation(extent={{-140,40},{-100,80}})));
  Modelica.Blocks.Interfaces.RealInput
            u2 "Connector of Real input signal 2" annotation (Placement(
        transformation(extent={{-140,-80},{-100,-40}})));
  Modelica.Blocks.Interfaces.RealOutput relErr
    "Connector of Real output signal"
    annotation (Placement(transformation(extent={{100,-30},{120,-10}})));
  Modelica.Blocks.Interfaces.RealOutput simEne
    "Connector of Real output signal"
    annotation (Placement(transformation(extent={{100,50},{120,70}})));
  Modelica.Blocks.Interfaces.RealOutput truEne
    "Connector of Real output signal"
    annotation (Placement(transformation(extent={{100,-70},{120,-50}})));
equation

  connect(u2, int2.u)
    annotation (Line(points={{-120,-60},{-62,-60}}, color={0,0,127}));
  connect(int2.y, div.u2) annotation (Line(points={{-39,-60},{50,-60},{50,-6},
          {58,-6}}, color={0,0,127}));
  connect(div.y, relErr) annotation (Line(points={{81,0},{96,0},{96,-20},{110,
          -20}}, color={0,0,127}));
  connect(u1, int1.u)
    annotation (Line(points={{-120,60},{-62,60}}, color={0,0,127}));
  connect(int1.y, add.u1) annotation (Line(points={{-39,60},{-32,60},{-32,6},
          {-22,6}}, color={0,0,127}));
  connect(int2.y, add.u2) annotation (Line(points={{-39,-60},{-32,-60},{-32,
          -6},{-22,-6}}, color={0,0,127}));
  connect(add.y, abs.u)
    annotation (Line(points={{1,0},{18,0}}, color={0,0,127}));
  connect(abs.y, div.u1) annotation (Line(points={{41,0},{50,0},{50,6},{58,6}},
        color={0,0,127}));
  connect(abs.y, absErr) annotation (Line(points={{41,0},{50,0},{50,20},{110,
          20}}, color={0,0,127}));
  connect(int1.y, simEne)
    annotation (Line(points={{-39,60},{110,60}}, color={0,0,127}));
  connect(int2.y, truEne)
    annotation (Line(points={{-39,-60},{110,-60}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                                Rectangle(
        extent={{-100,-100},{100,100}},
        lineColor={0,0,127},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),                                   Text(
          extent={{-62,34},{64,-42}},
          lineColor={238,46,47},
          textString="eneErr"),         Text(
        extent={{-150,150},{150,110}},
        textString="%name",
        lineColor={0,0,255})}),                                  Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Energy;
