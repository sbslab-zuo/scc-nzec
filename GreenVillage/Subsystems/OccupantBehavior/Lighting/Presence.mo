within GreenVillage.OccupantBehavior.Lighting;
model Presence
  "A model to predict occupants' presence depending on time of the day"
 extends Modelica.Blocks.Icons.DiscreteBlock;
 parameter Real beta0A = -28.44734 "Intercept of arrival model";
 parameter Real beta1A = 0.046770 "Coefficient of arrival model";
 parameter Real beta0L = 26.17718 "Intercept of leaving model";
 parameter Real beta1L = -0.018689 "Coefficient of leaving model";
 parameter Integer seed = 30 "Seed for the random number generator";
 parameter Modelica.SIunits.Time samplePeriod = 120 "Sample period";

 Modelica.Blocks.Interfaces.BooleanOutput occ "State of occupant presence"
   annotation (Placement(transformation(extent={{100,-10},{120,10}})));

 Real pArriv(
   unit="1",
   min=0,
   max=1) "Probability of people arriving";
 Real pLeave(
   unit="1",
   min=0,
   max=1) "Probability of people leaving";
 Real x(unit="min",min=0,max=1440) "Time of the day, independent variable";

protected
 parameter Modelica.SIunits.Time t0(final fixed = false) "First sample time instant";
 output Boolean sampleTrigger "True, if sample time instant";
 Real curSeed "Current value for seed as a real-valued variable";

initial equation
 t0 = time;
 curSeed = t0*seed;
 occ = false;
equation
 x=rem((time-t0),86400)/60;
 pArriv = 1/(1 + Modelica.Math.exp(-(beta0A+beta1A*x)));
 pLeave = 1/(1 + Modelica.Math.exp(-(beta0L+beta1L*x)));
 sampleTrigger = sample(t0, samplePeriod);
 when sampleTrigger then
   curSeed = seed*time;
   if x<720 then
     if pre(occ) then
       occ = pre(occ);
     else
       occ = Buildings.Occupants.BaseClasses.binaryVariableGeneration(pArriv, globalSeed=integer(curSeed));
     end if;
   else
     if not pre(occ) then
       occ = pre(occ);
     else
       occ = Buildings.Occupants.BaseClasses.binaryVariableGeneration(pLeave, globalSeed=integer(curSeed));
     end if;
   end if;
 end when;
 annotation (Icon(graphics={
           Rectangle(extent={{-60,40},{60,-40}}, lineColor={28,108,200}), Text(
           extent={{-40,20},{40,-20}},
           lineColor={28,108,200},
           fillColor={0,0,255},
           fillPattern=FillPattern.Solid,
           textStyle={TextStyle.Bold},
          textString="Presence")}),
    defaultComponentName="lig",
    Documentation(info=
                  "<html>
<p>
Model predicting the state of the lighting with the minimum illuminance on the working plane
and occupancy.
</p>
<h4>Dynamics</h4>
<p>
In this model, it was found people tend to switch on the lights-if needed- only at times when
entering a space, and they rarely switch off the lights until the space becomes completely empty.
</p>
<p>
The probability to switch on the lights upon arrival would depend on the minimum illuminance level
on their working plane.
</p>
<h4>References</h4>
<p>
The model is documented in the paper &quot;Hunt, D.R.G., 1980. Predicting artificial
lighting use-a method based upon observed patterns of behaviour. Lighting Research &amp; Technology,
12(1), pp.7-14.&quot;
</p>
<p>
The model parameters are regressed from the field study in 10 offices in Germany from
Mar. to Dec. 2000.
</p>
</html>", revisions=
         "<html>
<ul>
<li>
July 26, 2018, by Zhe Wang:<br/>
First implementation.
</li>
</ul>
</html>"));
end Presence;
