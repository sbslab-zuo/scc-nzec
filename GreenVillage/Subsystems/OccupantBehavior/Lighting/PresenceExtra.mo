within GreenVillage.OccupantBehavior.Lighting;
model PresenceExtra
  "A model to predict occupants' presence depending on time of the day and day of week"
  extends Modelica.Blocks.Icons.DiscreteBlock;
  parameter Real beta0ON = -0.83094 "Intercept of ON model";
  parameter Real beta1ON = -0.48290 "Coefficient of ON model";
  parameter Real beta2ON = 0.49672 "Coefficient of ON model";
  parameter Real beta3ON = -0.25856 "Coefficient of ON model";
  parameter Real beta4ON = 0.49672 "Coefficient of ON model";
  parameter Real beta5ON = -0.11711 "Coefficient of ON model";
  parameter Real beta6ON = -0.48290 "Coefficient of ON model";
  parameter Real beta7ON = -0.48290 "Coefficient of ON model";
  parameter Integer seed = 30 "Seed for the random number generator";
  parameter Modelica.SIunits.Time samplePeriod = 120 "Sample period";

  Modelica.Blocks.Interfaces.BooleanOutput on "State of occupant presence"
   annotation (Placement(transformation(extent={{100,-10},{120,10}})));

  Real pON(
   unit="1",
   min=0,
   max=1) "Probability of extra lights on";
  Real x1 "Day of week, Monday";
  Real x2 "day of week, Tuesday";
  Real x3 "Day of week, Wednesday";
  Real x4 "day of week, Thursday";
  Real x5 "Day of week, Friday";
  Real x6 "day of week, Saturday";
  Real x7 "Day of week, Sunday";

protected
  parameter Modelica.SIunits.Time t0(final fixed = false) "First sample time instant";
  output Boolean sampleTrigger "True, if sample time instant";
  Real curSeed "Current value for seed as a real-valued variable";

initial equation
  t0 = time;
  curSeed = t0*seed;
  on = false;
equation
  if (rem(floor((time-t0)/86400),7)+1) == 1 then  //first day of August 2018 is Wednesday
    [x1,x2,x3,x4,x5,x6,x7]=[0,0,1,0,0,0,0];
  elseif (rem(floor((time-t0)/86400),7)+1) == 2 then
    [x1,x2,x3,x4,x5,x6,x7]=[0,0,0,1,0,0,0];
  elseif (rem(floor((time-t0)/86400),7)+1) == 3 then
    [x1,x2,x3,x4,x5,x6,x7]=[0,0,0,0,1,0,0];
  elseif (rem(floor((time-t0)/86400),7)+1) == 4 then
    [x1,x2,x3,x4,x5,x6,x7]=[0,0,0,0,0,1,0];
  elseif (rem(floor((time-t0)/86400),7)+1) == 5 then
    [x1,x2,x3,x4,x5,x6,x7]=[0,0,0,0,0,0,1];
  elseif (rem(floor((time-t0)/86400),7)+1) == 6 then
    [x1,x2,x3,x4,x5,x6,x7]=[1,0,0,0,0,0,0];
  else
    [x1,x2,x3,x4,x5,x6,x7]=[0,1,0,0,0,0,0];
  end if;
  pON = 1/(1 + Modelica.Math.exp(-(beta0ON+beta1ON*x1+beta2ON*x2+beta3ON*x3+beta4ON*x4+beta5ON*x5+beta6ON*x6+beta7ON*x7)));
  sampleTrigger = sample(t0, samplePeriod);
  when sampleTrigger then
    curSeed = seed*time;
    on = Buildings.Occupants.BaseClasses.binaryVariableGeneration(pON, globalSeed=integer(curSeed));
  end when;
 annotation (Icon(graphics={
           Rectangle(extent={{-60,40},{60,-40}}, lineColor={28,108,200}), Text(
           extent={{-40,20},{40,-20}},
           lineColor={28,108,200},
           fillColor={0,0,255},
           fillPattern=FillPattern.Solid,
           textStyle={TextStyle.Bold},
          textString="Presence")}),
    defaultComponentName="lig",
    Documentation(info=
                  "<html>
<p>
Model predicting the state of the lighting with the minimum illuminance on the working plane
and occupancy.
</p>
<h4>Dynamics</h4>
<p>
In this model, it was found people tend to switch on the lights-if needed- only at times when
entering a space, and they rarely switch off the lights until the space becomes completely empty.
</p>
<p>
The probability to switch on the lights upon arrival would depend on the minimum illuminance level
on their working plane.
</p>
<h4>References</h4>
<p>
The model is documented in the paper &quot;Hunt, D.R.G., 1980. Predicting artificial
lighting use-a method based upon observed patterns of behaviour. Lighting Research &amp; Technology,
12(1), pp.7-14.&quot;
</p>
<p>
The model parameters are regressed from the field study in 10 offices in Germany from
Mar. to Dec. 2000.
</p>
</html>", revisions=
         "<html>
<ul>
<li>
July 26, 2018, by Zhe Wang:<br/>
First implementation.
</li>
</ul>
</html>"),
    __Dymola_Commands(file=
          "Resources/Scripts/Dymola/Calibration/LightsF1_two_stage_data.mos"
        "Simulate and Plot"));
end PresenceExtra;
