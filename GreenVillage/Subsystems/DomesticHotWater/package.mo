within GreenVillage.Subsystems;
package DomesticHotWater "This package intends to simulate domestic hot water system in community"

annotation (Documentation(info="<html>
<p> This package describes the domestic hot water system in HGV. The domestic hot water is provided by two sources: 
one is solar collectors, and the other is two heat pumps installed in Rosedale Buildings. The design documentation is shown 
in the following figure.
</p>
<p><br><img src=\"modelica://GreenVillage/Resources/Images/Subsystems/DomesticHotWater/schematic-hw.PNG\"/></p>
</html>", revisions="<html>

</html>"));
end DomesticHotWater;
