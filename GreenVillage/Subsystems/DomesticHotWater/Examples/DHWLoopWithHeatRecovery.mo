within GreenVillage.Subsystems.DomesticHotWater.Examples;
model DHWLoopWithHeatRecovery
  "Example that demonstrates  the domestic hot water loop"
  extends Modelica.Icons.Example;
  package Medium = Buildings.Media.Water "Fluid in the storage tank";
  GreenVillage.Subsystems.DomesticHotWater.DHWLoopWithHeatRecovery dhw(con(
        m_flow=-0.015))
    "Domestic hot water system with heat recovery from heat pump system"
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  Buildings.Fluid.Sources.Boundary_pT sin(redeclare package Medium = Medium,
      nPorts=1)       "Outlet for hot water draw"
    annotation (Placement(transformation(
      extent={{-10,-10},{10,10}},
      rotation=180,
      origin={50,-30})));
  Buildings.Fluid.Sources.MassFlowSource_T sou(
    redeclare package Medium = Medium,
    use_m_flow_in=false,
    nPorts=1,
    m_flow=0.005,
    T=308.15) "Consumption of hot water" annotation (Placement(transformation(
          extent={{-10,-10},{10,10}}, origin={-50,-30})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSou(
    T_start(displayUnit="K"),
    m_flow_nominal=sou.m_flow,
    redeclare package Medium = Medium) "Temperature sensor"
    annotation (Placement(transformation(extent={{-30,-40},{-10,-20}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSin(
    T_start(displayUnit="K"),
    m_flow_nominal=sou.m_flow,
    redeclare package Medium = Medium) "Temperature sensor"
    annotation (Placement(transformation(extent={{10,-40},{30,-20}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(f=60, V=480)
                                                               "Grid"
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));
equation
  connect(dhw.port_a, TSou.port_b)
    annotation (Line(points={{-6,0},{-6,-30},{-10,-30}}, color={0,127,255}));
  connect(TSou.port_a, sou.ports[1])
    annotation (Line(points={{-30,-30},{-40,-30}}, color={0,127,255}));
  connect(dhw.port_b, TSin.port_a)
    annotation (Line(points={{6,0},{6,-30},{10,-30}}, color={0,127,255}));
  connect(TSin.port_b, sin.ports[1])
    annotation (Line(points={{30,-30},{40,-30}}, color={0,127,255}));
  connect(gri.terminal, dhw.terminal) annotation (Line(points={{-70,20},{-70,-4},
          {0,-4},{0,0.2}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file="modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/DomesticHotWater/Examples/DHWLoopWithHeatRecovery.mos"
        "Simulate and Plot"));
end DHWLoopWithHeatRecovery;
