within GreenVillage.Subsystems.DomesticHotWater.Examples;
model DHWLoop "Domestic hot water loop witjout heat recovery"
  extends Modelica.Icons.Example;
  GreenVillage.Subsystems.DomesticHotWater.DHWLoop dhw
    "Domestic hot water without heat recovery"
    annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri "Grid"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
equation
  connect(gri.terminal, dhw.terminal) annotation (Line(points={{-50,-10},{-50,
          -20},{2,-20},{2,-9.8}}, color={0,120,120}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file(partOfCheck=true) = "modelica://GreenVillage/Resources/Scripts/Dymola/Subsystems/DomesticHotWater/Examples/DHWLoop.mos"
        "Simulate and Plot"));
end DHWLoop;
