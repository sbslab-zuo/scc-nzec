within GreenVillage.Subsystems.DomesticHotWater;
model DHWLoopWithHeatRecovery
  "Example showing use of the flat plate solar collector in a complete solar thermal system"
  extends GreenVillage.Subsystems.DomesticHotWater.DHWLoop(TTanSet2(k=273.15 +
          50));
  parameter Modelica.SIunits.MassFlowRate mHeaPum_flow_nominal = 0.005 "Norminal flowrate of heat recovery system";

  Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
        Medium)
    "Fluid connector a from heat pump system (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-70,-110},{-50,-90}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_b(redeclare package Medium =
        Medium)
    "Fluid connector b to heat pump system(positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{50,-110},{70,-90}})));
  Buildings.Fluid.FixedResistances.PressureDrop res(
    redeclare package Medium = Medium,
    m_flow_nominal=mHeaPum_flow_nominal,
    dp_nominal=6000)
    annotation (Placement(transformation(extent={{98,-96},{78,-76}})));
equation
  connect(port_a, tan2.port_a) annotation (Line(points={{-60,-100},{-60,-80},{
          100,-80},{100,-70}}, color={0,127,255}));
  connect(port_b, res.port_b)
    annotation (Line(points={{60,-100},{60,-86},{78,-86}}, color={0,127,255}));
  connect(res.port_a, tan2.port_b) annotation (Line(points={{98,-86},{126,-86},{
          126,-70},{120,-70}}, color={0,127,255}));
  annotation ( __Dymola_Commands,
        experiment(Tolerance=1e-6, StopTime=86400.0),
        Documentation(info="<html>
          <p>
            This example shows how several different models can be combined to create
            an entire solar water heating system. The
            <a href=\"modelica://Buildings.Fluid.Storage.StratifiedEnhancedInternalHex\">
            Buildings.Fluid.Storage.StratifiedEnhancedInternalHex</a> (tan) model is
            used to represent the tank filled with hot water. A loop, powered by a pump
            (<a href=\"modelica://Buildings.Fluid.Movers.FlowControlled_m_flow\">
            Buildings.Fluid.Movers.FlowControlled_m_flow</a>, pum), passes the water
            through an expansion tank
            (<a href=\"modelica://Buildings.Fluid.Storage.ExpansionVessel\">
            Buildings.Fluid.Storage.ExpansionVessel</a>, exp), a temperature sensor
            (<a href=\"modelica://Buildings.Fluid.Sensors.TemperatureTwoPort\">
            Buildings.Fluid.Sensors.TemperatureTwoPort</a>, TIn), the solar collector
            (<a href=\"modelica://Buildings.Fluid.SolarCollectors.ASHRAE93\">
            Buildings.Fluid.SolarCollectors.ASHRAE93,</a> solCol) and a second temperature
            sensor
            (<a href=\"modelica://Buildings.Fluid.Sensors.TemperatureTwoPort\">
            Buildings.Fluid.Sensors.TemperatureTwoPort</a>, TOut) before re-entering the
            tank.
          </p>
          <p>
            The solar collector is connected to the weather model
            (<a href=\"modelica://Buildings.BoundaryConditions.WeatherData.ReaderTMY3\">
            Buildings.BoundaryConditions.WeatherData.ReaderTMY3</a>, weaDat) which passes
            information for the San Francisco, CA, USA climate. This information is used to
            identify both the heat gain in the water from the sun and the heat loss to the
            ambient conditions.
          </p>
          <p>
            The flow rate through the pump is controlled by a solar pump controller model
            (<a href=\"modelica://Buildings.Fluid.SolarCollectors.Controls.SolarPumpController\">
            Buildings.Fluid.SolarCollectors.Controls.SolarPumpController</a>, pumCon) and a
            gain model. The controller outputs a binary on (1) / off (0) signal. The on/off
            signal is passed through the gain model, multiplying by 0.04, to represent a
            flow rate of 0.04 kg/s when the pump is active.
          </p>
          <p>
            The heat ports for the tank are connected to an ambient temperature of 20
            degrees C representing the temperature of the room the tank is stored in.
          </p>
          <p>
            bou1 (<a href=\"modelica://Buildings.Fluid.Sources.MassFlowSource_T\">
            Buildings.Fluid.Sources.MassFlowSource_T)</a> provides a constant mass flow
            rate for a hot water draw while bou
            (<a href=\"modelica://Buildings.Fluid.Sources.Boundary_pT\">
            Buildings.Fluid.Sources.Boundary_pT)</a> provides a boundary
            condition for the outlet of the draw.
          </p>
      </html>",
revisions="<html>
<ul>
<li>
December 22, 2014 by Michael Wetter:<br/>
Removed <code>Modelica.Fluid.System</code>
to address issue
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/311\">#311</a>.
</li>
<li>
April 18, 2014, by Michael Wetter:<br/>
Updated model to use the revised tank and increased the tank height.
</li>
<li>
March 25, 2014, by Michael Wetter:<br/>
Updated model with new expansion vessel.
</li>
<li>
March 27, 2013 by Peter Grant:<br/>
First implementation
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-160,-100},{180,100}})));
end DHWLoopWithHeatRecovery;
