within GreenVillage.Subsystems.DomesticHotWater;
model DHWLoop
  "Example showing use of the flat plate solar collector in a complete solar thermal system"
  replaceable package Medium = Buildings.Media.Water
    "Fluid in the storage tank";
  parameter Modelica.SIunits.HeatFlowRate tanEleHeaPow_nominal=1500 "Nominal electric heater power in tank 2";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.1 "Nominal flowrate in tank 1";

  Buildings.Fluid.SolarCollectors.ASHRAE93  solCol(
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    nPanels=5,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    per=Buildings.Fluid.SolarCollectors.Data.GlazedFlatPlate.FP_SolahartKf(),
    nSeg=9,
    lat=0.73097781993588,
    azi=0.3,
    til=0.78539816339745,
    redeclare package Medium = Medium)
    "Flat plate solar collector model"
    annotation (Placement(transformation(extent={{-40,38},{-20,58}})));

  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam=
    "modelica://Buildings/Resources/weatherdata/USA_CA_San.Francisco.Intl.AP.724940_TMY3.mos",
    computeWetBulbTemperature=false) "Weather data file reader"
    annotation (Placement(transformation(extent={{-160,80},{-140,100}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T2(
    T_start(displayUnit="K"),
    m_flow_nominal=solCol.m_flow_nominal,
    redeclare package Medium = Medium) "Temperature sensor"
    annotation (Placement(transformation(extent={{12,38},{32,58}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T1(m_flow_nominal=solCol.m_flow_nominal,
      redeclare package Medium = Medium) "Temperature sensor"
    annotation (Placement(transformation(extent={{-70,38},{-50,58}})));
  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex
   tan(
    redeclare package Medium = Medium,
    hTan=1.8,
    VTan=1.5,
    dIns=0.07,
    redeclare package MediumHex = Medium,
    CHex=200,
    dExtHex=0.01905,
    hHex_a=0.9,
    hHex_b=0.65,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    Q_flow_nominal=3000,
    mHex_flow_nominal=3000/20/4200,
    energyDynamicsHex=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nSeg=8,
    m_flow_nominal=m_flow_nominal,
    T_start=293.15,
    TTan_nominal=293.15,
    THex_nominal=323.15)
    "Storage tank model"
    annotation (Placement(transformation(
      extent={{-10,-10},{10,10}},
      origin={-62,-50})));
  Buildings.Fluid.SolarCollectors.Controls.SolarPumpController
    pumCon(per=Buildings.Fluid.SolarCollectors.Data.GlazedFlatPlate.FP_ThermaLiteHS20())
    "Pump controller"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-140,40})));
  Buildings.HeatTransfer.Sources.FixedTemperature rooT(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-118,-90},{-98,-70}})));
  Modelica.Blocks.Math.Gain gain(k=m_flow_nominal)
                                         "Flow rate of the system in kg/s"
    annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=270,
        origin={-140,12})));
  Buildings.Fluid.Sources.Boundary_pT bou(redeclare package Medium =
    Medium, nPorts=1) "Outlet for hot water draw"
    annotation (Placement(transformation(
      extent={{10,-10},{-10,10}},
      rotation=180,
      origin={-130,-20})));
  Buildings.Fluid.Movers.FlowControlled_m_flow pum(
    energyDynamics=Modelica.Fluid.Types.Dynamics.SteadyState,
    redeclare package Medium = Medium,
    m_flow_nominal=m_flow_nominal)
    "Pump forcing circulation through the system" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-100,0})));
  Buildings.Fluid.Storage.ExpansionVessel exp(
    V_start=0.1, redeclare package Medium = Medium)
    "Expansion tank"
    annotation (Placement(transformation(
      extent={{-10,-10},{10,10}},
      origin={-130,-48})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TTan
    "Temperature in the tank water that surrounds the heat exchanger"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-70,10})));
  Buildings.Fluid.Sources.MassFlowSource_T con(
    redeclare package Medium = Medium,
    use_m_flow_in=false,
    nPorts=1,
    m_flow=-0.01)
              "Consumption of hot water" annotation (Placement(transformation(
          extent={{10,-10},{-10,10}}, origin={168,-70})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T3(
    T_start(displayUnit="K"),
    m_flow_nominal=solCol.m_flow_nominal,
    redeclare package Medium = Medium) "Temperature sensor"
    annotation (Placement(transformation(extent={{-40,-60},{-20,-40}})));
  Buildings.Fluid.Actuators.Valves.ThreeWayEqualPercentageLinear val(
    redeclare package Medium = Medium,
    dpValve_nominal=6000,
    dpFixed_nominal={0,12000},
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{10,-60},{30,-40}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T4(
    T_start(displayUnit="K"),
    m_flow_nominal=solCol.m_flow_nominal,
    redeclare package Medium = Medium) "Temperature sensor"
    annotation (Placement(transformation(extent={{50,-80},{70,-60}})));
  Buildings.Controls.Continuous.LimPID conPID(controllerType=Modelica.Blocks.Types.SimpleController.PI,
      Ti=60) annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  Buildings.Controls.OBC.CDL.Continuous.Sources.Constant TMixSet(k=273.15 + 60)
    "Mixed water temperature setpoint"
    annotation (Placement(transformation(extent={{-40,0},{-20,20}})));
  Buildings.Fluid.Storage.StratifiedEnhanced tan2(redeclare package Medium =
        Medium,
    m_flow_nominal=m_flow_nominal,
    hTan=2,
    VTan=2,
    dIns=0.07)
    annotation (Placement(transformation(extent={{100,-80},{120,-60}})));
  Buildings.HeatTransfer.Sources.PrescribedHeatFlow preHea
    "Prescribed heat flow"
    annotation (Placement(transformation(extent={{120,-30},{140,-10}})));
  Modelica.Blocks.Logical.OnOffController onOffConEle(bandwidth=0.5)
    "Electric heater on off controller"
    annotation (Placement(transformation(extent={{80,20},{100,40}})));
  Modelica.Blocks.Math.BooleanToReal booToRea "Boolean signal to real"
    annotation (Placement(transformation(extent={{120,20},{140,40}})));
  Modelica.Blocks.Math.Gain nomElePow(k=tanEleHeaPow_nominal)
                                      "Nominal electricity power"
    annotation (Placement(transformation(extent={{92,-30},{112,-10}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TTan2
    "Temperature in the tank water that surrounds the heat exchanger"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=0,
        origin={90,-46})));
  Buildings.Controls.OBC.CDL.Continuous.Sources.Constant TTanSet2(k=273.15 + 60)
    "Tank water temperature setpoint"
    annotation (Placement(transformation(extent={{42,26},{62,46}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive loa(mode=
        Buildings.Electrical.Types.Load.VariableZ_P_input)
    annotation (Placement(transformation(extent={{-38,80},{-58,100}})));
  replaceable
    Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n           terminal
    constrainedby Buildings.Electrical.AC.OnePhase.Interfaces.Terminal_n
    annotation (Placement(transformation(extent={{-8,92},{12,112}}),
        iconTransformation(extent={{-10,-108},{10,-88}})));
  Modelica.Blocks.Math.Gain powCon(k=-1)
    "Consumed power (negative for cosumption, positive for generation)"
    annotation (Placement(transformation(extent={{-88,80},{-68,100}})));
  Buildings.Controls.OBC.CDL.Continuous.MultiSum totPow(nin=2) "Total power"
    annotation (Placement(transformation(extent={{-120,80},{-100,100}})));
  Buildings.Electrical.AC.OnePhase.Sensors.GeneralizedSensor met "Power meter"
    annotation (Placement(transformation(extent={{-14,80},{-34,100}})));
  System.Baseclasss.ControlBus infBus "Information bus" annotation (Placement(
        transformation(extent={{90,70},{110,90}}), iconTransformation(extent={{
            90,70},{110,90}})));
equation
  connect(solCol.port_b, T2.port_a) annotation (Line(
      points={{-20,48},{12,48}},
      color={0,127,255},
      thickness=0.5));
  connect(T1.port_b, solCol.port_a) annotation (Line(
      points={{-50,48},{-40,48}},
      color={0,127,255},
      thickness=0.5));
  connect(weaDat.weaBus,solCol. weaBus) annotation (Line(
      points={{-140,90},{-140,64},{-40,64},{-40,57.6}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(weaDat.weaBus, pumCon.weaBus) annotation (Line(
      points={{-140,90},{-140,64},{-134,64},{-134,50.2}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(rooT.port, tan.heaPorTop)                  annotation (Line(
      points={{-98,-80},{-4,-80},{-4,-32},{-60,-32},{-60,-42.6}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(rooT.port, tan.heaPorSid)                  annotation (Line(
      points={{-98,-80},{-56.4,-80},{-56.4,-50}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(pumCon.y, gain.u) annotation (Line(
      points={{-140,28.2},{-140,21.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain.y, pum.m_flow_in) annotation (Line(
      points={{-140,3.2},{-140,6.66134e-16},{-112,6.66134e-16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pum.port_b, T1.port_a) annotation (Line(
      points={{-100,10},{-100,48},{-70,48}},
      color={0,127,255},
      thickness=0.5));
  connect(pum.port_a, exp.port_a) annotation (Line(
      points={{-100,-10},{-100,-58},{-130,-58}},
      color={0,127,255},
      thickness=0.5));
  connect(exp.port_a, tan.portHex_b) annotation (Line(
      points={{-130,-58},{-72,-58}},
      color={0,127,255},
      thickness=0.5));
  connect(T2.port_b, tan.portHex_a) annotation (Line(
      points={{32,48},{40,48},{40,-20},{-84,-20},{-84,-53.8},{-72,-53.8}},
      color={0,127,255},
      thickness=0.5));
  connect(bou.ports[1], tan.port_a) annotation (Line(
      points={{-120,-20},{-88,-20},{-88,-50},{-72,-50}},
      color={0,127,255},
      thickness=0.5));
  connect(tan.heaPorVol[3], TTan.port) annotation (Line(
      points={{-62,-50.225},{-62,-14},{-60,-14},{-60,10}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(TTan.T, pumCon.TIn) annotation (Line(
      points={{-80,10},{-88,10},{-88,60},{-144,60},{-144,52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tan.port_b, T3.port_a)
    annotation (Line(points={{-52,-50},{-40,-50}},
                                                color={0,127,255},
      thickness=0.5));
  connect(tan.port_a, val.port_3) annotation (Line(points={{-72,-50},{-80,-50},{
          -80,-72},{20,-72},{20,-60}}, color={0,127,255},
      thickness=0.5));
  connect(T3.port_b, val.port_1)
    annotation (Line(points={{-20,-50},{10,-50}},color={0,127,255},
      thickness=0.5));
  connect(val.port_2, T4.port_a)
    annotation (Line(points={{30,-50},{40,-50},{40,-70},{50,-70}},
                                                          color={0,127,255},
      thickness=0.5));
  connect(conPID.y, val.y)
    annotation (Line(points={{11,10},{20,10},{20,-38}}, color={0,0,127}));
  connect(T4.T, conPID.u_m) annotation (Line(points={{60,-59},{60,-12},{0,-12},{
          0,-2}},   color={0,0,127}));
  connect(TMixSet.y, conPID.u_s)
    annotation (Line(points={{-19,10},{-12,10}},
                                              color={0,0,127}));
  connect(T4.port_b, tan2.port_a)
    annotation (Line(points={{70,-70},{100,-70}}, color={0,127,255},
      thickness=0.5));
  connect(tan2.port_b, con.ports[1])
    annotation (Line(points={{120,-70},{158,-70}}, color={0,127,255},
      thickness=0.5));
  connect(preHea.port, tan2.heaPorVol[2]) annotation (Line(points={{140,-20},{150,
          -20},{150,-46},{110,-46},{110,-70}}, color={191,0,0}));
  connect(onOffConEle.y, booToRea.u)
    annotation (Line(points={{101,30},{118,30}}, color={255,0,255}));
  connect(nomElePow.y, preHea.Q_flow)
    annotation (Line(points={{113,-20},{120,-20}}, color={0,0,127}));
  connect(booToRea.y, nomElePow.u) annotation (Line(points={{141,30},{144,30},{144,
          8},{80,8},{80,-20},{90,-20}}, color={0,0,127}));
  connect(TTan2.port, tan2.heaPorVol[2])
    annotation (Line(points={{100,-46},{110,-46},{110,-70}}, color={191,0,0}));
  connect(TTan2.T, onOffConEle.u) annotation (Line(points={{80,-46},{72,-46},{72,
          24},{78,24}}, color={0,0,127}));
  connect(TTanSet2.y, onOffConEle.reference)
    annotation (Line(points={{63,36},{78,36}}, color={0,0,127}));
  connect(loa.Pow,powCon. y)
    annotation (Line(points={{-58,90},{-67,90}},     color={0,0,127}));
  connect(totPow.y, powCon.u)
    annotation (Line(points={{-98.3,90},{-90,90}}, color={0,0,127}));
  connect(pum.P, totPow.u[1]) annotation (Line(
      points={{-109,11},{-109,64},{-128,64},{-128,93.5},{-122,93.5}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(nomElePow.y, totPow.u[2]) annotation (Line(
      points={{113,-20},{116,-20},{116,0},{148,0},{148,66},{-126,66},{-126,86.5},
          {-122,86.5}},
      color={0,0,127},
      pattern=LinePattern.Dash));
  connect(loa.terminal, met.terminal_p)
    annotation (Line(points={{-38,90},{-34,90}}, color={0,120,120}));
  connect(met.terminal_n, terminal)
    annotation (Line(points={{-14,90},{2,90},{2,102}}, color={0,120,120}));
  connect(met.S, infBus.domHotWatAppPow) annotation (Line(points={{-18,81},{-18,
          80},{100,80}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  annotation ( __Dymola_Commands,
        experiment(Tolerance=1e-6, StopTime=86400.0),
        Documentation(info="<html>
          <p>
            This example shows how several different models can be combined to create
            an entire solar water heating system. The
            <a href=\"modelica://Buildings.Fluid.Storage.StratifiedEnhancedInternalHex\">
            Buildings.Fluid.Storage.StratifiedEnhancedInternalHex</a> (tan) model is
            used to represent the tank filled with hot water. A loop, powered by a pump
            (<a href=\"modelica://Buildings.Fluid.Movers.FlowControlled_m_flow\">
            Buildings.Fluid.Movers.FlowControlled_m_flow</a>, pum), passes the water
            through an expansion tank
            (<a href=\"modelica://Buildings.Fluid.Storage.ExpansionVessel\">
            Buildings.Fluid.Storage.ExpansionVessel</a>, exp), a temperature sensor
            (<a href=\"modelica://Buildings.Fluid.Sensors.TemperatureTwoPort\">
            Buildings.Fluid.Sensors.TemperatureTwoPort</a>, TIn), the solar collector
            (<a href=\"modelica://Buildings.Fluid.SolarCollectors.ASHRAE93\">
            Buildings.Fluid.SolarCollectors.ASHRAE93,</a> solCol) and a second temperature
            sensor
            (<a href=\"modelica://Buildings.Fluid.Sensors.TemperatureTwoPort\">
            Buildings.Fluid.Sensors.TemperatureTwoPort</a>, TOut) before re-entering the
            tank.
          </p>
          <p>
            The solar collector is connected to the weather model
            (<a href=\"modelica://Buildings.BoundaryConditions.WeatherData.ReaderTMY3\">
            Buildings.BoundaryConditions.WeatherData.ReaderTMY3</a>, weaDat) which passes
            information for the San Francisco, CA, USA climate. This information is used to
            identify both the heat gain in the water from the sun and the heat loss to the
            ambient conditions.
          </p>
          <p>
            The flow rate through the pump is controlled by a solar pump controller model
            (<a href=\"modelica://Buildings.Fluid.SolarCollectors.Controls.SolarPumpController\">
            Buildings.Fluid.SolarCollectors.Controls.SolarPumpController</a>, pumCon) and a
            gain model. The controller outputs a binary on (1) / off (0) signal. The on/off
            signal is passed through the gain model, multiplying by 0.04, to represent a
            flow rate of 0.04 kg/s when the pump is active.
          </p>
          <p>
            The heat ports for the tank are connected to an ambient temperature of 20
            degrees C representing the temperature of the room the tank is stored in.
          </p>
          <p>
            bou1 (<a href=\"modelica://Buildings.Fluid.Sources.MassFlowSource_T\">
            Buildings.Fluid.Sources.MassFlowSource_T)</a> provides a constant mass flow
            rate for a hot water draw while bou
            (<a href=\"modelica://Buildings.Fluid.Sources.Boundary_pT\">
            Buildings.Fluid.Sources.Boundary_pT)</a> provides a boundary
            condition for the outlet of the draw.
          </p>
      </html>",
revisions="<html>
<ul>
<li>
December 22, 2014 by Michael Wetter:<br/>
Removed <code>Modelica.Fluid.System</code>
to address issue
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/311\">#311</a>.
</li>
<li>
April 18, 2014, by Michael Wetter:<br/>
Updated model to use the revised tank and increased the tank height.
</li>
<li>
March 25, 2014, by Michael Wetter:<br/>
Updated model with new expansion vessel.
</li>
<li>
March 27, 2013 by Peter Grant:<br/>
First implementation
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-160,-100},{180,100}})),
    Icon(graphics={Bitmap(extent={{-100,-100},{100,100}},
    fileName="modelica://GreenVillage/Resources/Images/Subsystems/DomesticHotWater/DomesticHotWater.gif"),
              Text(extent={{-160,114},{140,154}},  lineColor={0,0,255},textString="%name")}));
end DHWLoop;
