# -*- coding: utf-8 -*-
"""
Scripts for calibrating chiller model as an example for using calibrateModelica package
"""

import calibrateModelica as cm
import os

## Prepare the data
name="GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.AC.Examples.Calibration.PVGroupSC"
path="C:\Users\Danlin\Documents\Bitbucket\hgv\GreenVillage"
model_type=[]
fformat=[]

os.chdir(path)

model=cm.preprocess.model(name=name, path=path,model_type=model_type,fformat=fformat)

pv=cm.preprocess.data(model,[],[],csv=None)

"""
Before optimization using Genopt, here are several steps need to follow:
    1. determine design variables, and write a template in txt file for Genopt
    2. specify the configuration in Genopt configuration file, such as optWinXP.ini in windows OS
    
"""


# translate the model first
fileList=[path+"/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupSC/designVariables.txt"]
s=cm.calibrate.translateModelica(pv,fileList)


# Optimize the translated model
startTime=0
endTime=24*3600*30
s.setSolver="dassl"
optWinXP='/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupSC/genoptCommand/optWinXP.ini'
cm.calibrate.optimizeModelica(s,startTime,endTime,optWinXP)

# Read the optimization result 
fr=open(path+"/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupSC/genoptCommand/OutputListingAll.txt",'r')
r=fr.readlines()
fr.close()
 
# this is the optimal result 
optResult=r[len(r)-1].split()
 
# Rewrite the designVariable.txt with optimum design variable values
f=open(path+"/Resources/Input/Subsystems/RenewableEnergy/PV/PhysicalModel/AC/Examples/Calibration/PVGroupSC/designVariables.txt",'w')
f.writelines('eff = '+str(optResult[5])+ "\n")      
f.close()