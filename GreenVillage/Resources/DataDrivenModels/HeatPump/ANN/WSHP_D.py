#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 31 20:44:05 2018

@author: yangyangfu
"""
from keras.models import load_model
import numpy as np
import os
from sklearn.externals import joblib

def model(inputs):
    '''
    	Inputs: List
    		[0]: Real, global solar irradiance
    		[1]: Real, outdoor air temperature (oC)
    		[2]: Integer,number of hour in a day [0,23]
        [3]: Integer,number of weekday in a week [0,6],representing Monday to Sunday
        [4]: Integer,index of holiday [0,1], 0 means no, and 1 means yes
        [5]: Integer,index of peak season [0,1], o means no, and 1 means yes
        [6]~[29]: Real, previous 1~24 hourly power (average over one hour) 
    Output: numpy array, (1,)
        [0]: Predicted Power consumption (W)
    '''
    	
    path = os.path.abspath(os.path.dirname(__file__))
    pipeModel = 'dnnWSHPPipe_D.pkl'
    dnnModel = 'dnnWSHP_D.h5'
    print path
    print pipeModel
    print path+'/'+pipeModel

    # load model first
    with open(path+'/'+pipeModel,'rb') as fo:
        pipe = joblib.load(fo) 
    
    pipe.best_estimator_.named_steps['reg'].model = load_model(path+'/'+dnnModel)

 
    
    # Predict the output
    # change inputs from list to numpy array
    inp = np.array(inputs).reshape(1,len(inputs))
    # if all inputs are 0 or very small
    y=float(pipe.predict(inp)[0,0])
    	
    return max(0,y)


