def ANN(inputs):
    '''
    	Inputs: List
    		[0]: Diffusive irradiance
    		[1]: Direct irradiance
    		[2]: Global irradiance
    '''
    from sklearn.externals import joblib
    import os
    import numpy as np
    # GET Current file path
    path = os.path.abspath(os.path.dirname(__file__))
    modelName = 'annDaytime_NC.pkl'

    ann = joblib.load(path + '/'+ modelName)
    
    # Predict the output
    # change inputs from list to numpy array
    inp = np.array(inputs).reshape(1,3)
    # if all inputs are 0 or very small
    if np.sum(inp)<=5:
    	y = 0
    else:
    	y=max(0.,ann.predict(inp)[0])
    	
    return y
