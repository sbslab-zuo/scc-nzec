#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 11:14:31 2018

@author: yangyangfu
calculate holidays in US
"""

def holidayUS(dateList):
    """
    Input: date, list
        dateList[0]: year, eg. 2015;
        dateList[1]: month,eg. 1
        dateList[2]: day, eg. 20
    """
    
    from datetime import date
    import holidays
    
    us_holidays = holidays.UnitedStates()
    
    hol = float(date(int(dateList[0]),int(dateList[1]),int(dateList[2])) in us_holidays)
    
    return hol
