#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 11:14:31 2018

@author: yangyangfu
calculate holidays in US
"""

def unixTime(timeList):
    """
    Input: date, list
        timeList[0]: year, eg. 2015;
        timeList[1]: month,eg. 1
        timeList[2]: day, eg. 20
        timeList[3]: hour, eg. 20
        timeList[4]: minute, eg. 20
    """
    
    from datetime import datetime
    from pytz import timezone
    import pytz
    import time

    year = int(timeList[0])
    month = int(timeList[1])
    day = int(timeList[2])
    hour = int(timeList[3])
    minute = int(timeList[4]) if timeList[4] < 60 else 59

    #eastern = timezone('US/Eastern')
    localTime = timezone('US/Pacific')
    utc = pytz.utc
    datTim = datetime(year,month,day,hour,minute,tzinfo=utc) #utc time 

    
    # change it to local time 
    local_datTim = datTim.astimezone(localTime)

    ts = time.mktime(local_datTim.timetuple())
    
    return ts
