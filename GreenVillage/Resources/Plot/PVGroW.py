# -*- coding: utf-8 -*-
"""
Created on Sun Dec 31 16:27:19 2017

@author: Danlin
"""

# This code is used to plot power data generated from different solar input data
from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
import numpy as np

r=Reader('PVGroW.mat','dymola') 
# Solar input data from NREL
(t1,y1)=r.values('HDifHorNREL.y[1]') # Input data 
(t2,y2)=r.values('HGloHorNREL.y[1]')
(t3,y3)=r.values('HDirNorNREL.y[1]')
# Solar input data from SBS lab local sensors
(t4,y4)=r.values('HDifHorSBS.y[1]') # Input data 
(t5,y5)=r.values('HGloHorSBS.y[1]')
(t6,y6)=r.values('HDirNorSBS.y[1]')
# PV generation data 
(t7,y7)=r.values('PVGroW.P[1]')
(t8,y8)=r.values('PVGroW1.P[1]')
(t9,y9)=r.values('powGenPVW.y[1]') #from SBS lab local sensors

P1=r.integral('PVGroW.P[1]')
P2=r.integral('PVGroW1.P[1]')
P3=r.integral('powGenPVW.y[1]')

ax3=plt.subplot(3,1,3)

ax1=plt.subplot(3,1,1,sharex=ax3)
#ax1.plot(t1,y1,label='NSRDB')
ax1.plot(t4,y4,label='SBS')
plt.legend(loc=1)
ax1.set_ylabel('Diffuse horizontal irradiation (DHI) [W/$m^2$]')
plt.setp(ax1.get_xticklabels(), visible=False)

ax2=plt.subplot(3,1,2,sharex=ax3)
#ax2.plot(t2,y2,label='NSRDB')
ax2.plot(t5,y5,label='SBS')
plt.legend(loc=1)
ax2.set_ylabel('Global horizontal irradiation (GHI) [W/$m^2$]')
plt.setp(ax2.get_xticklabels(), visible=False)

ax3=plt.subplot(3,1,3)
#ax3.plot(t3,y3,label='NSRDB')
ax3.plot(t6,y6,label='SBS')
plt.legend(loc=1)
ax3.set_ylabel('Direct normal irradiation (DNI) [W/$m^2$]')
ax3.set_xlabel('Time (day)')
new_ticks=np.linspace(min(t3),max(t3),31)
plt.xticks(new_ticks,np.arange(0,31))
Fig = plt.gcf()
Fig.set_size_inches(15, 15)
plt.savefig('solarInput.png', format='png', dpi=1000)
plt.show()

ax4=plt.subplot(1,1,1)
#ax4.plot(t7,y7*0.8/1000,label='Model data (NSRDB input)')
ax4.plot(t8,y8*0.8/1000,label='Model data (SBS input)')
ax4.plot(t9,y9/1000,label='Measure data')
plt.legend(loc=2)
ax4.set_ylabel('Pwer generation of PV group W [kW]')
ax4.set_xlabel('Time (day) Jun 2017')
new_ticks=np.linspace(min(t3),max(t3),31)
plt.xticks(new_ticks,np.arange(0,31))
Fig = plt.gcf()
Fig.set_size_inches(15, 4)
plt.savefig('PVGeneration.png', format='png', dpi=1000)
plt.show()