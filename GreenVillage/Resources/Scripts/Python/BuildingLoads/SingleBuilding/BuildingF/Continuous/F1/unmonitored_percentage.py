# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 12:56:37 2018

@author: Danlin
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


cha_name='Unmonitored'
cha2_name='CH1-Bldg F1'
cha3_name='CH3-PV Array F1'
month=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
bui_name=' [F1]'
# Read data
df = pd.read_csv('monthdata.csv')
ch11=df[cha_name]
ch2=df[cha2_name]
ch6=df[cha3_name]
ch3=[]
ch4=[]
ch5=[]
ch1=[]

for i in range (len(ch11)):
    if ch11[i]>-1000:
        ch1.append(ch11[i])
    else:
        ch1.append(500)
        
        
for i in range(len(ch2)):
    ch3.append(ch1[i]/(ch2[i]-ch6[i])*100)
    
for i in range (len(ch3)):
    if i >=(273+31)*24:
        ch4.append(ch3[i])
        ch5.append(ch1[i])
        

   
fig4=plt.figure()
fig4.suptitle(cha_name+bui_name,fontsize=14, fontweight='bold')
ax9=fig4.add_subplot(1,1,1)
fig4.subplots_adjust(top=0.90)
ax9.plot(ch1,linestyle='--',color='blue')
ax9.set_ylabel('Hourly Unmonitored Power [Wh]',color='blue',fontweight='bold')
ax9.set_xlabel('Time [hr]\nJan.2017~ Dec.2017',fontweight='bold')
ax10=ax9.twinx()
ax10.scatter(np.linspace(0,len(ch1)+1,num=8760),ch3,color='red',s=0.5)
ax10.set_ylabel('Percentage of \nHourly Unmonitored Power [%]',color='red',fontweight='bold')
plt.savefig(cha_name[:4]+'percentage.png',dpi=300) 
plt.savefig(cha_name[:4]+'percentage.svg',dpi=300) 
plt.show()

fig5=plt.figure()
fig5.suptitle(cha_name+bui_name,fontsize=14, fontweight='bold')
ax11=fig5.add_subplot(1,1,1)
fig5.subplots_adjust(top=0.90)
ax11.scatter(np.linspace(0,len(ch4)+1,num=len(ch4)),ch4,color='blue',s=0.5)
ax11.set_ylabel('Percentage of \nHourly Unmonitored Power [%]')

fig6=plt.figure()
fig6.suptitle(cha_name+bui_name,fontsize=14, fontweight='bold')
ax12=fig6.add_subplot(1,1,1)
fig6.subplots_adjust(top=0.90)
ax12.plot(np.linspace(0+304*24,len(ch4)+1+304*24,num=len(ch4)),ch5,linestyle='--',color='blue')
ax12.set_ylabel('Hourly Unmonitored Power [Wh]',color='blue',fontweight='bold')
ax12.set_xlabel('Time [hr]\nNov.2017~ Dec.2017',fontweight='bold')
ax13=ax12.twinx()
ax13.scatter(np.linspace(0+304*24,len(ch4)+1+304*24,num=len(ch4)),ch4,color='red',s=2)
ax13.set_ylabel('Percentage of \nHourly Unmonitored Power [%]',color='red',fontweight='bold')