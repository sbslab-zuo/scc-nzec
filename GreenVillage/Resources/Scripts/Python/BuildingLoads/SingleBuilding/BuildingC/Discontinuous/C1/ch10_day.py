# -*- coding: utf-8 -*-
"""
Created on Sun Jan 28 20:43:03 2018

@author: Danlin
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df1 = pd.read_csv('monthdata.csv')

d=np.arange(0*24,365*24,1) # time
d1=d.reshape(365,24)

cha_name='CH10-Plug: Undercounter'
a0=df1[cha_name]/1000
a01=a0.values.reshape(365,24)
a02=np.transpose(a01)
'''
ax1= plt.subplot(1,1,1)
ax1.plot(a02,lw=0.75,linestyle='-')
Fig = plt.gcf()
Fig.set_size_inches(10, 4)
ax1.set_xticks(np.arange(d1[0][0], d1[0][23], 2))
xlab=['0:00','2:00','4:00','6:00','8:00','10:00',
      '12:00','14:00','16:00','18:00','20:00',
      '22:00']
ax1.set_xticklabels(xlab)
ax1.set_ylabel('Electrical Consumption [kWh]\nWSHP')
ax1.set_xlabel('Time [2017]')
plt.savefig('2017_CH5-AC WSHP1.png',dpi=300) 
plt.savefig('2017_CH5-AC WSHP1.svg',dpi=300) 
plt.show()
'''
row=53
col=7

myx=np.arange(24)

fig,axes=plt.subplots(nrows=row, ncols=col)
for ax0, row in zip (axes.flatten(),a01):
    ax0.plot(myx,row,color='blue')

    ax0.set_xticks(np.arange(myx[0], myx[23], 6))
    xlab=['0:00','6:00','12:00',
      '18:00','24:00']
    ax0.set_xticklabels(xlab)
    ax0.set_ylim(0,np.max(a0))

    Fig = plt.gcf()
    Fig.set_size_inches(20, 100)

ax0.set_ylabel('Electrical Consumption [kWh]\nWSHP')
ax0.set_xlabel('Time [2017]')

for i in range(365,371):
    axes.flatten()[i].axis("off")

    
plt.tight_layout()
#plt.ylabel('Electrical Consumption [kW]\nFront Fridge')
plt.savefig(cha_name[:4]+'365.png',dpi=300) 
plt.savefig(cha_name[:4]+'365.svg',dpi=300) 
#plt.savefig('2017eachday_CH6-Range_scatter.png',dpi=300) 
#plt.savefig('2017each_CH6-Range_scatter.svg',dpi=300)
plt.show()

print ('Total electrical consumption:')
print (np.sum (a0))