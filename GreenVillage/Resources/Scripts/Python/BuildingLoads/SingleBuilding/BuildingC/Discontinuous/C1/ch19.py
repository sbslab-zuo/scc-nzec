# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 16:01:01 2018

@author: Danlin
"""
'''
The unit for the data is W or Wh. It will be convert to kWh when plotting
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from termcolor import colored
from mean_day_con import mean_day_con
from mean_hour_con import mean_hour_con
from models import models

cha_name='CH19-Lights: Ceiling Track Center'
month=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

# Read data
df = pd.read_csv('monthdata.csv')
ch1=df[cha_name]
gloMax=ch1.max()



'''
*************** This part is for the average daily consumption to indicate************************ 
whether the consumption is related to tourist season
'''
day_consump,max,min,diff=mean_day_con(ch1)

if diff > 3000:
    print colored('\nWhether '+cha_name+' should consider the impact of tourist season: Y\n','red')
else:
    print('\nWhether '+cha_name+' should consider the impact of tourist season: N\n')
    
plt.plot(day_consump/1000,linestyle='-',marker='D',color='blue')
plt.xticks(np.arange(0,12),month)
plt.ylabel('Average Daily Consumption [kWh]')
plt.xlabel('Year 2017')
plt.title(cha_name,fontweight='bold')
plt.savefig(cha_name[:4]+'daily.png',dpi=300)
plt.savefig(cha_name[:4]+'daily.svg',dpi=300) 

'''
****************This part is for (average) hourly consumption*********************
'''
sun,mon,tue,wed,thu,fri,sat,sun_hr,mon_hr,tue_hr,wed_hr,thu_hr,fri_hr,sat_hr=mean_hour_con(ch1)

everyday_hr=(mon_hr+tue_hr+wed_hr+thu_hr+fri_hr+sat_hr+sun_hr)/7

data=np.array([everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr,everyday_hr]).transpose()
pattern=models(data)
np.savetxt('C1_'+cha_name[0:4]+'.txt',pattern,header='#1\ndouble table(48,'+str(len(pattern[0]))+')',comments='')

# Figure of the data of hourly consumption of each day of the week
fig=plt.figure()
fig.suptitle(cha_name,fontsize=14, fontweight='bold')

ax0=fig.add_subplot(7,1,1)  
fig.subplots_adjust(top=0.96)  
ax0.plot(sun/1000) 
ax0.text(0.9,0.9,'Sunday',ha='center',va='center',transform=ax0.transAxes,fontsize=10,fontweight='bold')
plt.ylim(0,gloMax/1000)
ax0.set_xlabel('Time [hr]')
ax0.set_ylabel('Hourly electrical consumption [kWh]')

ax1=fig.add_subplot(7,1,2) 
ax1.plot(mon/1000)
ax1.text(0.9,-0.3,'Monday',ha='center',va='center',transform=ax0.transAxes,fontsize=10,fontweight='bold')
plt.ylim(0,gloMax/1000)

ax2=fig.add_subplot(7,1,3) 
ax2.plot(tue/1000)
ax2.text(0.9,-1.5,'Tuesday',ha='center',va='center',transform=ax0.transAxes,fontsize=10,fontweight='bold') 
plt.ylim(0,gloMax/1000)

ax3=fig.add_subplot(7,1,4) 
ax3.plot(wed/1000)
ax3.text(0.9,-2.7,'Wednesday',ha='center',va='center',transform=ax0.transAxes,fontsize=10,fontweight='bold')  
plt.ylim(0,gloMax/1000)

ax4=fig.add_subplot(7,1,5) 
ax4.plot(thu/1000)
ax4.text(0.9,-3.9,'Thursday',ha='center',va='center',transform=ax0.transAxes,fontsize=10,fontweight='bold')  
plt.ylim(0,gloMax/1000)

ax5=fig.add_subplot(7,1,6) 
ax5.plot(fri/1000)
ax5.text(0.9,-5.1,'Friday',ha='center',va='center',transform=ax0.transAxes,fontsize=10,fontweight='bold')  
plt.ylim(0,gloMax/1000)

ax6=fig.add_subplot(7,1,7) 
ax6.plot(sat/1000)
ax6.text(0.9,-6.3,'Saturday',ha='center',va='center',transform=ax0.transAxes,fontsize=10,fontweight='bold')  
plt.ylim(0,gloMax/1000)

Fig = plt.gcf()
Fig.set_size_inches(10,25)
plt.savefig(cha_name[:4]+'hourly.png',dpi=300) 
plt.savefig(cha_name[:4]+'hourly.svg',dpi=300) 
plt.show()

# Figure of the data of average hourly consumption
fig1=plt.figure()
fig1.suptitle(cha_name,fontsize=14, fontweight='bold')
ax7=fig1.add_subplot(1,1,1)  
fig1.subplots_adjust(top=0.90)  
ax7.plot(sun_hr,label='Sunday') 
ax7.plot(mon_hr,label='Monday') 
ax7.plot(tue_hr,label='Tuesday') 
ax7.plot(wed_hr,label='Wednesday')
ax7.plot(thu_hr,label='Thursday')
ax7.plot(fri_hr,label='Friday')
ax7.plot(sat_hr,label='Saturday')
ax7.legend()
plt.ylim(0,gloMax)
ax7.set_xlabel('Time [hr]')
ax7.set_ylabel('Average Electrical Demand [W]')
Fig = plt.gcf()
Fig.set_size_inches(10,4)
plt.savefig(cha_name[:4]+'hourly_mean.png',dpi=300) 
plt.savefig(cha_name[:4]+'hourly_mean.svg',dpi=300) 
plt.show()

# Figure of the data of average hourly consumption
fig2=plt.figure()
fig2.suptitle(cha_name,fontsize=14, fontweight='bold')
ax72=fig2.add_subplot(1,1,1)  
fig2.subplots_adjust(top=0.90)  
ax72.plot(sun_hr,label='Sunday') 
ax72.plot(mon_hr,label='Monday') 
ax72.plot(tue_hr,label='Tuesday') 
ax72.plot(wed_hr,label='Wednesday')
ax72.plot(thu_hr,label='Thursday')
ax72.plot(fri_hr,label='Friday')
ax72.plot(sat_hr,label='Saturday')
ax72.legend()
#plt.ylim(0,gloMax)
ax72.set_xlabel('Time [hr]')
ax72.set_ylabel('Average Electrical Demand [W]')
Fig = plt.gcf()
Fig.set_size_inches(10,4)
plt.savefig(cha_name[:4]+'hourly_mean_1.png',dpi=300) 
plt.savefig(cha_name[:4]+'hourly_mean_1.svg',dpi=300) 
plt.show()

# Figure 
fig3=plt.figure()
fig3.suptitle(cha_name,fontsize=14, fontweight='bold')
ax8=fig3.add_subplot(1,1,1)  
fig3.subplots_adjust(top=0.90)  
ax8.plot(everyday_hr,label='Everyday',linestyle='-',lw=3,color='red')
#ax8.plot(workday_hr,label='Workday',linestyle='-',lw=3,color='red')
#ax8.plot(tue_wed_fri_hr,label='Tue/Wed/Fri',linestyle='-',lw=3,color='blue')
#ax8.plot(weekend_hr,label='Weekend',linestyle='-',lw=3,color='orange')
#ax8.plot(sun_hr,label='Sunday',linestyle='-',lw=3,color='blue')
#ax8.plot(sat_hr,label='Saturday',linestyle='-',lw=3,color='orange')
ax8.plot(mon_hr,label='Monday',linestyle='--',lw=0.5) 
ax8.plot(tue_hr,label='Tuesday',linestyle='--',lw=0.5) 
ax8.plot(wed_hr,label='Wednesday',linestyle='--',lw=0.5)
ax8.plot(thu_hr,label='Thursday',linestyle='--',lw=0.5)
ax8.plot(fri_hr,label='Friday',linestyle='--',lw=0.5)
ax8.plot(sat_hr,label='Saturday',linestyle='--',lw=0.5)
ax8.plot(sun_hr,label='Sunday',linestyle='-',lw=0.5)

ax8.legend()
ax8.set_xlabel('Time [hr]')
ax8.set_ylabel('Electrical demand [W]')
Fig = plt.gcf()
Fig.set_size_inches(10,4)
plt.savefig(cha_name[:4]+'pattern.png',dpi=300) 
plt.savefig(cha_name[:4]+'pattern.svg',dpi=300) 
plt.show()