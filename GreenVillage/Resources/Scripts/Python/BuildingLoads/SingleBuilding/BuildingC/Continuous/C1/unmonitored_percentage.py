# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 12:56:37 2018

@author: Danlin
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


cha_name='Unmonitored'
cha2_name='CH1-Rosedale C1'

# Read data
df = pd.read_csv('monthdata10_12.csv')
ch1=df[cha_name]
ch2=df[cha2_name]
ch3=[]
ch4=[]
for i in range(len(ch2)):
    ch3.append(ch1[i]/ch2[i]*100)
    ch4.append(i+6552)
    
fig4=plt.figure()
fig4.suptitle(cha_name+' [C1]',fontsize=14, fontweight='bold')
ax9=fig4.add_subplot(1,1,1)
fig4.subplots_adjust(top=0.90)
ax9.plot(ch4,ch1,linestyle='--',color='blue')
ax9.set_ylabel('Hourly Unmonitored Power [kWh]',color='blue',fontweight='bold')
ax9.set_xlabel('Time [hr]\n2017.10~2017.12')
ax10=ax9.twinx()
ax10.scatter(ch4,ch3,color='red',s=0.5)
ax10.set_ylabel('Percentage of \nHourly Unmonitored Power [%]',color='red',fontweight='bold')
plt.savefig(cha_name[:4]+'percentage.png',dpi=300) 
plt.savefig(cha_name[:4]+'percentage.svg',dpi=300) 
plt.show()