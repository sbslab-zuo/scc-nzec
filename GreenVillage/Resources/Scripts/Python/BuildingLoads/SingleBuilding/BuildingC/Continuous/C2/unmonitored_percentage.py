# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 12:56:37 2018

@author: Danlin
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


cha_name='Unmonitored'
cha2_name='CH1-C2'

# Read data
df = pd.read_csv('monthdata.csv')
ch1=df[cha_name]
ch2=df[cha2_name]
ch3=[]
for i in range(len(ch2)):
    ch3.append(ch1[i]/ch2[i]*100)
    
fig4=plt.figure()
fig4.suptitle(cha_name+' [C2]',fontsize=14, fontweight='bold')
ax9=fig4.add_subplot(1,1,1)
fig4.subplots_adjust(top=0.90)
ax9.plot(ch1,linestyle='--',color='blue')
ax9.set_ylabel('Hourly Unmonitored Power [kWh]',color='blue',fontweight='bold')
ax9.set_xlabel('Time [hr/2017]')
ax10=ax9.twinx()
ax10.scatter(np.linspace(0,len(ch1)+1,num=8760),ch3,color='red',s=0.5)
ax10.set_ylabel('Percentage of \nHourly Unmonitored Power [%]',color='red',fontweight='bold')
plt.savefig(cha_name[:4]+'percentage.png',dpi=300) 
plt.savefig(cha_name[:4]+'percentage.svg',dpi=300) 
plt.show()