# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 14:20:42 2018

@author: Danlin
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from termcolor import colored

df = pd.read_csv('monthdata.csv')
rows=len(df.columns)

ax=df.plot(subplots=True, layout=(rows,1))
plt.ylabel('Electrical Consumption [Wh]')

Fig = plt.gcf()
Fig.set_size_inches(10,50)
plt.savefig('all_channels.png',dpi=300) 
plt.savefig('all_channels.svg',dpi=300) 
plt.show()

cha1_name='Unmonitored'
cha2_name='CH1-Rosedale C1'

ch1=df[cha1_name]
ch2=df[cha2_name]

ch3=[]
ch4=[]
for i in range (len(ch1)):
    ch3.append(ch1[i]/ch2[i]*100)
    ch4.append(i)


ax1=plt.subplot(1,1,1)
ax1.plot(ch1/1000,color='blue',label='Unmonitored')
#ax1.scatter(ch4,ch2/1000,color='green',label='Total',s=1)
#plt.legend()
ax1.set_ylabel('Unmonitored Hourly Consumption [kWh]',fontweight='bold',color='blue')
ax1.set_xlabel('Time [hr]')
ax2=ax1.twinx()
ax2.scatter(ch4,ch3, color='red',s=1)
ax2.set_ylabel('Unmonitored Percentage [%]',fontweight='bold',color='red')
Fig = plt.gcf()
Fig.set_size_inches(10,4)