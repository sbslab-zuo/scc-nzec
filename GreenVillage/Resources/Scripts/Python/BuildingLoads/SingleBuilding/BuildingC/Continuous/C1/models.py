# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 09:34:49 2018

@author: Danlin
"""

import numpy as np

def models(data):
    
    time=np.linspace(0,24,num=25,endpoint=True)*3600
    time1=np.repeat(time,2)
    time2=np.delete(time1,[0,49])
    time3=time2.reshape(time2.shape[0],-1)
    
    model1=np.repeat(data,2,axis=0)
    model2=np.append(time3,model1,axis=1)
    
    return model2



