# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 14:50:31 2018

@author: Danlin
"""

# This function is used to calculate the average daily consumption of the certain channel
# Total 12 average daily consumptions will be calculated (January ~ December)
import numpy as np
def mean_day_con(data):
    
    a=31
    b=30
    c=28

# get the data of each month    
    jan=data[0:a*24]
    feb=data[a*24:(a+c)*24]
    mar=data[(a+c)*24:(2*a+c)*24]
    apr=data[(2*a+c)*24:(2*a+b+c)*24]
    may=data[(2*a+b+c)*24:(3*a+b+c)*24]
    jun=data[(3*a+b+c)*24:(3*a+2*b+c)*24]
    jul=data[(3*a+2*b+c)*24:(4*a+2*b+c)*24]
    aug=data[(4*a+2*b+c)*24:(5*a+2*b+c)*24]
    sep=data[(5*a+2*b+c)*24:(5*a+3*b+c)*24]
    oct=data[(5*a+3*b+c)*24:(6*a+3*b+c)*24]
    nov=data[(6*a+3*b+c)*24:(6*a+4*b+c)*24]
    dec=data[(6*a+4*b+c)*24:(7*a+4*b+c)*24]
    
    jan_ad=np.nanmean(jan)*24
    feb_ad=np.nanmean(feb)*24
    mar_ad=np.nanmean(mar)*24
    apr_ad=np.nanmean(apr)*24
    may_ad=np.nanmean(may)*24
    jun_ad=np.nanmean(jun)*24
    jul_ad=np.nanmean(jul)*24
    aug_ad=np.nanmean(aug)*24
    sep_ad=np.nanmean(sep)*24
    oct_ad=np.nanmean(oct)*24
    nov_ad=np.nanmean(nov)*24
    dec_ad=np.nanmean(dec)*24
    ddata=[jan_ad,feb_ad,mar_ad,apr_ad,may_ad,jun_ad,jul_ad,aug_ad,sep_ad,oct_ad,nov_ad,dec_ad]
    ddata=np.asarray(ddata)
    diff=max(ddata)-min(ddata)
    
    return ddata,max(ddata),min(ddata),diff


    

    
    
    
    
    