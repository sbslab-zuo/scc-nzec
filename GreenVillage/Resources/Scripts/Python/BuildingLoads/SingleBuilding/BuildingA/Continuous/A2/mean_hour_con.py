# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 13:42:22 2018

@author: Danlin
"""

import numpy as np
def mean_hour_con(data):
    
    a= len(data)/24
    data=data.values.reshape(a,24)
    
    sun=[]
    mon=[]
    tue=[]
    wed=[]
    thu=[]
    fri=[]
    sat=[]
    
    for i in range (len(data)):
        if i%7==0:
            sun.append(data[i])
        elif i%7==1:
            mon.append(data[i])
        elif i%7==2:
            tue.append(data[i])
        elif i%7==3:
            wed.append(data[i])
        elif i%7==4:
            thu.append(data[i])
        elif i%7==5:
            fri.append(data[i])
        else:
            sat.append(data[i])
            
    sun=np.asarray(sun)
    sun=np.transpose(sun)
    mon=np.asarray(mon)
    mon=np.transpose(mon)
    tue=np.asarray(tue)
    tue=np.transpose(tue)
    wed=np.asarray(wed)
    wed=np.transpose(wed)
    thu=np.asarray(thu)
    thu=np.transpose(thu)
    fri=np.asarray(fri)
    fri=np.transpose(fri)
    sat=np.asarray(sat)
    sat=np.transpose(sat)
    
    sun_hr=np.nanmean(sun, axis=1)
    mon_hr=np.nanmean(mon, axis=1)
    tue_hr=np.nanmean(tue, axis=1)
    wed_hr=np.nanmean(wed, axis=1)
    thu_hr=np.nanmean(thu, axis=1)
    fri_hr=np.nanmean(fri, axis=1)
    sat_hr=np.nanmean(sat, axis=1)
    
    return sun,mon,tue,wed,thu,fri,sat,sun_hr,mon_hr,tue_hr,wed_hr,thu_hr,fri_hr,sat_hr