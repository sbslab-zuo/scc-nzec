# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 14:20:42 2018

@author: Danlin
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from termcolor import colored

df = pd.read_csv('monthdata.csv')
rows=len(df.columns)

ax=df.plot(subplots=True, layout=(rows,1))
plt.ylabel('Electrical Consumption [Wh]')

Fig = plt.gcf()
Fig.set_size_inches(10,50)
plt.savefig('all_channels.png',dpi=300) 
plt.savefig('all_channels.svg',dpi=300) 
plt.show()