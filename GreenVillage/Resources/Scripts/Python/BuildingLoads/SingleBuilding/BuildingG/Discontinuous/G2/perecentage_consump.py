# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 14:44:20 2018

@author: Danlin
"""

import pandas as pd
import numpy as np


# Read data
df = pd.read_csv('monthdata.csv')

df1=df.sum(axis=0)
df2=df1[2:-3]
total=df2.sum(axis=0)

df3=df2.tolist()
perc=[]

data=np.zeros((len(df2),2))

for i in range(len(df3)):
    perc.append(df2[i]/total*100)
    data[i,0]=df3[i]
    data[i,1]=perc[i]
    
a=df2.to_frame()
a['Percentage [%]']=perc
a.columns=['Consumption [Wh]','Percentage [%]']
print a
