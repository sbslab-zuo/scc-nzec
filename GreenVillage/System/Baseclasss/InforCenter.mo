within GreenVillage.System.Baseclasss;
model InforCenter
  Modelica.Blocks.Sources.RealExpression PLoad_Total(y=buiLoa + heaPumPow)
    "Total power consumed by the loads"
    annotation (Placement(transformation(extent={{0,-6},{20,14}})));
  Modelica.Blocks.Interfaces.RealInput buiLoa "Load in buildings"
    annotation (Placement(transformation(extent={{-140,30},{-100,70}})));
  Modelica.Blocks.Interfaces.RealInput heaPumPow "Power load in heat pump"
    annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealInput renEneGen
    "Generation in renewable energy"
    annotation (Placement(transformation(extent={{-140,-80},{-100,-40}})));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-116,-110},{124,-140}},
          lineColor={0,0,0},
          textString="%name")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end InforCenter;
