within GreenVillage.System;
model HeatPumpHotWater
  "Heat pump system and domestic hot water system"
  extends Modelica.Icons.Example;

  replaceable package Medium = Buildings.Media.Water;

 parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump8 datHP8
    "Peformance data for heat pump 8";
 parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump9 datHP9
    "Peformance data for heat pump 9";

 parameter Modelica.SIunits.MassFlowRate mHeaPum_flow_nominal=
      datHP8.sta[datHP8.nSta].nomVal.mCon_flow_nominal+
      datHP9.sta[datHP9.nSta].nomVal.mCon_flow_nominal
    "Norminal flowrate of heat recovery system";

  Subsystems.HeatPump.WSHPPhysics.HPLoopWithHeatRecovery heaPum(datHP8=datHP8,
      datHP9=datHP9)
    "Heat pump system"
    annotation (Placement(transformation(extent={{-10,30},{10,50}})));
  Subsystems.DomesticHotWater.DHWLoopWithHeatRecovery domHotWat(
      mHeaPum_flow_nominal=mHeaPum_flow_nominal, redeclare package Medium =
        Medium)
    "Domestic hot water system"
    annotation (Placement(transformation(extent={{10,-18},{-10,-38}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid gri(f=60, V=480)
                                                               "Grid"
    annotation (Placement(transformation(extent={{-80,30},{-60,50}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSou(
    T_start(displayUnit="K"),
    redeclare package Medium = Medium,
    m_flow_nominal=mHeaPum_flow_nominal)
     "Temperature sensor"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-24,0})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSin(
    T_start(displayUnit="K"),
    redeclare package Medium = Medium,
    m_flow_nominal=mHeaPum_flow_nominal)
   "Temperature sensor"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=90,
        origin={20,0})));



equation
  connect(gri.terminal, heaPum.terminal) annotation (Line(points={{-70,30},{-70,
          20},{0,20},{0,30.2}}, color={0,120,120}));
  connect(domHotWat.port_b, TSou.port_a) annotation (Line(points={{-6,-18},{-24,
          -18},{-24,-10}}, color={0,127,255}));
  connect(TSou.port_b, heaPum.port_a)
    annotation (Line(points={{-24,10},{-24,26},{-8,26}}, color={0,127,255}));
  connect(domHotWat.port_a, TSin.port_b)
    annotation (Line(points={{6,-18},{20,-18},{20,-10}}, color={0,127,255}));
  connect(TSin.port_a, heaPum.port_b)
    annotation (Line(points={{20,10},{20,26},{8,26}}, color={0,127,255}));
  connect(gri.terminal, domHotWat.terminal) annotation (Line(points={{-70,30},{-70,
          20},{0,20},{0,-18.2}}, color={0,120,120}));
  annotation (__Dymola_Commands(file="modelica://GreenVillage/Resources/Scripts/Dymola/System/HeatPumpHotWater.mos"
        "Simulate and Plot"));
end HeatPumpHotWater;
