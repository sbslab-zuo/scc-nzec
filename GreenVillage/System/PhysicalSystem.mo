within GreenVillage.System;
model PhysicalSystem
  " In the example, all the models are physics-based models."

 replaceable package Medium = Buildings.Media.Water;

 parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump8 datHP8
    "Peformance data for heat pump 8";
 parameter GreenVillage.Subsystems.HeatPump.WSHPPhysics.Data.HeatPump9 datHP9
    "Peformance data for heat pump 9";

 parameter Modelica.SIunits.MassFlowRate mHeaRec_flow_nominal=
      datHP8.sta[datHP8.nSta].nomVal.mCon_flow_nominal+
      datHP9.sta[datHP9.nSta].nomVal.mCon_flow_nominal
    "Norminal flowrate of heat recovery system";

  GreenVillage.Subsystems.Grid.Grid gri(f=f, V=V_nominal)
                                        "Electrical grid model"
    annotation (Placement(transformation(extent={{0,40},{20,60}})));
  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.HGVPVs PV(V_nominal=
        V_nominal)
    "PV system"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  GreenVillage.Subsystems.BuildingLoads.Loads buiLoa "Building loads"
    annotation (Placement(transformation(extent={{-100,-20},{-80,0}})));
  Modelica.Blocks.Sources.CombiTimeTable DHI(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/System/difHorIrrSBS.txt"),
    tableName="table") "Direct horizontal irradiance"
    annotation (Placement(transformation(extent={{-140,80},{-120,100}})));

  Modelica.Blocks.Sources.CombiTimeTable GHI(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/System/gloHorIrrSBS.txt"),
    tableName="table")  "Horizontal global radiation"
    annotation (Placement(transformation(extent={{-140,50},{-120,70}})));

  Modelica.Blocks.Sources.CombiTimeTable DNI(
    tableOnFile=true,
    table=fill(
        0.0,
        0,
        2),
    fileName=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Input/System/dirNorIrrSBS.txt"),
    tableName="table")  "Direct normall radiation"
    annotation (Placement(transformation(extent={{-140,20},{-120,40}})));

  GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Weather.ReaderTMY3
    weaDat(
    computeWetBulbTemperature=false,
    filNam=ModelicaServices.ExternalReferences.loadResource("modelica://GreenVillage/Resources/Weather/USA_FL_Tampa.Intl.AP.722110_TMY3.mos"),
    HSou=GreenVillage.Subsystems.RenewableEnergy.PV.PhysicalModel.Utilities.Types.RadiationDataSource.Input_HDirNor_HDifHor_HGloHor)
    "Weather data model"
    annotation (Placement(transformation(extent={{-98,66},{-78,86}})));

  GreenVillage.System.Baseclasss.ControlBus InfBus "Information bus"
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
  GreenVillage.Subsystems.HeatPump.WSHPPhysics.HPLoopWithHeatRecovery heaPum(datHP8=
        datHP8, datHP9=datHP9)
    "Heat pump system"
    annotation (Placement(transformation(extent={{0,-20},{20,0}})));
  GreenVillage.Subsystems.DomesticHotWater.DHWLoopWithHeatRecovery
                                                      domHotWat(
                                                 redeclare package Medium =
        Medium, mHeaPum_flow_nominal=mHeaRec_flow_nominal)
    "Domestic hot water system"
    annotation (Placement(transformation(extent={{20,-80},{0,-100}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSin(
    T_start(displayUnit="K"),
    redeclare package Medium = Medium,
    m_flow_nominal=mHeaRec_flow_nominal)
   "Temperature sensor"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=90,
        origin={40,-60})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSou(
    T_start(displayUnit="K"),
    redeclare package Medium = Medium,
    m_flow_nominal=mHeaRec_flow_nominal)
     "Temperature sensor"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-20,-60})));
  parameter Modelica.SIunits.Voltage V_nominal=120
    "Nominal voltage (V_nominal >= 0)";
  parameter Modelica.SIunits.Frequency f=60 "Frequency of the source";
equation
  connect(DHI.y[1],weaDat. HDifHor_in) annotation (Line(points={{-119,90},{-108,
          90},{-108,66.4},{-99,66.4}},      color={0,0,127}));
  connect(GHI.y[1],weaDat. HGloHor_in) annotation (Line(points={{-119,60},{-108,
          60},{-108,64},{-99,64}},     color={0,0,127}));
  connect(DNI.y[1],weaDat. HDirNor_in) annotation (Line(points={{-119,30},{-106,
          30},{-106,60},{-106,62.6},{-99,62.6}},
                                           color={0,0,127}));
  connect(PV.terminal, heaPum.terminal) annotation (Line(points={{-60,50},{-72,50},
          {-72,-40},{10,-40},{10,-19.8}}, color={0,120,120}));
  connect(PV.terminal, buiLoa.terminal) annotation (Line(points={{-60,50},{-72,50},
          {-72,-10},{-79,-10}}, color={0,120,120}));
  connect(domHotWat.port_b, TSou.port_a) annotation (Line(points={{4,-80},{-20,-80},
          {-20,-70}},      color={0,127,255}));
  connect(domHotWat.port_a, TSin.port_b)
    annotation (Line(points={{16,-80},{40,-80},{40,-70}},color={0,127,255}));
  connect(gri.terminal, buiLoa.terminal) annotation (Line(points={{10,40.2},{10,
          20},{-70,20},{-70,-10},{-79,-10}}, color={0,120,120}));
  connect(gri.terminal, heaPum.terminal) annotation (Line(points={{10,40.2},{10,
          18},{-68,18},{-68,-38},{10,-38},{10,-19.8}}, color={0,120,120}));
  connect(TSin.port_a, heaPum.port_b)
    annotation (Line(points={{40,-50},{40,-24},{18,-24}}, color={0,127,255}));
  connect(TSou.port_b, heaPum.port_a)
    annotation (Line(points={{-20,-50},{-20,-24},{2,-24}}, color={0,127,255}));
  connect(weaDat.weaBus, PV.weaBus) annotation (Line(
      points={{-78,76},{-70,76},{-70,58},{-60.2,58}},
      color={255,204,51},
      thickness=0.5));
  connect(gri.terminal, domHotWat.terminal) annotation (Line(points={{10,40.2},{
          10,16},{-66,16},{-66,-42},{10,-42},{10,-80.2}}, color={0,120,120}));
  connect(PV.terminal, domHotWat.terminal) annotation (Line(points={{-60,50},{
          -74,50},{-74,-44},{10,-44},{10,-80.2}}, color={0,120,120}));
  connect(PV.infBus, InfBus) annotation (Line(
      points={{-49.8,40.4},{-49.8,28},{70,28},{70,0}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(gri.InfBus, InfBus) annotation (Line(
      points={{16,40},{16,30},{70,30},{70,0}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(buiLoa.infBus, InfBus) annotation (Line(
      points={{-89.8,-19.6},{-89.8,-24},{-50,-24},{-50,26},{70,26},{70,0}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(heaPum.infBus, InfBus) annotation (Line(
      points={{17,-19.8},{70,-19.8},{70,0}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(domHotWat.infBus, InfBus) annotation (Line(
      points={{0,-98},{0,-110},{70,-110},{70,0}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaDat.weaBus, heaPum.weaBus) annotation (Line(
      points={{-78,76},{-20,76},{-20,-2},{1.2,-2}},
      color={255,204,51},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-140,-140},
            {140,140}}),                                        graphics={
        Ellipse(lineColor = {75,138,73},
                fillColor={255,255,255},
                fillPattern = FillPattern.Solid,
                extent={{-138,-138},{140,140}}),
        Polygon(lineColor = {0,0,255},
                fillColor = {75,138,73},
                pattern = LinePattern.None,
                fillPattern = FillPattern.Solid,
                points={{-58,76},{98,0},{-58,-90},{-58,76}})}), Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-140,-140},{140,140}}),
        graphics={Line(points={{-94,-14}}, color={28,108,200})}),
    __Dymola_Commands(file="modelica://GreenVillage/Resources/Scripts/Dymola/System/PhysicalSystem.mos"
        "Simulate and Plot"));
end PhysicalSystem;
