within GreenVillage.System;
model DataDrivenModel "Data driven model for the HGV community"
  extends Modelica.Icons.Example;
  extends
    GreenVillage.Subsystems.HeatPump.DataDrivenModel.Example.BaseClasses.PartialInputInterface;
  parameter Modelica.SIunits.Voltage V_nominal
    "Nominal voltage (V_nominal >= 0)";
  parameter String moduleName = "WSHP_All"
    "Name of the python module that contains the function";
  parameter String functionName = "model"
    "Name of the python function";
  Subsystems.Grid.Grid              gri(f=f, V=V_nominal)
                                        "Electrical grid model"
    annotation (Placement(transformation(extent={{64,70},{84,90}})));
  Baseclasss.ControlBus                     InfBus "Information bus"
    annotation (Placement(transformation(extent={{70,30},{90,50}})));
  Subsystems.BuildingLoads.Loads              buiLoa "Building loads"
    annotation (Placement(transformation(extent={{-36,20},{-16,40}})));
  Subsystems.HeatPump.DataDrivenModel.HeatPumpANN annHeaPum(
    V_nominal=V_nominal)
    "ANN model for heat pumps"
    annotation (Placement(transformation(extent={{30,-50},{50,-30}})));
  Subsystems.RenewableEnergy.PV.DataDrivenModel.HGVPVs annPV
    annotation (Placement(transformation(extent={{24,70},{4,90}})));

  parameter Modelica.SIunits.Frequency f=60 "Frequency of the source";
equation
  connect(annPV.terminal, buiLoa.terminal) annotation (Line(points={{4.8,80},{-4,
          80},{-4,30},{-15,30}},      color={0,120,120}));
  connect(annPV.terminal, annHeaPum.terminal) annotation (Line(points={{4.8,80},
          {-2,80},{-2,26},{60,26},{60,-40},{49.2,-40}},
                                                      color={0,120,120}));
  connect(gri.terminal, buiLoa.terminal) annotation (Line(points={{74,70.2},{74,
          64},{0,64},{0,30},{-15,30}},     color={0,120,120}));
  connect(gri.terminal, annHeaPum.terminal) annotation (Line(points={{74,70.2},{
          74,62},{2,62},{2,28},{62,28},{62,-40},{49.2,-40}}, color={0,120,120}));
  connect(gri.InfBus, InfBus) annotation (Line(
      points={{80,70},{80,40}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(annPV.weaBus, InfBus) annotation (Line(
      points={{22.6,89},{30,89},{30,40},{80,40}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(buiLoa.infBus, InfBus) annotation (Line(
      points={{-25.8,20.4},{-25.8,18},{30,18},{30,40},{80,40}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus.HGloHor, annHeaPum.irrGloSol) annotation (Line(
      points={{-60,80},{-42,80},{-42,12},{20,12},{20,-31},{28,-31}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(fromKelvin.Celsius, annHeaPum.TOutDoo) annotation (Line(points={{-49,60},
          {-44,60},{-44,10},{18,10},{18,-34},{28,-34}}, color={0,0,127}));
  connect(calTim.hour, annHeaPum.houInd) annotation (Line(points={{-79,36.2},{-46,
          36.2},{-46,8},{16,8},{16,-37},{28,-37}}, color={255,127,0}));
  connect(calTim.weekDay, annHeaPum.weeDay) annotation (Line(points={{-79,25},{-48,
          25},{-48,6},{14,6},{14,-40},{28,-40}}, color={255,127,0}));
  connect(holDay.holiday, annHeaPum.holInd) annotation (Line(points={{-29,-10},{
          12,-10},{12,-43},{28,-43}}, color={255,127,0}));
  connect(peaSea.y, annHeaPum.peaSea) annotation (Line(points={{-29,-40},{10,-40},
          {10,-46},{28,-46}}, color={255,127,0}));
  connect(his.y, annHeaPum.preHouPow) annotation (Line(points={{-29,-70},{10,-70},
          {10,-49},{28,-49}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {120,100}})),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}})));
end DataDrivenModel;
