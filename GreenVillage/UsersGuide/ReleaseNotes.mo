within GreenVillage.UsersGuide;
package ReleaseNotes "Release notes"
  extends Modelica.Icons.ReleaseNotes;

 class Version_1_1_0 "Version 1.1.0"
   extends Modelica.Icons.ReleaseNotes;
     annotation (Documentation(info="<html>
<p>Version 1.1.0 is fully compatible with version 1.0.0. It adds the package <span style=\"font-family: Courier New;\">GreenVillage.Subsystems.OccupantBehavior</span> that contains schedule models that determine equipment operation schedules based on building occupancy. An example demonstrates the usage of the model <span style=\"font-family: Courier New;\">GreenVillage.Subsystems.OccupantBehavior.Schedule.Schedule.</span></p>
<p>The following <b><span style=\"color: #0000ff;\">new libraries</span></b> have been added: </p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\"><tr>
<td valign=\"top\"><p>GreenVillage.Subsystems.OccupantBehavior</p></td>
<td valign=\"top\"><p><br>This package contains blocks that determine equipment operation schedules based on building occupancy.</p></td>
</tr>
</table>
</html>"));
 end Version_1_1_0;

class Version_1_0_0 "Version 1.1.0"
  extends Modelica.Icons.ReleaseNotes;
    annotation (preferredView="info", Documentation(info="<html>
<p>First release of the library. </p>
<p>This version contains component and system models for modeling community building loads, domestic hot water system, heat pump system, and power sources (i.e., grid and PV).</p>
</html>"));
end Version_1_0_0;
  annotation (preferredView="info",
  Documentation(info="<html>
<p>
This section summarizes the changes that have been performed
on the Buildings library.
</p>
<ul>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_5_0_1\">Version 5.0.1</a> (November 22, 2017)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_5_0_0\">Version 5.0.0</a> (November 17, 2017)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_4_0_0\">Version 4.0.0</a> (March 29, 2017)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_3_0_0\">Version 3.0.0</a> (March 29, 2016)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_2_1_0\">Version 2.1.0</a> (July 13, 2015)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_2_0_0\">Version 2.0.0</a> (May 4, 2015)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_6_build1\">Version 1.6 build1</a> (June 19, 2014)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_5_build3\">Version 1.5 build3</a> (February 12, 2014)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_5_build2\">Version 1.5 build2</a> (December 13, 2013)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_5_build1\">Version 1.5 build1</a> (October 24, 2013)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_4_build1\">Version 1.4 build1</a> (May 15, 2013)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_3_build1\">Version 1.3 build1</a> (January 8, 2013)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_2_build1\">Version 1.2 build1</a> (July 26, 2012)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_1_build1\">Version 1.1 build1</a> (February 29, 2012)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_0_build2\">Version 1.0 build2</a> (December 8, 2011)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_1_0_build1\">Version 1.0 build1</a> (November 4, 2011)
</li>
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_12_0\">Version 0.12.0 </a> (May 6, 2011)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_11_0\">Version 0.11.0 </a> (March 17, 2011)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_10_0\">Version 0.10.0 </a> (July 30, 2010)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_9_1\">Version 0.9.1 </a> (June 24, 2010)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_9_0\">Version 0.9.0 </a> (June 11, 2010)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_8_0\">Version 0.8.0 </a> (February 6, 2010)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_7_0\">Version 0.7.0 </a> (September 29, 2009)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_6_0\">Version 0.6.0 </a> (May 15, 2009)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_5_0\">Version 0.5.0 </a> (February 19, 2009)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_4_0\">Version 0.4.0 </a> (October 31, 2008)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_3_0\">Version 0.3.0 </a> (September 30, 2008)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_2_0\">Version 0.2.0 </a> (June 17, 2008)
<li>
<a href=\"modelica://Buildings.UsersGuide.ReleaseNotes.Version_0_1_0\">Version 0.1.0 </a> (May 27, 2008)
</li>
</ul>

</html>"));
end ReleaseNotes;
