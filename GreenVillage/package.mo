within ;
package GreenVillage 
annotation (uses(
    Buildings(version = "5.0.1"),
 BuildingSystems(version="2.0.0-beta"),
    Modelica_StateGraph2(version="2.0.2"),
    Modelica(version="3.2.3"),
    ModelicaServices(version="3.2.3")));
end GreenVillage;
